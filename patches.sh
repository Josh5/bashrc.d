
# Create a patch from the directories specified
function patch_create {
    #Help:
    #>>
    # usage:                patch_create <source1> <source2> [outfile]
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;
    # create_patch old_tree new_tree_w_changes output_file
    if [[ !  -z  ${3}  ]]; then
        outfile=${3}.patch;
    else
        stamp=`date "+%Y%m%d-%H%M%S"`;
        outfile=${HOME}/dev/diff-${stamp}.patch;
    fi
    diff -ruN ${1} ${2} > ${outfile};
}
function patch_apply {
    #Help:
    #>>
    # usage:                patch_apply [OPTIONS] <patchfile>
    # options:              --test                     -           Dry run. Will not actually attempt to apply the patch
    # options:              --reverse                  -           Assume patches were created with old and new files swapped
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;
    if [[ !  -z  ${1} ]]; then
        ARGS="";
        url_regex='(https?|ftp|file)://[-A-Za-z0-9\+&@#/%?=~_|!:,.;]*[-A-Za-z0-9\+&@#/%=~_|]';
        for arg in $@; do
            if [[ "${arg}" == "--test" ]]; then
                ARGS="${ARGS} --verbose --dry-run";
                continue;
            fi
            if [[ "${arg}" == "--reverse" ]]; then
                ARGS="${ARGS} --reverse";
                continue;
            fi
            if [[ -f "${arg}" ]]; then
                # Arg points to a file. Doing it this way allows us to add optins berfore or after dclairing the file
                ARGS="${ARGS} --input=${arg}";
                continue;
            fi
            if [[ ${arg} =~ ${url_regex} ]]; then
                # Arg points to a url.
                stamp=`date "+%Y%m%d-%H%M%S"`;
                temp_file=/tmp/patch_${stamp}.diff;
                curl -sSL ${arg} -o ${temp_file};
                ARGS="${ARGS} --input=${temp_file}";
                continue;
            fi
        done
        if [[ ${ARGS}  =~ '--input' ]]; then
            echo patch -p1 ${ARGS};
            patch -p1 ${ARGS};
            return 0;
        fi
    fi
    echo "You need to at least specify a patch";
    echo "Run '${FUNCNAME[0]} --help' for more information";
    echo
    return 1;
}
function patch_to_go {
    # TODO: Add support for mercurial and non src control directories
    PROJECT="${PWD##*/}";
    PROJECTTYPE="generic";
    PATCHESDIR=${HOME}/patches;
    mkdir -p ${PATCHESDIR};
    [[ -d "./.git" ]] && PROJECTTYPE="git";
    [[ -d "./.hg" ]] && PROJECTTYPE="hg";
    if [[ "${1}" == "--import" ]]; then
        # TODO: If not ${2} specified, then prompt to apply the latest one in ${PATCHESDIR}
        INFILE=${2};
        if [[ "${PROJECTTYPE}" == "git" ]]; then
            git apply < ${INFILE};
            echo "Patch imported into project...";
        fi
    else
        DATESTAMP=`date "+%Y-%m-%d"`;
        OUTFILE=${PROJECT}-${DATESTAMP}-${PROJECTTYPE}.patch;
        if [[ "${PROJECTTYPE}" == "git" ]]; then
            # TODO: If file already exists, prompt before overwrite
            git diff > ${PATCHESDIR}/${OUTFILE};
            echo "Patch exported to ${PATCHESDIR}/${OUTFILE}";
        fi
    fi
}

function diff_dir {
    diff \
        --recursive \
        --side-by-side \
        --suppress-blank-empty \
        --suppress-common-lines \
        --ignore-all-space \
        ${1} \
        ${2}
}

