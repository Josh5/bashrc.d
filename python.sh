###
# File: python.sh
# Project: bashrc.d
# File Created: Thursday, 02nd September 2022 11:14:05 am
# Author: Josh.5 (jsunnex@gmail.com)
# -----
# File Created: Thursday, 02nd September 2022 11:14:05 am
# Modified By: Josh5 (jsunnex@gmail.com)
###

######## SETUP: #########
#
### Install pyenv
#
# Follow the instructions here:
#   https://github.com/pyenv/pyenv?tab=readme-ov-file#basic-github-checkout
#
#       > mkdir -p ~/.local/share
#       > git clone https://github.com/pyenv/pyenv.git ~/.local/share/pyenv
#       > cd ~/.local/share/pyenv && src/configure && make -C src
#
###

# Enable pyenv if installed
if [ -d "$HOME/.local/share/pyenv" ]; then
    export PYENV_ROOT="$HOME/.local/share/pyenv"
    [[ -d $PYENV_ROOT/bin ]] && export PATH="$PYENV_ROOT/bin:$PATH"
    eval "$(pyenv init -)"
fi
alias pvm='pyenv'

###
# Setup build environment for Ubuntu: 
#   sudo apt install \
#         build-essential \
#         zlib1g-dev \
#         libncurses5-dev \
#         libgdbm-dev \
#         libnss3-dev \
#         libssl-dev \
#         libreadline-dev \
#         libffi-dev \
#         libsqlite3-dev \
#         wget \
#         libbz2-dev
###
function python_build_and_install_from_source {
    #Help:
    #>>
    # usage:                python_build_and_install_from_source [VERSION]
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;

    __downloads_path="${HOME}/Downloads"
    __python_version=${1}
    if [[ -z ${__python_version} ]]; then
        echo "Specify the version. Eg ${BASH_SOURCE[0]} 3.9.7"
        return 1
    fi

    # Download the version 
    if [[ ! -f "${__downloads_path}/Python-${__python_version}.tgz" ]]; then

        curl -sSL \
            -o "${__downloads_path}/Python-${__python_version}.tgz" \
            https://www.python.org/ftp/python/${__python_version}/Python-${__python_version}.tgz
    fi

    # Extract clean from source
    rm -rf "${__downloads_path}/Python-${__python_version}"
    pushd "${__downloads_path}" || return 1
    tar -xvf Python-${__python_version}.tgz
    popd

    # Configure, build and install
    pushd "${__downloads_path}/Python-${__python_version}" || return 1
    ./configure --enable-optimizations

    make -j8

    sudo make altinstall
    popd
}
