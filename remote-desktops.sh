
# Add rdesktop functions if rdesktop is installed
if [[ $(command -v rdesktop) ]]; then
    function run_rdesktop {
        THIS_RES=$(xdpyinfo | awk '/dimensions/{print $2}');
        WIDTH=$(echo ${THIS_RES} | cut -d'x' -f1);
        HEIGHT=$(echo ${THIS_RES} | cut -d'x' -f2);
        RES=${THIS_RES};
        ARGS="";
        for arg in "$@"; do
            if [[ "${arg}" =~ "--monitors" ]]; then
                mon_count=$(echo ${arg} | awk -F'=' '{print $2}');
                if [[ ${mon_count} == 1 ]]; then
                    RES=1920x1020;
                fi
                if [[ ${mon_count} == 2 ]]; then
                    RES=3840x1020;
                fi
                if [[ ${mon_count} == 3 ]]; then
                    RES=5760x1020;
                fi
                continue;
            fi
            if [[ "${arg}" =~ "-1" ]]; then
                RES=1920x1020;
                continue;
            fi
            if [[ "${arg}" =~ "-2" ]]; then
                RES=3840x1020;
                continue;
            fi
            if [[ "${arg}" =~ "-3" ]]; then
                RES=5760x1020;
                ARGS="${ARGS} -g 5760x1020";
                continue;
            fi
            ARGS="${ARGS} ${arg}";
        done
        ARGS="${ARGS} -g ${RES}";
        echo rdesktop ${ARGS};
        rdesktop ${ARGS};
    }
fi
