
#   ____      _                      
#  / ___|___ | | ___  _   _ _ __ ___ 
# | |   / _ \| |/ _ \| | | | '__/ __|
# | |__| (_) | | (_) | |_| | |  \__ \
#  \____\___/|_|\___/ \__,_|_|  |___/
#

# Define colours to make functions pretty
export CLBLUE="\e[34m";         # Blue
export CLMAG="\e[95m";          # Magenta
export CLGREEN="\e[92m";        # Green
export CLRED="\e[91m";          # Red

export CHEAD="\e[34m";          # Blue
export CLPASSED="\e[92m";       # Green
export CLERRORS="\e[91m";       # Red
export CLWARNING="\e[93m";      # Yellow
export CLIGNORED="\e[36m";      # Cyan

export CNORM="\e[0m";
