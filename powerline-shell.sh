# This will include powerline shell if it is available
# https://github.com/b-ryan/powerline-shell

if [ "X${BASH_VERSION:-}" != "X" ]; then
    if [[ ! -z $(command -v powerline-shell) ]]; then
        function _update_ps1() {
            PS1=$(powerline-shell $?)
        }
        # if [[ $TERM != linux && ! $PROMPT_COMMAND =~ _update_ps1 ]]; then
        #     PROMPT_COMMAND="_update_ps1; $PROMPT_COMMAND"
        # fi
    fi
fi
if [ -n "$ZSH_VERSION" ]; then 
    function powerline_precmd() {
        PS1="$(powerline-shell --shell zsh $?)"
    }
    function install_powerline_precmd() {
      for s in "${precmd_functions[@]}"; do
        if [ "$s" = "powerline_precmd" ]; then
          return
        fi
      done
      precmd_functions+=(powerline_precmd)
    }
    # if [ "$TERM" != "linux" ]; then
    #     install_powerline_precmd
    # fi
    bindkey '^ ' autosuggest-accept
fi
