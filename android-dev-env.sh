# @Author: Josh S
# @Date:   2020-07-04 09:44:04
# @Last Modified by:   Josh.5
# @Last Modified time: 2020-04-07 09:50:28

if [[ $(command -v repo) || -f ${HOME}/.local/share/android-dev-env/bin/repo ]]; then
    export PATH="${HOME}/.local/share/android-dev-env/bin:${PATH}"
fi

# Add stand-alone Android SDK to path
if [[ $(command -v sdkmanager) || -f ${HOME}/.local/share/android-dev-env/cmdline-tools/tools/bin/sdkmanager ]]; then
    export PATH="${HOME}/.local/share/android-dev-env/cmdline-tools/tools/bin:${PATH}"
fi
if [[ $(command -v adb) || -f ${HOME}/.local/share/android-dev-env/platform-tools/adb ]]; then
    export PATH="${HOME}/.local/share/android-dev-env/platform-tools:${PATH}"
fi

# Add Android Studio installation to path
export ANDROID_HOME="$HOME/Android/Sdk"
export ANDROID_SDK_ROOT="$HOME/Android/Sdk"
export PATH=$PATH:$HOME/Android/Sdk/tools; PATH=$PATH:$HOME/Android/Sdk/platform-tools
