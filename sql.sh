
__print_running_postgres () {
    docker ps --format '{{.Names}}' 2> /dev/null | grep "postgres" | head -n1
}

postgres_restore_database () {
    #Help:
    #>>
    # usage:                postgres_restore_database [OPTIONS] [FILE]
    # options:              --database=<name>             -           Specify the database schema to uese when restoring this dump
    # options:              --owner=<name>                -           Specify the user for this database. (Default: 'pgdata')
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;
    if [[ ! -z  ${1} ]]; then
        CWD=${PWD};
        __postgres_restore_database_tmp_db=/tmp/postgres_restore_$(date +%s);
        __postgres_restore_database_database=""
        __postgres_restore_database_owner="pgdata"
        __postgres_restore_database_file=""
        for arg in $@; do
            case ${arg} in
                --database*)
                    __postgres_restore_database_database=$(echo ${arg} | awk -F'=' '{print $2}');
                    continue;
                    ;;
                --owner*)
                    __postgres_restore_database_owner=$(echo ${arg} | awk -F'=' '{print $2}');
                    continue;
                    ;;
                *) 
                    __postgres_restore_database_file="${arg}";
                    continue;
                    ;;
            esac
        done
        if [[ ! -f ${__postgres_restore_database_file} ]]; then
            echo "No such file: ${__postgres_restore_database_file}";
            echo "Unable to restore...";
            return;
        fi
        if [[ -z ${__postgres_restore_database_database} ]]; then
            echo "No database specified";
            echo "Unable to restore...";
            return;
        fi

        # Trigger a prompt for SUDO password
        sudo echo

        # Create a tmp copy of the datbase dump
        echo "Creating copy of database dump prior to restore"
        cp -f ${__postgres_restore_database_file} ${__postgres_restore_database_tmp_db};
        cd /tmp;

        # Check what type of DB dump this is
        __pg_restore_command=""
        if [[ "${__postgres_restore_database_file: -4}" != ".sqlz" ]]; then
            # The input is a PostgreSQL custom-format dump.
            # Use the pg_restore command-line client to restore this dump to a database.
            __pg_restore_command="pg_restore"
        else
            __pg_restore_command="psql"
        fi

        # Check if Postgres is running in a Docker container or not
        latest_running_postgres_container=$(__print_running_postgres)
        if [[ ! -z ${latest_running_postgres_container} ]]; then
            echo "Found running postgres docker container. Assuming that for database restore."
            __postgres_restore_database_command="sudo docker exec -i -u postgres ${latest_running_postgres_container} ${__pg_restore_command}";
            __psql_database_command="sudo docker exec -i -u postgres ${latest_running_postgres_container} psql";
            # Restart the postgres docker container here to kick any current connections off (will prevent errors during restore)
            echo "Restarting postgres docker container";
            sudo docker restart ${latest_running_postgres_container};
            sleep 10
        else
            echo "Assuming local postgres install for database restore."
            __postgres_restore_database_command="sudo -u postgres ${__pg_restore_command}";
            __psql_database_command="sudo -u postgres psql";
        fi

        # Prep Postgres for database restore
        # Create user with password if not exists
        if ! ${__psql_database_command} -tc "SELECT 1 FROM pg_roles WHERE rolname = '${__postgres_restore_database_owner}'" | grep -q 1; then
            echo "CREATE ROLE ${__postgres_restore_database_owner} WITH LOGIN NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION PASSWORD '${__postgres_restore_database_owner}';"
            echo "CREATE ROLE ${__postgres_restore_database_owner} WITH LOGIN NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION PASSWORD '${__postgres_restore_database_owner}';" | ${__psql_database_command}
        fi
        echo "DROP DATABASE IF EXISTS ${__postgres_restore_database_database};"
        echo "DROP DATABASE IF EXISTS ${__postgres_restore_database_database};" | ${__psql_database_command};
        echo "CREATE DATABASE ${__postgres_restore_database_database} WITH OWNER ${__postgres_restore_database_owner};"
        echo "CREATE DATABASE ${__postgres_restore_database_database} WITH OWNER ${__postgres_restore_database_owner};" | ${__psql_database_command};
        # echo "CREATE DATABASE ${__postgres_restore_database_database};"
        # echo "CREATE DATABASE ${__postgres_restore_database_database};" | ${__psql_database_command};
        # echo "GRANT ALL ON DATABASE ${__postgres_restore_database_database} TO ${__postgres_restore_database_owner} WITH GRANT OPTION;"
        # echo "GRANT ALL ON DATABASE ${__postgres_restore_database_database} TO ${__postgres_restore_database_owner} WITH GRANT OPTION;" | ${__psql_database_command};

        # Restore database dump
        echo "cat ${__postgres_restore_database_tmp_db} | ${__postgres_restore_database_command} -d ${__postgres_restore_database_database}";
        cat ${__postgres_restore_database_tmp_db} | ${__postgres_restore_database_command} -d ${__postgres_restore_database_database};

        echo "Database restored - ${__postgres_restore_database_database}";
        cd ${CWD};
        rm -f ${__postgres_restore_database_tmp_db};
    fi
}

psql_shell_docker () {
    latest_running_postgres_container=$(__print_running_postgres)
    if [[ ! -z ${latest_running_postgres_container} ]]; then
        echo docker exec -ti -u postgres ${latest_running_postgres_container} psql; 
        docker exec -ti -u postgres ${latest_running_postgres_container} psql; 
    else
        echo "No running postgres docker container found".
    fi
}

psql_command_docker () {
    latest_running_postgres_container=$(__print_running_postgres)
    if [[ ! -z ${latest_running_postgres_container} ]]; then
        echo docker exec -ti -u postgres ${latest_running_postgres_container} psql -c "${@}";
        docker exec -ti -u postgres ${latest_running_postgres_container} psql -c "${@}";
    else
        echo "No running postgres docker container found".
    fi
}
