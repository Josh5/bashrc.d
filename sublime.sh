if [ "X${BASH_VERSION:-}" != "X" ]; then
    if [[ $(command -v subl) ]]; then
        subl() {
            # If we are passed an arg, first see if that is a file that exists, if not see if we can open a project of that name
            if [[ !  -z  ${1} &&  !  -e  ${1} ]]; then
                project=$(ls ${HOME}/Documents/sublime_projects/${1}* 2> /dev/null | head -1);
                if [[ !  -z  ${project}  ]]; then
                    command subl --project ${project}
                    return;
                fi
            fi
            if [[ ! -t 0 ]]; then
                TMPDIR=${TMPDIR:-/tmp}  # default to /tmp if TMPDIR isn't set
                F=$(mktemp $TMPDIR/subl-stdin-XXXXXXXX)
                cat >| $F  # use >| instead of > if you set noclobber in bash
                command subl $F # open in sublime
                sleep .3  # give subl a little time to open the file
                rm -f $F
                return;
            else
                # If no project is found or the arg was an existing file, then open the passed arg as file
                command subl $@;
                return;
            fi
        }
        sublpatch() {
            OPEN_FILES="";
            for FILE in `ls .hg/patches/*`; do
                if [[ $(basename ${FILE}) != "series" && $(basename ${FILE}) != "status" ]]; then
                    OPEN_FILES="${OPEN_FILES} ${FILE}";
                fi
            done
            if [[ ! -z ${OPEN_FILES} ]]; then
                subl -n ${OPEN_FILES};
            fi
        }
    fi
fi
