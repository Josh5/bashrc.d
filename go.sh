#!/usr/bin/env bash
###
# File: go.sh
# Project: bashrc.d
# File Created: Monday, 5th September 2022 9:58:40 pm
# Author: Josh Sunnex (jsunnex@gmail.com)
# -----
# Last Modified: Monday, 13th February 2023 10:18:32 am
# Modified By: Josh Sunnex (jsunnex@gmail.com)
###


if [ -d ${HOME}/go/bin ]; then
    # Add composer bin dir
    export PATH=${PATH}:${HOME}/go/bin
fi
