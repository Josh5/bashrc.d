
# WSL Specific commands
if [[ "${BASHRCD_CONFIG}" == "WSL" ]]; then

    # Pycharm alias - open the linux version of Pycharm from wsl
    if ! command -v pycharm &> /dev/null && command -v pycharm64.exe &> /dev/null; then
        alias pycharm="pycharm64.exe"
    fi

fi
