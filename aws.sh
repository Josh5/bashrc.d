# For these functions you need to have the AWS cli installed (v2)
# 
# SETUP: 
#   - Follow the CLI installation here:https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html
#   - Fetch your credentials from IAM.
#       - If none exist, create them: https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html#cli-configure-quickstart-creds-create
#   - Apply your credentials using `aws configure`. Eg:
#
#       > AWS Access Key ID [None]: XXXXXXX
#       > AWS Secret Access Key [None]: XXXXXXXXXXXXXXX
#       > Default region name [None]: eu-west-1
#       > Default output format [None]: json
#
#   - Run a simple command to ensure connection `aws sts get-caller-identity`
#   - Add available roles to be assumed
#       - Read the file `~/.aws/credentials` and ensure you have configured the AWS default credentials. Eg:
#
#           > [default]
#           > aws_access_key_id = XXXXXXX
#           > aws_secret_access_key = XXXXXXXXXXXXXXX

#       - Edit the file `~/.aws/config` and configure the default profile. Eg:
#
#           > [default]
#           > region = eu-west-1
#           > output = json
#           > s3 =
#           >   max_concurrent_requests = 1000
#           >   max_queue_size = 10000
#           >   multipart_threshold = 64MB
#           >   multipart_chunksize = 16MB

#       - Continue editing the file `~/.aws/config` and add a profile for each role that you want to be able to assume. Eg:
#
#           > [default]
#           > ...
#           > 
#           > [profile admin_prod]
#           > role_arn = arn:aws:iam::XXXXXXXXXXX:role/role_id
#           > source_profile = default
#           > account_name = test-123456789
#           > environment_name = test-1
#           > environment_type = test
#           > environment_customer = default
#           > region = eu-west-1
#           > color = f9f06b
#

if [[ $(command -v aws) ]]; then

    function __aws_clear_assume_role {
        unset AWS_DEFAULT_PROFILE
        unset AWS_ACCESS_KEY_ID
        unset AWS_SECRET_ACCESS_KEY
        unset AWS_SESSION_TOKEN
        unset AWS_EXPIRATION
        unset AWS_REGION
        unset AWS_DEFAULT_REGION
        # Vars not used by CLI
        unset AWS_PROFILE
        unset AWS_ACCOUNT
        unset AWS_ACCOUNT_NAME
        unset AWS_PROFILE_ROLE_ARN
        unset AWS_ACCOUNT_ID
        unset AWS_CONFIG_PROFILE_NAME
        unset ENVIRONMENT_NAME
        unset ENVIRONMENT_TYPE
        unset CUSTOMER
        if [[ "${1}" != "silent" ]]; then
            echo "Clearing out currently set credentials from exported variables:"
            echo "  - AWS_DEFAULT_PROFILE"
            echo "  - AWS_ACCESS_KEY_ID"
            echo "  - AWS_SECRET_ACCESS_KEY"
            echo "  - AWS_SESSION_TOKEN"
            echo "  - AWS_EXPIRATION"
            echo "  - AWS_REGION"
            echo "  - AWS_DEFAULT_REGION"
            echo "  - AWS_PROFILE"
            echo "  - AWS_ACCOUNT"
            echo "  - AWS_ACCOUNT_NAME"
            echo "  - AWS_PROFILE_ROLE_ARN"
            echo "  - AWS_ACCOUNT_ID"
            echo "  - AWS_CONFIG_PROFILE_NAME"
            echo "  - ENVIRONMENT_NAME"
            echo "  - ENVIRONMENT_TYPE"
            echo "  - CUSTOMER"
            echo
        fi
        return
    }

    function aws_select_default_profile {
        #Help:
        #>>
        # usage:                aws_select_default_profile
        # description:          Selects and then sets the default AWS credentials profile to use for other aws commands.
        # options:              --clear                                         -   Clear out any AWS CLI env credentials in the current session
        # options:              --silent                                        -   Do not print the assumed role credentials in the terminal window
        # options:              --region=<REGION>                               -   Provide region also. Will export this to 'AWS_REGION' & 'AWS_DEFAULT_REGION'.
        # examples:             "aws_select_default_profile --region=eu-west-2  -   Export the session for eu-west-2 region
        #<<
        __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;

        # Defaults
        local aws_credentials_file="${HOME}/.aws/credentials"
        local duration="3600"
        local clear="false"
        local print_aws_credentials="true"
        local export_region=""

        # Parse some args
        for ARG in ${@}; do

            case ${ARG} in  
                --help)
                    return 0;
                    ;;
                --clear)
                    clear="true";
                    ;;
                --silent)
                    print_aws_credentials="false";
                    ;;
                --region*)
                    export_region=$(echo ${ARG} | awk -F'=' '{print $2}');
                    [ -z "${export_region:-}" ] && { echo "Missing value for --region argument. Exit!" && echo && aws_select_default_profile --help && return 1; }
                    ;;
                *) 
                    ;;
            esac

        done

        # Ensure requred things are set before running the rest of the function
        if [[ ! -x $(command -v jq) ]]; then
            echo "ERROR: Missing dependency 'jq'. Install this first...";
            return 1;
        fi
        if [[ ! -f "${aws_credentials_file}" ]]; then
            echo "ERROR: The file '${aws_credentials_file}' does not exist.";
            return 1;
        fi

        # Clear out the current variables
        if [[ ${clear} == "true" ]]; then
            __aws_clear_assume_role
            return
        fi

        local profiles=($(grep -oP '(?<=\[).+(?=\]$)' ${aws_credentials_file}))
        local selected_profile=""
        echo "Select a profile to use:"
        select selected_profile in "${profiles[@]}"; do
            echo
            echo "Selected profile: '${selected_profile}'"
            break
        done
        # Read role ARN from the config file based ont he provided profile
        local aar_aws_access_key_id=$(sed -n '/^\['${selected_profile}'\]/,/^\[/p' ${aws_credentials_file} \
            | grep -oP '^aws_access_key_id\s*=\s*.+$' | cut -d'=' -f2 | tr -d '[:space:]')
        local aar_aws_secret_access_key=$(sed -n '/^\['${selected_profile}'\]/,/^\[/p' ${aws_credentials_file} \
            | grep -oP '^aws_secret_access_key\s*=\s*.+$' | cut -d'=' -f2 | tr -d '[:space:]')
   
        # Clear out previous session
        echo "Clear out previous session"
        echo
        __aws_clear_assume_role "silent"

        # Fetch credentials for a session
        echo "Exporting session token of profile '${selected_profile}'"
        echo

        # Export credentials
        export AWS_DEFAULT_PROFILE=${selected_profile:-default}
        export AWS_ACCESS_KEY_ID=${aar_aws_access_key_id:?}
        export AWS_SECRET_ACCESS_KEY=${aar_aws_secret_access_key:?}
        if [ "X${export_region:-}" != "X" ]; then
            export AWS_REGION="${export_region:?}"
            export AWS_DEFAULT_REGION="${export_region:?}"
        fi

        echo "Success, your session credentials are now exported to:"
        if [[ ${print_aws_credentials} == "true" ]]; then
            echo "  - AWS_DEFAULT_PROFILE:      ${AWS_DEFAULT_PROFILE:?}"
            echo "  - AWS_ACCESS_KEY_ID:        ${AWS_ACCESS_KEY_ID:?}"
            echo "  - AWS_SECRET_ACCESS_KEY:    ${AWS_SECRET_ACCESS_KEY:?}"
            [ "X${export_region:-}" != "X" ] && echo "  - AWS_REGION                ${AWS_REGION:?}"
            [ "X${export_region:-}" != "X" ] && echo "  - AWS_DEFAULT_REGION        ${AWS_DEFAULT_REGION:?}"
        else
            echo "  - AWS_DEFAULT_PROFILE"
            echo "  - AWS_ACCESS_KEY_ID"
            echo "  - AWS_SECRET_ACCESS_KEY"
            [ "X${export_region:-}" != "X" ] && echo "  - AWS_REGION"
            [ "X${export_region:-}" != "X" ] && echo "  - AWS_DEFAULT_REGION"
        fi
    }

    function aws_export_session {
        #Help:
        #>>
        # usage:                aws_export_session
        # description:          Exports a session for the selected profile.
        # options:              --clear                                     -       Clear out the assumed role credentials in the current session
        # options:              --silent                                    -       Do not print the assumed role credentials in the terminal window
        # options:              --region=<REGION>                           -   Provide region also. Will export this to 'AWS_REGION' & 'AWS_DEFAULT_REGION'.
        # examples:             "aws_export_session --region=eu-west-2      -       Export the session for eu-west-2 region
        #<<
        __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;

        # Defaults
        local aws_credentials_file="${HOME}/.aws/credentials"
        local duration="3600"
        local clear="false"
        local print_aws_credentials="true"
        local aar_profile=""
        local export_region=""

        # Parse some args
        for ARG in ${@}; do

            case ${ARG} in  
                --help)
                    return 0;
                    ;;
                --clear)
                    clear="true";
                    ;;
                --silent)
                    print_aws_credentials="false";
                    ;;
                --region*)
                    export_region=$(echo ${ARG} | awk -F'=' '{print $2}');
                    [ -z "${export_region:-}" ] && { echo "Missing value for --region argument. Exit!" && echo && aws_export_session --help && return 1; }
                    ;;
                *) 
                    ;;
            esac

        done

        # Ensure requred things are set before running the rest of the function
        if [[ ! -x $(command -v jq) ]]; then
            echo "ERROR: Missing dependency 'jq'. Install this first...";
            return 1;
        fi
        if [[ ! -f "${aws_credentials_file}" ]]; then
            echo "ERROR: The file '${aws_credentials_file}' does not exist.";
            return 1;
        fi

        # Clear out the current variables
        if [[ ${clear} == "true" ]]; then
            __aws_clear_assume_role
            return
        fi

        # Set profile based on an existing export of AWS_DEFAULT_PROFILE
        if [ "X${AWS_DEFAULT_PROFILE:-}" != "X" ]; then
            aar_profile="${AWS_DEFAULT_PROFILE}"
        fi

        # Fetch role arn from current cli config
        local status=$(aws sts get-caller-identity)
        if [[ $? -gt 0 ]]; then
            echo
            return
        fi
        local status_user_id=$(echo "${status}" | jq -r '.UserId')
        local status_assumed_arn=$(echo "${status}" | jq -r '.Arn')
        if [[ -z $(echo "${status}" | grep 'AWSCLI-Session' 2> /dev/null) ]]; then
            # The current idetity is the main account
            local profile=$(echo "${status}" | jq -r '.Arn' | cut -d'/' -f2)
        else
            assumed_account=$(echo "${status}" | jq -r '.Account')
            __commented_role_arn="arn:aws:iam::${assumed_account}:role\/$(echo "${status_assumed_arn}" | sed -e "s/\/AWSCLI-Session//" | cut -d'/' -f2)"
            local profile=$(grep -PB3 '^role_arn\s*=\s*'${__commented_role_arn}'$' ~/.aws/config \
                | sed -n '/^\[profile/,/^role_arn\s*=\s*'${__commented_role_arn}'/p' \
                | grep -oP '(?<=\[profile).+(?=\]$)' | tr -d '[:space:]')
        fi

        local profiles=($(grep -oP '(?<=\[).+(?=\]$)' ${aws_credentials_file}))
        local selected_profile=""
        if [[ ! -z "${aar_profile:-}" && " ${profiles[@]} " =~ " ${aar_profile} " ]]; then
            selected_profile="${aar_profile}"
        else
            echo "Select a profile to use:"
            select selected_profile in "${profiles[@]}"; do
                echo
                echo "Selected profile: '${selected_profile}'"
                break
            done
        fi
        # Read role ARN from the config file based ont he provided profile
        local aar_aws_access_key_id=$(sed -n '/^\['${selected_profile}'\]/,/^\[/p' ${aws_credentials_file} \
            | grep -oP '^aws_access_key_id\s*=\s*.+$' | cut -d'=' -f2 | tr -d '[:space:]')
        local aar_aws_secret_access_key=$(sed -n '/^\['${selected_profile}'\]/,/^\[/p' ${aws_credentials_file} \
            | grep -oP '^aws_secret_access_key\s*=\s*.+$' | cut -d'=' -f2 | tr -d '[:space:]')
   
        # Clear out previous session
        echo "Clear out previous session"
        echo
        __aws_clear_assume_role "silent"

        # Fetch credentials for a session
        echo "Exporting session token of profile '${selected_profile}'"
        echo

        # Print profile
        echo "UserId:       '${status_user_id}'"
        echo "Arn:          '${status_assumed_arn}'"
        echo

        # Get the session token
        local cmd="aws --output json --profile ${selected_profile:-default} sts get-session-token --duration-seconds=${duration:?}"
        echo "${cmd}"
        local credentials=$(${cmd})
        res=$?
        if [[ ${res} -gt 0 ]]; then
            return 1
        fi
        echo

        # Print credentials information in terminal
        if [[ ${print_aws_credentials} == "true" ]]; then
            echo "${credentials}"
            echo
        fi

        # Export credentials
        export AWS_DEFAULT_PROFILE=${selected_profile:-default}
        export AWS_ACCESS_KEY_ID=$(echo "${credentials}" | jq -r '.Credentials.AccessKeyId')
        export AWS_SECRET_ACCESS_KEY=$(echo "${credentials}" | jq -r '.Credentials.SecretAccessKey')
        export AWS_SESSION_TOKEN=$(echo "${credentials}" | jq -r '.Credentials.SessionToken')
        export AWS_EXPIRATION=$(echo "${credentials}" | jq -r '.Credentials.Expiration')
        # Export additinal environmental config
        export AWS_CONFIG_PROFILE_NAME=${selected_profile}
        if [ "X${export_region:-}" != "X" ]; then
            export AWS_REGION="${export_region:?}"
            export AWS_DEFAULT_REGION="${export_region:?}"
        fi

        [[ ${res} -gt 0 ]] && echo "Failed to generate sessiontoken..." && return 1

        echo "Success, your session credentials are now exported to:"
        if [[ ${print_aws_credentials} == "true" ]]; then
            echo "  - AWS_DEFAULT_PROFILE:      ${AWS_DEFAULT_PROFILE:?}"
            echo "  - AWS_ACCESS_KEY_ID:        ${AWS_ACCESS_KEY_ID:?}"
            echo "  - AWS_SECRET_ACCESS_KEY:    ${AWS_SECRET_ACCESS_KEY:?}"
            echo "  - AWS_SESSION_TOKEN:        ${AWS_SESSION_TOKEN:?}"
            echo "  - AWS_EXPIRATION:           ${AWS_EXPIRATION:?}"
            [ "X${export_region:-}" != "X" ] && echo "  - AWS_REGION                ${AWS_REGION:?}"
            [ "X${export_region:-}" != "X" ] && echo "  - AWS_DEFAULT_REGION        ${AWS_DEFAULT_REGION:?}"
        else
            echo "  - AWS_DEFAULT_PROFILE"
            echo "  - AWS_ACCESS_KEY_ID"
            echo "  - AWS_SECRET_ACCESS_KEY"
            echo "  - AWS_SESSION_TOKEN"
            echo "  - AWS_EXPIRATION"
            [ "X${export_region:-}" != "X" ] && echo "  - AWS_REGION"
            [ "X${export_region:-}" != "X" ] && echo "  - AWS_DEFAULT_REGION"
        fi
    }

    function aws_assume_role {
        #Help:
        #>>
        # usage:                aws_assume_role [OPTIONS]
        # options:              --clear                             -       Clear out the assumed role credentials in the current session
        # options:              --role-arn=<value>                  -       Specify a role ARN rather than reading it from the config file.
        # options:              --profile=<value>                   -       Specify the name of the profile. Will not display the selection option if it finds a match.
        # options:              --config=<path>                     -       The path to your AWS cli config file (default ~/.aws/config)
        # options:              --duration=<value>                  -       The duration, in seconds, of the role session (default 3600 seconds)
        # options:              --silent                            -       Do not print the assumed role credentials in the terminal window
        # options:              --status                            -       Print the currently assumed role for this terminal session
        # options:              --region                            -       Export the configure region also (if specified in the aws cli config)
        # examples:             "aws_assume_role --duration=900     -       Assume a role for 15 mins
        #<<
        __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;

        # Defaults
        local clear="false"
        local status="false"
        local export_region="false"
        local duration="3600"
        local aws_config_file="${HOME}/.aws/config"
        local print_aws_credentials="true"
        local aar_role_arn=""
        local aar_profile=""

        # Parse some args
        for ARG in ${@}; do

            case ${ARG} in  
                --help)
                    return 0;
                    ;;
                --clear)
                    clear="true";
                    ;;
                --role-arn*)
                    # EG. Eg arn:aws:organizations::XXXXXXXXX:role/OrganizationAccountAccessRole
                    aar_role_arn=$(echo ${ARG} | awk -F'=' '{print $2}');
                    ;;
                --profile*)
                    aar_profile=$(echo ${ARG} | awk -F'=' '{print $2}');
                    ;;
                --config*)
                    aws_config_file=$(echo ${ARG} | awk -F'=' '{print $2}');
                    ;;
                --duration*)
                    duration=$(echo ${ARG} | awk -F'=' '{print $2}');
                    ;;
                --silent)
                    print_aws_credentials="false";
                    ;;
                --status)
                    status="true";
                    ;;
                --region)
                    export_region="true";
                    ;;
                *) 
                    ;;
            esac

        done

        # Ensure requred things are set before running the rest of the function
        if [[ ! -x $(command -v jq) ]]; then
            echo "ERROR: Missing dependency 'jq'. Install this first...";
            return 1;
        fi
        if [[ ! -f "${aws_config_file}" ]]; then
            echo "ERROR: The file '${aws_config_file}' does not exist.";
            return 1;
        fi

        # Clear out the current variables
        if [[ ${clear} == "true" ]]; then
            __aws_clear_assume_role
            return
        fi

        # Fetch role arn from current cli config
        if [[ ${status} == "true" ]]; then
            status=$(aws sts get-caller-identity)
            if [[ $? -gt 0 ]]; then
                echo
                return
            fi
            local user_id=$(echo "${status}" | jq -r '.UserId')
            local assumed_arn=$(echo "${status}" | jq -r '.Arn')
            if [[ -z $(echo "${status}" | grep 'AWSCLI-Session' 2> /dev/null) ]]; then
                # The current idetity is the main account
                local profile=$(echo "${status}" | jq -r '.Arn' | cut -d'/' -f2)
                echo "User/Role:    '${profile:-UNKNOWN}'"
            else
                local assumed_account=$(echo "${status}" | jq -r '.Account')
                local __commented_role_arn="arn:aws:iam::${assumed_account}:role\/$(echo "${assumed_arn}" | sed -e "s/\/AWSCLI-Session//" | cut -d'/' -f2)"
                local profile=$(grep -PB3 '^role_arn\s*=\s*'${__commented_role_arn}'$' ~/.aws/config \
                    | sed -n '/^\[profile/,/^role_arn\s*=\s*'${__commented_role_arn}'/p' \
                    | grep -oP '(?<=\[profile).+(?=\]$)' | tr -d '[:space:]')
                echo "Profile:      '${profile:-UNKNOWN}'"
            fi
            echo "UserId:       '${user_id}'"
            echo "Arn:          '${assumed_arn}'"
            echo
            return
        fi

        if [[ -z ${aar_role_arn} ]]; then
            echo "Select a profile to assume the role of:"
            local profiles=($(grep -oP '(?<=\[profile).+(?=\]$)' ${aws_config_file}))
            local selected_profile=""
            if [[ ! -z ${aar_profile} && " ${profiles[@]} " =~ " ${aar_profile} " ]]; then
                selected_profile="${aar_profile}"
            else
                select selected_profile in "${profiles[@]}"; do
                    echo
                    echo "Selected profile: '${selected_profile}'"
                    break
                done
            fi
            # Read role ARN from the config file based ont he provided profile
            local aar_source_profile=$(sed -n '/^\[profile '${selected_profile}'\]/,/^\[/p' ${aws_config_file} \
                | grep -oP '^source_profile\s*=\s*.+$' | cut -d'=' -f2 | tr -d '[:space:]')
            local aar_role_arn=$(sed -n '/^\[profile '${selected_profile}'\]/,/^\[/p' ${aws_config_file} \
                | grep -oP '^role_arn\s*=\s*.+$' | cut -d'=' -f2 | tr -d '[:space:]')
            local aar_account_id=$(sed -n '/^\[profile '${selected_profile}'\]/,/^\[/p' ${aws_config_file} \
                | grep -oP '^role_arn\s*=\s*.+$' | cut -d':' -f5 | tr -d '[:space:]')
            local aar_account_name=$(sed -n '/^\[profile '${selected_profile}'\]/,/^\[/p' ${aws_config_file} \
                | grep -oP '^account_name\s*=\s*.+$' | cut -d'=' -f2 | tr -d '[:space:]')
            local aar_environment_name=$(sed -n '/^\[profile '${selected_profile}'\]/,/^\[/p' ${aws_config_file} \
                | grep -oP '^environment_name\s*=\s*.+$' | cut -d'=' -f2 | tr -d '[:space:]')
            local aar_environment_type=$(sed -n '/^\[profile '${selected_profile}'\]/,/^\[/p' ${aws_config_file} \
                | grep -oP '^environment_type\s*=\s*.+$' | cut -d'=' -f2 | tr -d '[:space:]')
            local aar_environment_customer=$(sed -n '/^\[profile '${selected_profile}'\]/,/^\[/p' ${aws_config_file} \
                | grep -oP '^environment_customer\s*=\s*.+$' | cut -d'=' -f2 | tr -d '[:space:]')
            local aar_region=$(sed -n '/^\[profile '${selected_profile}'\]/,/^\[/p' ${aws_config_file} \
                | grep -oP '^region\s*=\s*.+$' | cut -d'=' -f2 | tr -d '[:space:]')
        fi

        # Ensure a role was either provided or selected above
        if [[ -z ${aar_role_arn} ]]; then
            echo "NO Role ARN was provided. Cannot continue... Exit!"
            return 1
        fi
        echo

        # Clear out previous session
        echo "Clear out previous session"
        echo
        __aws_clear_assume_role "silent"

        # Fetch credentials for the assumed role
        echo "Assuming the role of ${aar_role_arn}"
        echo

        # Get the credentials
        local cmd="aws --output json --profile ${aar_source_profile:-default} sts assume-role --role-arn=${aar_role_arn} --role-session-name=AWSCLI-Session --duration-seconds=${duration}"
        echo "${cmd}"
        local credentials=$(${cmd})
        res=$?
        if [[ ${res} -gt 0 ]]; then
            return 1
        fi
        echo

        # Print credentials information in terminal
        if [[ ${print_aws_credentials} == "true" ]]; then
            echo "${credentials}"
            echo
        fi

        # Export credentials
        export AWS_ACCESS_KEY_ID=$(echo "${credentials}" | jq -r '.Credentials.AccessKeyId')
        export AWS_SECRET_ACCESS_KEY=$(echo "${credentials}" | jq -r '.Credentials.SecretAccessKey')
        export AWS_SESSION_TOKEN=$(echo "${credentials}" | jq -r '.Credentials.SessionToken')
        export AWS_EXPIRATION=$(echo "${credentials}" | jq -r '.Credentials.Expiration')
        # Export additinal environmental config
        export AWS_ACCOUNT_NAME=${aar_account_name}
        export AWS_PROFILE_ROLE_ARN=${aar_role_arn}
        export AWS_ACCOUNT_ID=${aar_account_id}
        export AWS_CONFIG_PROFILE_NAME=${selected_profile}
        export ENVIRONMENT_NAME=${aar_environment_name}
        export ENVIRONMENT_TYPE=${aar_environment_type}
        export CUSTOMER=${aar_environment_customer}
        if [[ "${export_region}" == "true" ]]; then
            export AWS_REGION="${aar_region}"
            export AWS_DEFAULT_REGION="${aar_region}"
        fi

        [[ ${res} -gt 0 ]] && echo "Failed to assume role..." && return 1

        echo "Success, your credentials are now exported to:"
        echo "  - AWS_ACCESS_KEY_ID"
        echo "  - AWS_SECRET_ACCESS_KEY"
        echo "  - AWS_SESSION_TOKEN"
        echo "  - AWS_EXPIRATION"
        [[ "${export_region}" == "true" ]] && echo "  - AWS_REGION"
        [[ "${export_region}" == "true" ]] && echo "  - AWS_DEFAULT_REGION"
        echo
        echo "Additionally, the following has been set:"
        echo "  - AWS_ACCOUNT_NAME:         ${AWS_ACCOUNT_NAME}"
        echo "  - AWS_PROFILE_ROLE_ARN:     ${AWS_PROFILE_ROLE_ARN}"
        echo "  - AWS_ACCOUNT_ID:           ${AWS_ACCOUNT_ID}"
        echo "  - AWS_CONFIG_PROFILE_NAME:  ${AWS_CONFIG_PROFILE_NAME}"
        echo "  - ENVIRONMENT_NAME:         ${ENVIRONMENT_NAME}"
        echo "  - ENVIRONMENT_TYPE:         ${ENVIRONMENT_TYPE}"
        echo "  - CUSTOMER:                 ${CUSTOMER}"
    }

    function aws_asg_terminate_instance {
        #Help:
        #>>
        # usage:                aws_asg_terminate_instance [OPTIONS]
        # options:              --name=<string>                     -       The name of the ASG
        # options:              --id=<string>                       -       The ID of the instance that you want terminated
        #<<
        __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;

        __aws_asg_terminate_instance_name=""
        __aws_asg_terminate_instance_id=""

        # Parse some args
        for ARG in ${@}; do

            case ${ARG} in
                --name*)
                    __aws_asg_terminate_instance_name=$(echo ${ARG} | awk -F'=' '{print $2}');
                    ;;
                --id*)
                    __aws_asg_terminate_instance_id=$(echo ${ARG} | awk -F'=' '{print $2}');
                    ;;
                *) 
                    ;;
            esac

        done

        aws autoscaling complete-lifecycle-action \
            --lifecycle-hook-name instance-terminating \
            --auto-scaling-group-name ${__aws_asg_terminate_instance_name} \
            --lifecycle-action-result ABANDON \
            --instance-id ${__aws_asg_terminate_instance_id} \
            --profile corp
    }

    function aws_list_ami {
        #Help:
        #>>
        # usage:                aws_list_ami <glob>
        #<<
        __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;

        aws ec2 describe-images \
            --region us-east-1 \
            --query 'reverse(sort_by(Images, &CreationDate))[].Name' \
            --filters "Name=name,Values=${@}" "Name=architecture,Values=x86_64" "Name=virtualization-type,Values=hvm"
    }

    function aws_print_all_ssm_params {
        #Help:
        #>>
        # usage:                aws_print_all_ssm_params
        # examples:             "aws_print_all_ssm_params > ssm-vars.txt"     -       Export all current params to a file.
        #<<
        __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;

        local parameters=""
        local parameter=""
        local working_dir=$(mktemp -d)
        pushd "${working_dir:?}" &>/dev/null || return 1
        echo "" >ssm-vars.txt
        all_ssm_parameters=$(aws ssm describe-parameters --query 'Parameters[].Name' --output text)
        for parameter in ${all_ssm_parameters:?}; do
            (
                parameter_value=$(aws ssm get-parameter --name ${parameter:?} --with-decryption --query 'Parameter.Value' --output text)
                [ -z "${parameter_value}" ] && parameter_value="-"
                echo "${parameter:?}=${parameter_value:?}" >>"tmp-ssm-var-${parameter:?}.txt"
            ) &
        done
        wait
        for parameter in ${all_ssm_parameters:?}; do
            cat "tmp-ssm-var-${parameter:?}.txt" >>ssm-vars.txt
        done
        cat ssm-vars.txt
        popd &>/dev/null || return 1
    }

fi

function aws_cli_install {
    #Help:
    #>>
    # usage:                aws_cli_install
    # description:          Installs a portable instance of aws cli
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;

    # Create any missing directories
    mkdir -p \
        "${HOME}"/.local/{bin,share}
    export PATH="${PATH}:${HOME}/.local/bin"

    # Install AWS CLI
    command -v wget >/dev/null 2>&1 || { echo "ERROR! Missing required command 'wget'"; return 1; }
    command -v unzip >/dev/null 2>&1 || { echo "ERROR! Missing required command 'unzip'"; return 1; }
    mkdir -p "${HOME}/.local/share/aws-cli"
    local dl_url="https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip"
    if [ "X${AWS_CLI_VERSION:-}" != "X" ]; then
        dl_url="https://awscli.amazonaws.com/awscli-exe-linux-x86_64-${AWS_CLI_VERSION:?}.zip"
    fi
    wget -q "${dl_url:?}" -O "${HOME}/.local/share/aws-cli/awscliv2.zip"
    unzip -q -o "${HOME}/.local/share/aws-cli/awscliv2.zip" -d "${HOME}/.local/share/aws-cli/"
    "${HOME}/.local/share/aws-cli/aws/install" --update --install-dir "${HOME}/.local/share/aws-cli" --bin-dir "${HOME}/.local/bin"
}

function aws_check_flow_log_config_in_all_regions {
    regions=$(aws ec2 describe-regions --query "Regions[].RegionName" --output text)
    for region in $regions; do
        echo "Checking region: $region"
        
        # Get the list of all VPCs in the region
        vpcs=$(aws ec2 describe-vpcs --region $region --query "Vpcs[].VpcId" --output text)
        
        # Loop through each VPC
        for vpc in $vpcs; do
            echo "Checking VPC: $vpc in region: $region"
            
            # Get the VPC Flow Log configuration for the VPC
            aws ec2 describe-flow-logs --region $region --filter Name=resource-id,Values=$vpc --query "FlowLogs[]" --output table
        done
    done
}
