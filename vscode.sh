if [ "X${BASH_VERSION:-}" != "X" ]; then
    vscode() {
        VSCODE_CMD=$(which code);
        # If we are passed an arg, first see if that is a file that exists, if not see if we can open a project of that name
        if [[ ! -z ${1} && ! -e ${1} ]]; then
            PROJECT_DIR=${HOME}/Documents/vscode_projects;
            mkdir -p ${PROJECT_DIR};
            PROJECT=$(ls ${PROJECT_DIR}/${1}* 2> /dev/null | head -1);
            if [[ ! -z ${PROJECT} ]]; then
                ${VSCODE_CMD} ${PROJECT}
                return;
            fi
        fi
        # If no project is found or the arg was an existing file, then open the passed arg as file
        ${VSCODE_CMD} $@;
        return;
    }
    
    # Functions for creating 'WIP' directories
    __wip_select() {
        mkdir -p "${HOME}/Documents/WIP"
        select_options=()
        for wip_dir in "${HOME}/Documents/WIP/"*; do
            [[ ! -d "${wip_dir}" ]] && continue;
            select_options+=("${wip_dir##*/}")
        done
        [[ ! -z "${@}" ]] && select_options+=("${@}");
        
        select opt in "${select_options[@]}"; do
            if [[ " ${select_options[*]} " =~ " ${opt} " ]]; then
                echo "${opt}"
                return 0
            fi
        done
    }
    wip-open() {
        echo
        selected_opt=$(__wip_select "NEW PROJECT" | tail -n1)
        
        wip_dir="${HOME}/Documents/WIP/${selected_opt}"
        if [ "${selected_opt}" == "NEW PROJECT" ]; then
            echo
            read -p "Enter a name for the new project: " NAME

            # replace any whitespace with dashes
            wip_dir="${HOME}/Documents/WIP/${NAME// /-}-$(date +"%Y-%m-%d-%H%M")"
            
            mkdir -p "${wip_dir}"
        fi
        
        touch "${wip_dir}/notes.md"
        if command -v code $> /dev/null; then
            command code "${wip_dir}"
        fi
    }
    wip-del() {
        echo
        selected_opt=$(__wip_select | tail -n1)
        
        wip_dir="${HOME}/Documents/WIP/${selected_opt}"
        echo "${wip_dir}";
        echo
        read -p "Are you sure you want to delete the WIP directory listed above? (y/n) " AN;
        while [[ ${AN} != "Y" && ${AN} != "y" && ${AN} != "N" && ${AN} != "n" ]]; do
            read -p "You need to enter either 'y' or 'n'. (y/n) " AN;
        done
        if [[ "${AN}" == "Y" || "${AN}" == "y" ]]; then
            rm -rf ${wip_dir};
        fi
    }
fi
