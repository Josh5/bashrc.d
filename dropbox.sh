
# Restart dropbox (if it is not showing the tray icon)
function dropbox_restart {
    bash -c 'dropbox stop && env DBUS_SESSION_BUS_ADDRESS="" dropbox start -i';
}
