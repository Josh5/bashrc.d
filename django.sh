
# Enable calling django manage script from anywhere in the tree
function django-manage {
    SEARCH_DIR=$PWD;
    while [[ ${SEARCH_DIR} != / ]]; do
        if [ -e ${SEARCH_DIR}/manage.py ]; then
            ${SEARCH_DIR}/manage.py $@;
            break;
        fi
        # Note: if you want to ignore symlinks, use "$(realpath -s "${SEARCH_DIR}"/..)"
        SEARCH_DIR="$(readlink -f "${SEARCH_DIR}"/..)"
    done
}

