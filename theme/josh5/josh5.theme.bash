# Custom version of the Bash-it Zork theme. 
# In some cases I want to run this terminal theme without Bash-it installed.
# This provides a stand-alone minimal terminal with only the theme part applied.
#
SCM_THEME_PROMPT_PREFIX=""
SCM_THEME_PROMPT_SUFFIX=""

SCM_THEME_PROMPT_DIRTY=" ${bold_red}✗${normal}"
SCM_THEME_PROMPT_CLEAN=" ${bold_green}✓${normal}"
SCM_GIT_CHAR="${bold_green}±${normal}"
SCM_SVN_CHAR="${bold_cyan}⑆${normal}"
SCM_HG_CHAR="${bold_red}☿${normal}"

#Mysql Prompt
export MYSQL_PS1="(\u@\h) [\d]> "

case $TERM in
        xterm*)
        TITLEBAR="\[\033]0;\w\007\]"
        ;;
        *)
        TITLEBAR=""
        ;;
esac

PS3=">> "

# show chroot if exist
chroot(){
    if [ -n "$debian_chroot" ]; then
        my_ps_chroot="${bold_cyan}$debian_chroot${normal}";
        echo "($my_ps_chroot)";
    fi
}

# show virtualenvwrapper
my_ve(){
    local my_ps_ve=""
    if [ -n "$SANDBOX_ENV" ]; then
        my_ps_ve="${my_ps_ve:-}(${bold_purple}${SANDBOX_ENV}${normal})";
        
    fi
    if [ -n "$CONDA_DEFAULT_ENV" ]; then
        my_ps_ve="${my_ps_ve:-}(${bold_purple}${CONDA_DEFAULT_ENV}${normal})";
    elif [ -n "$VIRTUAL_ENV" ]; then
        my_ps_ve="${my_ps_ve:-}(${bold_purple}${ve}${normal})";
    fi
    echo "${my_ps_ve:-}";
}

if [ -d ${HOME}/.bash_it -a -r ${HOME}/.bash_it -a -x ${HOME}/.bash_it ]; then
    __my_rvm_ruby_version() {
        local gemset=$(echo $GEM_HOME | awk -F'@' '{print $2}')
        [ "$gemset" != "" ] && gemset="@$gemset"
        local version=$(echo $MY_RUBY_HOME | awk -F'-' '{print $2}')
        local full="$version$gemset"
        [ "$full" != "" ] && echo "[$full]"
    }

    is_vim_shell() {
        if [ ! -z "$VIMRUNTIME" ]; then
            echo "[${cyan}vim shell${normal}]"
        fi
    }
else
    # Just load up a custom minimal theme that is close to the one provided in bash-it
    GIT_EXE="$(type -P 'git' || true)"

    black="\[\e[0;30m\]"
    red="\[\e[0;31m\]"
    green="\[\e[0;32m\]"
    yellow="\[\e[0;33m\]"
    blue="\[\e[0;34m\]"
    purple="\[\e[0;35m\]"
    cyan="\[\e[0;36m\]"
    white="\[\e[0;37m\]"
    orange="\[\e[0;91m\]"

    bold_black="\[\e[30;1m\]"
    bold_red="\[\e[31;1m\]"
    bold_green="\[\e[32;1m\]"
    bold_yellow="\[\e[33;1m\]"
    bold_blue="\[\e[34;1m\]"
    bold_purple="\[\e[35;1m\]"
    bold_cyan="\[\e[36;1m\]"
    bold_white="\[\e[37;1m\]"
    bold_orange="\[\e[91;1m\]"

    normal="\[\e[0m\]"
    reset_color="\[\e[39m\]"

    __my_rvm_ruby_version() {
        echo "";
    }
    scm_prompt() {
        # Minimal git branch info
        SCM_BRANCH=""
        if [[ -x "$GIT_EXE" ]] && [[ -n "$(git rev-parse --is-inside-work-tree 2> /dev/null)" ]]; then
            SCM_BRANCH=$(git symbolic-ref -q --short HEAD 2> /dev/null) ||
            SCM_BRANCH=$(git describe --tags --exact-match 2> /dev/null) || 
            SCM_BRANCH=$(git rev-parse --short HEAD)
            SCM_BRANCH="[${SCM_BRANCH}]"
        fi
        echo -e "${SCM_BRANCH}"
    }
    function safe_append_prompt_command {
        local prompt_re
        # Set OS dependent exact match regular expression
        if [[ ${OSTYPE} == darwin* ]]; then
            # macOS
            prompt_re="[[:<:]]${1}[[:>:]]"
        else
            # Linux, FreeBSD, etc.
            prompt_re="\<${1}\>"
        fi
        if [[ ${PROMPT_COMMAND[*]:-} =~ ${prompt_re} ]]; then
            return
        elif [[ -z ${PROMPT_COMMAND} ]]; then
            PROMPT_COMMAND="${1}"
        else
            PROMPT_COMMAND="${1};${PROMPT_COMMAND}"
        fi
    }
fi

prompt() {
    SCM_PROMPT_FORMAT='[%s][%s]'
    my_ps_host="${green}\h${normal}";
    # yes, these are the the same for now ...
    my_ps_host_root="${green}\h${normal}";

    my_ps_user="${bold_green}\u${normal}"
    my_ps_root="${bold_red}\u${normal}";
    my_ps_time="${yellow}\@${normal}";

    if [ -n "$VIRTUAL_ENV" ]
    then
        ve=`basename "$VIRTUAL_ENV"`;
    fi

    # nice prompt
    case "`id -u`" in
        0) PS1="${TITLEBAR}┌─$(my_ve)$(chroot)[$my_ps_root][$my_ps_host_root][$my_ps_time]$(scm_prompt)$(__my_rvm_ruby_version)[${cyan}\w${normal}]\n└─▪ "
        ;;
        *) PS1="${TITLEBAR}┌─$(my_ve)$(chroot)[$my_ps_user][$my_ps_host][$my_ps_time]$(scm_prompt)$(__my_rvm_ruby_version)[${cyan}\w${normal}]\n└─▪ "
        ;;
    esac
}

PS2="└─▪ "

safe_append_prompt_command prompt
