
if [[ "${BASHRCD_CONFIG}" == "WSL" ]]; then
    # Open GUI windows in windows X11 window manager
    #export DISPLAY=localhost:0.0

    # Ensure the Documents directory is symlinked to the windows share
    if [[ ! -e "${HOME}/Documents" ]]; then
        ln -s "/mnt/c/Users/${USER}/Documents" "${HOME}/Documents"
    fi
fi
