#!/usr/bin/env bash
###
# File: dev-tools.sh
# Project: bashrc.d
# File Created: Wednesday, 7th September 2022 9:00:41 am
# Author: Josh Sunnex (jsunnex@gmail.com)
# -----
# Last Modified: Wednesday, 15th January 2025 11:46:35 am
# Modified By: Josh.5 (jsunnex@gmail.com)
###


 
# Check a server's service on any port for the support of TLS/SSL ciphers, 
#   protocols as well as some cryptographic flaws.
#   Source: https://github.com/drwetter/testssl.sh
testssl.sh() {
    local container_run_cmd="docker"
    local container_run_cmd_extra_params=""
    if command -v podman &>/dev/null; then
        # Prefer podman over docker
        container_run_cmd="podman"
        container_run_cmd_extra_params="--userns keep-id --security-opt label:disable";
    elif ! docker ps &>/dev/null; then
        container_run_cmd="sudo ${container_run_cmd:?}"
    fi
    ${container_run_cmd:?} run --rm -ti ${container_run_cmd_extra_params:-} drwetter/testssl.sh ${@}
}

url_encode() {
    python3 -c "import urllib.parse;print(urllib.parse.quote('${@}', safe=''))"
}


# port_scan () {
#     #Help:
#     #>>
#     # usage:                docker_run_sh [OPTIONS]
#     # description:          Run a docker container and attach a shell. By default will run 'sh' but this can be modified with the '--exec' option.
#     # options:              --address=<address>                           -       The address to scan
#     # options:              --start=<start-port>                          -       The start port to scan
#     # options:              --end=<end-port>                              -       The end port to scan
#     # examples:             "port_scan --address=10.0.0.1"                -       Run scan against the IP address
#     # examples:             "port_scan --address=10.0.0.1" --start=1000   -       start scan at port 1000
#     #<<
#     __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;

port_scan () {
    local ADDRESS="";
    local START_PORT=1
    local END_PORT=65535
    local CONCURRENT_LIMIT=50
    local TIMEOUT=1

    # Get Args
    for arg in ${@}; do
        if [[ "${arg}" =~ "--address" ]]; then
            ADDRESS=$(echo ${arg} | awk -F'=' '{print $2}');
            continue;
        fi
        if [[ "${arg}" =~ "--start" ]]; then
            START_PORT=$(echo ${arg} | awk -F'=' '{print $2}');
            continue;
        fi
        if [[ "${arg}" =~ "--end" ]]; then
            END_PORT=$(echo ${arg} | awk -F'=' '{print $2}');
            continue;
        fi
    done
    if [ -z ${ADDRESS:-} ]; then
        echo "--address parameter required"
        echo
        return 1
    fi

    # Prepare a file to hold all the port numbers for the progress bar
    local PORTS_FILE=$(mktemp)
    for (( PORT=START_PORT; PORT<=END_PORT; PORT++ )); do
        echo $PORT >> "${PORTS_FILE:?}"
    done

    local RESULTS=$(mktemp)
    function check_port() {
        PORT=$1
        # Check TCP port
        if timeout $TIMEOUT bash -c "echo > /dev/tcp/${ADDRESS:?}/${PORT:?}" 2>/dev/null; then
            echo "TCP Port ${PORT:?} is open" >> "${RESULTS:?}"
        fi
    }

    # Initialize a counter to keep track of how many jobs are running
    local COUNT=0

    # Track progress manually
    local PROGRESS=0
    local PERCENT=0
    local TOTAL_PORTS=$(($END_PORT - $START_PORT + 1))
    echo -ne "Scanning port: [0/${TOTAL_PORTS}]\r"

    while IFS= read -r PORT; do
        (check_port $PORT > /dev/null 2>&1 & disown)
        ((COUNT++))
        ((PROGRESS++))
        PERCENT=$((PROGRESS * 100 / TOTAL_PORTS))
        
        # Update the progress display
        echo -ne "Scanning address ${ADDRESS:?} for TCP port: $PORT [${PROGRESS}/${TOTAL_PORTS} (${PERCENT}%)]\r"

        # When we reach our concurrency limit, wait for all background jobs to complete
        if [ "$COUNT" -ge "$CONCURRENT_LIMIT" ]; then
            wait
            COUNT=0
        fi
    done < "$PORTS_FILE"

    # Ensure all background jobs are finished
    wait
    echo -e ""
    echo "Scanning address ${ADDRESS:?} complete."
    sleep 1
    echo
    echo "Open ports:"
    cat "$RESULTS"
    rm "$RESULTS" "$PORTS_FILE"
}
