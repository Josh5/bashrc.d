#!/usr/bin/env bash
###
# File: terraform.sh
# Project: bashrc.d
# File Created: Friday, 5th May 2023 3:17:48 pm
# Author: Josh.5 (jsunnex@gmail.com)
# -----
# Last Modified: Monday, 2nd September 2024 2:25:19 pm
# Modified By: Josh5 (jsunnex@gmail.com)
###

function tvm {
    #Help:
    #>>
    # usage:                tvm [ARG] [<VALUE>]
    # description:          Terraform Version Manager
    # arguments:            install <version>                   -       Download and install a <version>.
    # arguments:            use <version>                       -       Use a specified <version> of terraform.
    # arguments:            use-tmp <version>                   -       Tempoarily use a specified <version> of terraform for this terminal session only.
    # arguments:            ls                                  -       List installed versions.
    # arguments:            ls-remote                           -       List remote versions available for install.
    # examples:             "tvm install 0.13.5"                -       Download and install v0.13.5
    # examples:             "tvm use 0.13.5"                    -       Use the 0.13.0 release of Terraform
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;

    function __tvm_install {
        local __version="$@"
        local __url="https://releases.hashicorp.com/terraform/${__version}/terraform_${__version}_linux_amd64.zip"
        mkdir -p "${HOME}/.local/share/tvm/"
        echo "Downloading and installing terraform ${__version}..."
        wget -q "https://releases.hashicorp.com/terraform/${__version}/terraform_${__version}_SHA256SUMS" \
            -O "${HOME}/.local/share/tvm/terraform_${__version}_SHA256SUMS"
        echo "$(wget -qO- "https://releases.hashicorp.com/terraform/${__version}/terraform_${__version}_SHA256SUMS" | grep "linux_amd64.zip")" > "${HOME}/.local/share/tvm/terraform_${__version}_SHA256SUMS"
        echo "Downloading ${__url}..."
        wget -q --show-progress "https://releases.hashicorp.com/terraform/${__version}/terraform_${__version}_linux_amd64.zip" \
            -O "${HOME}/.local/share/tvm/terraform_${__version}_linux_amd64.zip"
        [[ $? -gt 0 ]] && echo "Unable to download version ${__version}..." && return 1
        echo "Computing checksum with sha256sum"
        
        pushd "${HOME}/.local/share/tvm/" &> /dev/null || return 1
        sha256sum --check "./terraform_${__version}_SHA256SUMS" &> /dev/null
        [[ $? -gt 0 ]] && echo "ERROR! Checksums did not match" && return 1
        popd &> /dev/null || return 1
        echo "Checksums matched!"
        echo "Extracting ${__version}..."
        mkdir -p "${HOME}/.local/share/tvm/${__version}"
        unzip -q -o "${HOME}/.local/share/tvm/terraform_${__version}_linux_amd64.zip" -d "${HOME}/.local/share/tvm/${__version}/"
    }

    function __tvm_use {
        local __version="$@"
        if [[ ! -f "${HOME}/.local/share/tvm/${__version}/terraform" ]]; then
            echo "N/A: version ${__version} is not yet installed."
            echo
            echo "You need to run 'tvm install ${__version}' to install it before using it."
            return 1
        fi
        mkdir -p "${HOME}/.local/bin"
        ln -sf "${HOME}/.local/share/tvm/${__version}/terraform" "${HOME}/.local/bin/terraform"
        echo "Now using terraform ${__version}"
    }

    function __tvm_use_tmp {
        local __version="$@"
        if [[ ! -f "${HOME}/.local/share/tvm/${__version}/terraform" ]]; then
            echo "N/A: version ${__version} is not yet installed."
            echo
            echo "You need to run 'tvm install ${__version}' to install it before using it."
            return 1
        fi
        export PATH="${HOME}/.local/share/tvm/${__version}/:${PATH}"
        echo "Exported installation of terraform ${__version} for this terminal only"
    }

    function __tvm_ls {
        for installed_version in "${HOME}"/.local/share/tvm/*; do
            if [[ -d "${installed_version}" ]]; then
                basename "${installed_version}"
            fi
        done
    }

    function __tvm_ls_remote {
        wget -qO- "https://releases.hashicorp.com/terraform/" | grep '<a href=' | grep -oP '(?<=/terraform/)[^/]+' | tac
    }

    __tvm_command=""
    __tvm_selected_version=""
    for i in "$@"; do
        case $i in
            install)
                __tvm_command="install";
                __tvm_selected_version="$2"
                shift # past argument
                shift # past value
                ;;
            use)
                __tvm_command="use";
                __tvm_selected_version="$2"
                shift # past argument
                shift # past value
                ;;
            use-tmp)
                __tvm_command="use-tmp";
                __tvm_selected_version="$2"
                shift # past argument
                shift # past value
                ;;
            ls)
                __tvm_ls
                return $?
                ;;
            ls-remote)
                __tvm_ls_remote
                return $?
                ;;
            -*|--*)
                echo "Unknown option $i"
                return 1
                ;;
            *)
                ;;
        esac
    done

    if [[ "${__tvm_command}" = "install" ]]; then
        __tvm_install ${__tvm_selected_version}
        return $?
    elif [[ "${__tvm_command}" = "use" ]]; then
        __tvm_use ${__tvm_selected_version}
        return $?
    elif [[ "${__tvm_command}" = "use-tmp" ]]; then
        __tvm_use_tmp ${__tvm_selected_version}
        return $?
    else
        echo "No valid command provided in '${@}'."
        echo
        __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} --help && return 1;
    fi  
}


function terraform_export_new_resource_addresses {
    #Help:
    #>>
    # usage:                terraform_export_new_resource_addresses
    # description:          Prints a list of resources to be created on the next terraform apply
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;

    terraform plan -refresh=false -no-color | grep --color=never "will be created" | sed -n 's/.*# \([^ ]*\) will be created.*/\1/p'
}


function terraform_resource_values {
    #Help:
    #>>
    # usage:                terraform_resource_values [RESOURCE_ADDRESS] [DELIMITER] [SEARCH VALUES]
    # description:          Returns a string of the reuquested resource search values separated by a provided delimiter
    # examples:             "terraform_resource_values aws_route.nat-gw-route[0] _ route_table_id destination_cidr_block"
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;

    local _address="${1}"
    local _delimiter="${2}"
    local _values="${@:3}"
    
    # Write current state to a JSON file (if the JSON file does not yet exist)
    local _json_file=".terraform/terraform_show.json"
    if [[ ! -f "${_json_file}" ]]; then
        terraform show -no-color -json > "${_json_file}"
    fi
    # Parse the JSON file for any requestsed values
    local _return_values=""
    for _value in ${_values}; do
        _return_value=$(jq -r --arg addr "${_address}" --arg value "${_value}" '.values.root_module.resources[] | select(.address == $addr) | .values | .[$value]' "${_json_file}")
        if [[ -z "${_return_values}" ]]; then
            _return_values+="${_return_value}"
        else
            _return_values+="${_delimiter:?}${_return_value:?}"
        fi
    done
    echo "${_return_values}"
}


function terraform_generate_tf_import_script {
    #Help:
    #>>
    # usage:                terraform_generate_tf_import_script [PATH]
    # description:          Generates a script that can be used to import resources from another terraform project.
    # description:
    # description:          > Note:
    # description:          >   This cannot work for all resources, hence the commands are written to a script.
    # description:          >   Ensure you review the script and adjust for resources that are unable to be imported via a simple resource ID.
    # description:          >   Details of the current state of the resources can be found in the '.terraform/terraform_show.json' file
    # description:          >   generated in the source project.
    # examples:             "terraform_generate_tf_import_script /path/to/another/terraform/project"
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;

    local _source_project_path
    local _new_resources
    local _resource_id
    _source_project_path="${1}"

    # Fetch a list of new resources in this TF project
    _new_resources=$(terraform_export_new_resource_addresses)

    # Write fresh import and removal scripts
    mkdir -p .terraform
    echo '#!/usr/bin/env bash' > "$(pwd)/.terraform/import-resources.sh"
    mkdir -p "${_source_project_path}/.terraform"
    echo '#!/usr/bin/env bash' > "${_source_project_path}/.terraform/remove-resources.sh"

    # Build a list of value overrides for resources that do not use id for importing the resource
    declare -A value_overrides=(
        [aws_route]="_ route_table_id destination_cidr_block"
        [aws_route_table_association]="/ subnet_id route_table_id"
    )

    # Loop over all discovered new resources
    for resource_address in ${_new_resources}; do
        echo "Processing resource '${resource_address}'"

        # Check for resource type overrides
        local _state_values="_ id"
        local _resource_type="${resource_address%%.*}"
        if [[ -n "${value_overrides[$_resource_type]}" ]]; then
            _state_values="${value_overrides[$_resource_type]}"
        fi

        # Check other TF project for a current resource ID
        pushd "${_source_project_path:?}" > /dev/null || { echo "Failed to change directory."; return 1; }
        _resource_id=$(terraform_resource_values "${resource_address:?}" ${_state_values})
        popd > /dev/null || { echo "Failed to change directory."; return 1; }

        if [[ -z ${_resource_id} ]]; then
            echo "  - ERROR! Unable to find existing resource in this project '${_source_project_path}'"
            continue
        fi
        echo "  - Found existing resource ID - ${_resource_id:?}"

        # Write import command to script
        echo "terraform import ${resource_address:?} ${_resource_id:?}" >> .terraform/import-resources.sh
        echo "  - Resource import command written to $(pwd)/.terraform/import-resources.sh"

        # Write state remove command to script in other project
        pushd "${_source_project_path:?}" > /dev/null || { echo "Failed to change directory."; return 1; }
        echo "terraform state rm ${resource_address:?}" >> .terraform/remove-resources.sh
        echo "  - Resource removal command written to $(pwd)/.terraform/remove-resources.sh"
        popd > /dev/null || { echo "Failed to change directory."; return 1; }
    done
}

terraform_init_aws_s3_env() {
    #Help:
    #>>
    # usage:                terraform_init_aws_s3_env [OPTIONS]
    # options:              --type=<string>                                     Type of Terraform project (Options: [global-core, env-core, svc-core, env-deploy])
    # examples:             "terraform_init_aws_s3_env"                         Initialise this terraform project for env-core
    # examples:             "terraform_init_aws_s3_env --type=env-deploy"       Initialise this terraform project for env-deploy
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;

    local __type=""
    while [[ $# -gt 0 ]]; do
        case "$1" in
            --type=*)
                __type="${1#*=}"
                shift
                ;;
            *)
            echo "Unknown option: $1"
            return 1
            ;;
        esac
    done

    export CI_ENVIRONMENT_NAME="${ENVIRONMENT_NAME}"
    export AWS_ACCOUNT_ID=$(aws sts get-caller-identity --output text | awk '{print $1}')

    if [ "X${__type:-}" = "X" ]; then
        echo "Select a project type:"
        local project_types=(
            "global-core"
            "svc-core"
            "env-core"
            "env-deploy"
        )
        select __type in "${project_types[@]}"; do
            echo
            echo "Selected project type: '${__type}'"
            break
        done
    fi

    __terraform_s3_bucket="tf-${AWS_ACCOUNT_ID:?}-core-state"
    if [[ "${__type:?}" == "global-core" ]]; then
        __terraform_s3_bucket="tf-${AWS_ACCOUNT_ID:?}-core-state"
    elif [[ "${__type:?}" == "svc-core" ]]; then
        __terraform_s3_bucket="tf-${AWS_ACCOUNT_ID:?}-svc-state"
    elif [[ "${__type:?}" == "env-core" ]]; then
        __terraform_s3_bucket="tf-${AWS_ACCOUNT_ID:?}-core-state"
    elif [[ "${__type:?}" == "env-deploy" ]]; then
        __terraform_s3_bucket="tf-${AWS_ACCOUNT_ID:?}-deploy-state"
    else
        echo "ERROR: Unable to determine the terraform state bucket to use!"
        return 1
    fi

    echo
    echo "Initializing terraform using S3 bucket '${__terraform_s3_bucket:?}'"
    echo

    echo rm -rf ./.terraform
    rm -rf ./.terraform
    echo

    echo terraform init -backend-config="bucket=${__terraform_s3_bucket}"
    terraform init -backend-config="bucket=${__terraform_s3_bucket}"
    echo

    if [[ ! -z "${CI_ENVIRONMENT_NAME}" && "${__type:?}" != "global-core" && "${__type:?}" != "svc-core" ]]; then
        echo terraform workspace select "${CI_ENVIRONMENT_NAME}"
        terraform workspace select "${CI_ENVIRONMENT_NAME}"
        [[ $? -gt 0 ]] && echo terraform workspace new "${CI_ENVIRONMENT_NAME}"
        echo
    fi

    if [[ -n "${ENVIRONMENT_TYPE}" ]]; then
        export TF_VAR_environment_type="${ENVIRONMENT_TYPE:?}"
    fi
    if [[ -n "${AWS_REGION}" ]]; then
        export TF_VAR_aws_region="${AWS_REGION:?}"
    fi
    if [[ -n "${CUSTOMER}" ]]; then
        export TF_VAR_customer="${CUSTOMER:?}"
    fi
}

terraform_init_gitlab_state_env() {
    #Help:
    #>>
    # usage:                terraform_init_gitlab_state_env [OPTIONS]
    # options:              --env=<string>                                                  The environment name (Eg. prod,test,staging)
    # options:              --type=<string>                                                 Type of Terraform project (Options: [global-core, env-core, svc-core, env-deploy])
    # examples:             "terraform_init_gitlab_state_env --env=prod"                    Initialise this terraform project for the prod env
    # examples:             "terraform_init_gitlab_state_env --env=prod --type=env-deploy"  Initialise this terraform project for env-deploy
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;

    local __environment=""
    local __type=""
    while [[ $# -gt 0 ]]; do
        case "$1" in
            --env=*)
                __environment="${1#*=}"
                shift
                ;;
            --type=*)
                __type="${1#*=}"
                shift
                ;;
            *)
            echo "Unknown option: $1"
            return 1
            ;;
        esac
    done

    if [[ -z "${__environment:-}" ]]; then
        echo "You must specify an environment with the '--env' param."
        __read_function_help "${BASH_SOURCE[0]}" "${FUNCNAME[0]}" --help && return 1;
    fi
    
    # Assume the gitlab role and 
    gitlab_assume_token
    echo

    if [ "X${__type:-}" = "X" ]; then
        echo "Select a project type:"
        local project_types=(
            "global-core"
            "svc-core"
            "env-core"
            "env-deploy"
        )
        select __type in "${project_types[@]}"; do
            echo
            echo "Selected project type: '${__type}'"
            break
        done
    fi

    # Fetch origin path
    local __path_with_namespace=$(git remote get-url origin | sed -E 's/^(https:\/\/[^\/]+\/|git@[^:]+:)//' | sed 's/\.git$//')
    # Fetch project ID from origin path
    local __project=$(python3 -c "import urllib.parse;print(urllib.parse.quote('${__path_with_namespace:?}', safe=''))")
    # Define the TF state name
    local __terraform_state_name="tf-${__environment:?}-${__type:?}-state"

    echo
    echo "Initializing terraform using gitlab state name '${__terraform_state_name:?}' in project '${__path_with_namespace:?}'"
    echo

    echo rm -rf ./.terraform
    rm -rf ./.terraform
    echo
    
    echo terraform init \
        -backend-config="address=${CI_API_V4_URL:?}/projects/${__project:?}/terraform/state/${__terraform_state_name:?}" \
        -backend-config="lock_address=${CI_API_V4_URL:?}/projects/${__project:?}/terraform/state/${__terraform_state_name:?}/lock" \
        -backend-config="unlock_address=${CI_API_V4_URL:?}/projects/${__project:?}/terraform/state/${__terraform_state_name:?}/lock" \
        -backend-config="username=${CI_REGISTRY_USER:?}" \
        -backend-config="password=${CI_JOB_TOKEN:?}" \
        -backend-config="lock_method=POST" \
        -backend-config="unlock_method=DELETE" \
        -backend-config="retry_wait_min=5"
    terraform init \
        -backend-config="address=${CI_API_V4_URL:?}/projects/${__project:?}/terraform/state/${__terraform_state_name:?}" \
        -backend-config="lock_address=${CI_API_V4_URL:?}/projects/${__project:?}/terraform/state/${__terraform_state_name:?}/lock" \
        -backend-config="unlock_address=${CI_API_V4_URL:?}/projects/${__project:?}/terraform/state/${__terraform_state_name:?}/lock" \
        -backend-config="username=${CI_REGISTRY_USER:?}" \
        -backend-config="password=${CI_JOB_TOKEN:?}" \
        -backend-config="lock_method=POST" \
        -backend-config="unlock_method=DELETE" \
        -backend-config="retry_wait_min=5"
    echo

    if [[ -n "${__environment}" ]]; then
        export TF_VAR_environment="${__environment:?}"
    fi
}
