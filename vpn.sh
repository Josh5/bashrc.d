#!/usr/bin/env bash
###
# File: openvpn.sh
# Project: bashrc.d
# File Created: Friday, 14th October 2022 12:22:57 pm
# Author: Josh Sunnex (jsunnex@gmail.com)
# -----
# Last Modified: Friday, 14th October 2022 12:29:21 pm
# Modified By: Josh Sunnex (jsunnex@gmail.com)
###


if command -v openvpn &> /dev/null; then
    function openvpn_connect {
        __conf_array='';
        __selected='';
        i=1
        k=0
        echo
        for vpn_config in ${HOME}/Documents/OpenVPN/*.ovpn; do
            config=$(basename $(echo ${vpn_config} | sed 's/\.ovpn$//') ); 
            if [[ ${config} == ${1} ]]; then
                __selected=${i};
            fi
            echo "      ${i}) ${config}";
            __conf_array[ $k ]="${config}" 
            ((i++))
            ((k++))
        done
        # If we were passed a config name as an argument to this function, then just set it here
        if [[ ! -z ${__selected} ]]; then
            echo
            echo "Using config ${__selected}) ${__conf_array[${__selected}]}";
        else
            echo
            read -p "Select a config from the list above: " __selected
        fi
        numbers='^[0-9]+$'
        if [[ "${__selected}" -le "${#__conf_array[@]}" && ${__selected} =~ ${numbers} && "${__selected}" != "0" ]]; then
            (( __selected-- ))
            selected_config=${__conf_array[${__selected}]}
            
            echo
            echo "Provide sudo password..."
            sudo echo
            echo "Connecting to config to ${selected_config}..."
            sudo openvpn --config "${HOME}/Documents/OpenVPN/${selected_config}.ovpn"
        else
            echo; echo "That was not a valid selection."; echo "No config selected.";
            return;
        fi
    }
fi
