
# Fix folder to have correct web permissions
function fix_web_permissions {
    _UID=www-data;
    _GID=www-data;
    if [[ !  -z  ${1}  ]]; then
        _UID=${1};
        if [[ !  -z  ${2}  ]]; then
            _GID=${2};
        fi
    fi
    sudo chown -Rf ${_UID}:${_GID} ./
    sudo find ./ -type d -exec sudo chmod 775 -f {} \;
    sudo find ./ -type f -exec sudo chmod 664 -f {} \;
    sudo find ./ -type d -name '.hg' -exec sudo chown -Rf $USER:$USER {} \;
    sudo find ./ -type d -name '.git' -exec sudo chown -Rf $USER:$USER {} \;
}

# Show apache logs (include any error logs in the tree if specified)
function tailapache {
    ARGS="-fn50 /var/log/apache2/error.log";
    if [[ "${1}" =~ "-f" ]]; then
        ERROR_LOGS=$(find . -name "laravel*.log" -o -name "error.log" -o -name "error_log");
        ARGS="${ARGS} ${ERROR_LOGS}";
    fi
    tail ${ARGS} | sed 's/\\n/\n/g; s/\\t/  /g;';
}
function taildocker {
    #>>
    # usage:                taildocker [OPTIONS]
    # options:              --error            -           Show error logs
    # options:              --access           -           Show access logs
    # options:              --curl             -           Show curl debugging
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;
    APPDATADIR=/home/${USER}/appdata
    ARGS="-fn5 ";
    PROJECT="${PWD##*/}";
    # See if we can actually find a container with a mount point including this absolute path
    # First check all containers
    for container_name in $(docker ps -a --format '{{.Names}}'); do
        if [[ ! -z $(docker inspect -f '{{ json .Mounts }}' ${container_name} | python -m json.tool | grep "${PWD}") ]]; then
            PROJECT=${container_name};
            break;
        fi
    done
    # Now run it again checking only running containers. We will priorities these
    for container_name in $(docker ps --format '{{.Names}}'); do
        if [[ ! -z $(docker inspect -f '{{ json .Mounts }}' ${container_name} | python -m json.tool | grep "${PWD}") ]]; then
            PROJECT=${container_name};
            break;
        fi
    done
    PROJECT_DIR=${APPDATADIR}/${PROJECT}/log/
    echo "Reading logs from ${PROJECT_DIR}";
    PARAMS=$@;
    if [[ -z ${1} ]]; then
        PARAMS='--error --access'; # show all logs by default
    fi
    for arg in ${PARAMS}; do
        if [[ "${arg}" == "--error" || "${arg}" == "-e" ]]; then
            ARGS="${ARGS} /var/log/apache2/error.log";
            if [ -d "${PROJECT_DIR}" ]; then
                ERROR_LOGS=$(find ${PROJECT_DIR} -name "error.log");
                ARGS="${ARGS} ${ERROR_LOGS}";
            fi
            ERROR_LOGS=$(find . -name "laravel*.log" -o -name "error.log" -o -name "error_log");
            ARGS="${ARGS} ${ERROR_LOGS}";
            continue;
        fi
        if [[ "${arg}" == "--curl" || "${arg}" == "-c" ]]; then
            ERROR_LOGS=$(find . -name "curl.log");
            ARGS="${ARGS} ${ERROR_LOGS}";
            continue;
        fi
        if [[ "${arg}" == "--access" || "${arg}" == "-a"  ]]; then
            if [ -d "${PROJECT_DIR}" ]; then
                ERROR_LOGS=$(find ${PROJECT_DIR} -name "access.log");
                ARGS="${ARGS} ${ERROR_LOGS}";
            fi
            ERROR_LOGS=$(find . -name "laravel*.log");
            ARGS="${ARGS} ${ERROR_LOGS}";
            continue;
        fi
    done
    tail ${ARGS} | sed 's/\\n/\n/g; s/\\t/  /g;';
}

function tor_browser {
    docker run -it --rm \
        -v /tmp/.X11-unix:/tmp/.X11-unix \
        -e DISPLAY=unix$DISPLAY \
        -v /dev/snd:/dev/snd --privileged \
        --name tor-browser \
        jess/tor-browser
}

function curl_load {
    #Help:
    #>>v2
    # Usage:      curl_load [OPTIONS] <curl_command> [<curl_args>]
    #
    # Description:
    #   Runs multiple curl commands in parallel and measures the average time taken.
    #
    # Options:
    #   --workers               Number of parallel curl workers to run (default: '2')
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2>/dev/null && return

    (
        # Disable job control messages in this subshell
        set +m

        local request_workers=2
        local run_counter=1
        local average_run_time=""
        local tmp_dir=$(mktemp -d)

        # Parse options
        while [[ $# -gt 0 ]]; do
            case "$1" in
            --workers=*)
                request_workers="${1#*=}"
                shift
                ;;
            --workers)
                request_workers="$2"
                shift 2
                ;;
            --)
                shift
                break
                ;;
            *)
                break
                ;;
            esac
        done

        # First argument is the curl command
        local curl_command=$1
        shift
        if [ "${curl_command:-}" != "curl" ]; then
            echo "ERROR: Invalid curl command '${curl_command:-}'. Exit!"
            echo
            curl_load --help
            exit 1
        fi

        # Remaining arguments are curl options
        local curl_args=("$@")

        run_requests() {
            local total_time=0

            for ((i = 1; i <= ${request_workers:?}; i++)); do
                (
                    start_time=$(date +%s.%N)
                    ${curl_command:?} "${curl_args[@]}" >/dev/null 2>&1
                    end_time=$(date +%s.%N)
                    elapsed_time=$(echo "$end_time - $start_time" | bc -l)
                    echo $elapsed_time >"${tmp_dir:?}/time_$i"
                ) &>/dev/null &
            done

            wait

            for ((i = 1; i <= ${request_workers:?}; i++)); do
                if [[ -f "${tmp_dir:?}/time_$i" ]]; then
                    elapsed_time=$(cat "${tmp_dir:?}/time_$i")
                    total_time=$(echo "$total_time + $elapsed_time" | bc -l)
                    rm "${tmp_dir:?}/time_$i"
                fi
            done
            local avg_time_s=$(echo "$total_time / $request_workers" | bc -l)
            local avg_time_ms=$(echo "$avg_time_s * 1000" | bc -l)
            if (($(echo "$avg_time_ms >= 1000" | bc -l))); then
                average_run_time=$(printf "%.3f" "$avg_time_s")s
            else
                average_run_time=$(printf "%.3f" "$avg_time_ms")ms
            fi

        }

        check_status() {
            local status
            status=$(${curl_command:?} "${curl_args[@]}" -w '%{http_code}' -o /dev/null -s)
            if [[ "$status" -lt 200 || "$status" -ge 300 ]]; then
                echo -e "\nNon-2xx status code received: $status. Exiting loop."
                exit 1
            fi
            echo -ne "  Successfully ran ${request_workers:?} requests. Status Code: $status - Avg Time: ${average_run_time} - Run Counter: $run_counter\r"
        }

        echo
        echo "Running parallel test with provided curl command..."
        while true; do
            run_requests
            check_status
            sleep 1
            ((run_counter++))
        done
    )
}
