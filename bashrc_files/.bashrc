# This file needs to be installed in ~/.bashrc
# Install this with:
#
#     mv ~/.bashrc ~/.bashrc.bkup && ln -s ~/bashrc.d/bashrc_files/.bashrc
#

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# Source global definitions
if [ -f /etc/bashrc ]; then
  . /etc/bashrc
fi

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# Ensure .local/bin is exported to PATH
if [ -d "${HOME}/.local/bin" ]; then
    export PATH="${PATH}:${HOME}/.local/bin"
fi

# Add my additional bash scripts if they are available
if [ -d ${HOME}/bashrc.d -a -r ${HOME}/bashrc.d -a -x ${HOME}/bashrc.d ]; then
  # export BASH_IT=true; # Set this to 'false' to disable bash-it
  export BASHRCD_CONFIG=linux; # Set this to 'WSL' for windows bash
  export WSL_DOCKER_AUTO_START=false; # Set this to 'true' for dockerd to autostart with wsl
  export DISABLE_SSH_AGENT_CONFIG=false # Set this to 'true' to disable the ssh agent config part of this project
  for i in ${HOME}/bashrc.d/*.sh; do
    if [ -f $i ]; then
      . $i
    fi
  done
fi
