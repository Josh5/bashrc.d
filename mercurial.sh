# @Author: Josh S
# @Date:   2018-08-07 11:13:07
# @Last Modified by:   Josh S
# @Last Modified time: 2018-09-14 08:17:15

function hg_clean_dir {
    RM_FILES=$(hg status -n);
    if [[ ! -z "${RM_FILES}" ]]; then
        echo "${RM_FILES}";
        echo
        read -p "Are you sure you want to delete the files listed above? (y/n) " AN;
        if [[ "${AN}" != "N" && "${AN}" != "n" ]]; then
            rm -rf ${RM_FILES};
        fi
    else
        echo "Found no files to clear out. Doing nothing...";
    fi
}
