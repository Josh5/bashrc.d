###
# File: ruby.sh
# Project: bashrc.d
# File Created: Thursday, 25th March 2021 8:24:23 pm
# Author: Josh.5 (jsunnex@gmail.com)
# -----
# Last Modified: Friday, 30th September 2022 12:11:04 am
# Modified By: Josh Sunnex (jsunnex@gmail.com)
###

if [[ $(command -v gem) ]]; then
    NPM_PACKAGES="${HOME}/.local";

    export GEM_HOME="${HOME}/.gem/gems"
    export PATH="${HOME}/.gem/gems/bin:${PATH}"
fi

if [[ $(command -v rbenv) ]]; then
    # See https://github.com/rbenv/rbenv#readme for installation
    eval "$(rbenv init - bash)"
fi
