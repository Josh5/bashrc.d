function tiz {
    tizbusname=$(dbus-send --session \
        --dest=org.freedesktop.DBus \
        --type=method_call \
        --print-reply \
        /org/freedesktop/DBus \
        org.freedesktop.DBus.ListNames \
        | grep tizonia \
        | awk -F'"' '{ print $2 }'
    );
    if [[ ! -z ${tizbusname} ]]; then

        if [[ ${1} && ! -z ${@:2} ]]; then
            tizonia-remote kill
            tizplay -d "${@:2}"
        else
            tizonia-remote ${@}
        fi
    else
        echo tizplay ${@}
    fi
}

function tizplay {
    if [[ ! -z $(which tizonia) ]]; then
        if [[ !  -z  ${@}  ]]; then
            mode="search";
            args="";
            for arg in $@; do
                if [[ ! "${arg}" =~ "--" ]]; then
                    args="${args} ${arg}";
                fi
                if [[ "${arg}" == "--playlist" ]]; then
                    mode="playlist";
                fi
                if [[ "${arg}" == "--channel" ]]; then
                    mode="channel";
                fi
                if [[ "${arg}" == "--video" ]]; then
                    mode="video";
                fi
                if [[ "${arg}" == "--videomix" ]]; then
                    mode="videomix";
                fi
                if [[ "${arg}" == "--mix" ]]; then
                    mode="mix";
                fi
            done
            _${mode}_tizplay ${args} ;
        else
            echo "You need to specify a search criteria";
        fi
    else
        echo "You will need to first install tizonia."
        echo "Try running 'snap install tizonia'."
    fi
}
function _search_tizplay {
    global_args="";
    SEARCHSTRING="";
    for arg in ${@}; do
        if [[ "-d -s -ds -sd" == *"${arg}"* ]]; then
            global_args="${global_args} ${arg}";
            continue;
        fi
        SEARCHSTRING="${SEARCHSTRING} ${arg}"
    done
    echo tizonia ${global_args} --youtube-audio-search "'${SEARCHSTRING}'"
    tizonia ${global_args} --youtube-audio-search "${SEARCHSTRING}"
}
function _playlist_tizplay {
    SEARCHSTRING=${@};
    echo tizonia --youtube-audio-playlist "'${SEARCHSTRING}'"
    tizonia --youtube-audio-playlist "${SEARCHSTRING}"
}
function _channel_tizplay {
    SEARCHSTRING=${@};
    echo tizonia --youtube-audio-channel-uploads "'${SEARCHSTRING}'"
    tizonia --youtube-audio-channel-uploads "${SEARCHSTRING}"
}
function _video_tizplay {
    SEARCHSTRING=${@};
    echo tizonia --youtube-audio-stream "${SEARCHSTRING}"
}
function _mix_tizplay {
    SEARCHSTRING=${@};
    echo tizonia --youtube-audio-mix "${SEARCHSTRING}"
}
function _videomix_tizplay {
    SEARCHSTRING=${@};
    echo tizonia --youtube-audio-mix "${SEARCHSTRING}"
}


function tizupgrade {
    PYTHON_DEPENDENCIES=" \
        fuzzywuzzy>=0.17.0 \
        gmusicapi>=11.1.1 \
        pafy>=0.5.4 \
        pycountry>=18.12.8 \
        python-levenshtein>=0.12.0 \
        soundcloud>=0.5.0 \
        spotipy>=2.4.4 \
        titlecase>=0.12.0 \
        youtube-dl>=2018.12.9 \
    "
    sudo -H python -m pip install --upgrade $PYTHON_DEPENDENCIES;
}
