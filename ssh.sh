# Ensure this distro is keeping the ssh socket alive between terminals
if [ "${DISABLE_SSH_AGENT_CONFIG:-}" != "true" ]; then
    if [ -x "$(command -v ssh-agent)" ] && [ -x "$(command -v ss)" ]; then
        MY_UUID="$(id -u)"
        MY_SSH_ENV_FILE="${HOME}/.ssh/ssh-agent.$(hostnamectl hostname).env"
        # Check if the ssh agent socket exists in my run user path. This was started on login and should be used first.
        if ss -xl | grep "/run/user/${MY_UUID:?}/ssh-agent.socket" > /dev/null; then
            echo "SSH_AUTH_SOCK=/run/user/${MY_UUID:?}/ssh-agent.socket; export SSH_AUTH_SOCK;" > "${MY_SSH_ENV_FILE}"
            echo "SSH_AGENT_PID=$(pgrep -fa -U "${MY_UUID:?}" ssh-agent | head -n1 | awk '{print $1}'); export SSH_AGENT_PID;" >> "${MY_SSH_ENV_FILE}"
        elif ss -xl | grep "/run/user/${MY_UUID:?}/keyring/ssh" > /dev/null; then
            echo "SSH_AUTH_SOCK=/run/user/${MY_UUID:?}/keyring/ssh; export SSH_AUTH_SOCK;" > "${MY_SSH_ENV_FILE}"
            echo "SSH_AGENT_PID=$(pgrep -fa -U "${MY_UUID:?}" ssh-agent | head -n1 | awk '{print $1}'); export SSH_AGENT_PID;" >> "${MY_SSH_ENV_FILE}"
        elif ss -xl | grep "/run/user/${MY_UUID:?}/keyring/.ssh" > /dev/null; then
            echo "SSH_AUTH_SOCK=/run/user/${MY_UUID:?}/keyring/.ssh; export SSH_AUTH_SOCK;" > "${MY_SSH_ENV_FILE}"
            echo "SSH_AGENT_PID=$(pgrep -fa -U "${MY_UUID:?}" ssh-agent | head -n1 | awk '{print $1}'); export SSH_AGENT_PID;" >> "${MY_SSH_ENV_FILE}"
        # Else, check for a currently running instance of the agent with a socket in another path.
        elif ! pgrep -fa -U "${MY_UUID:?}" ssh-agent > /dev/null; then
            # Launch a new instance of the agent
            ssh-agent -s | sed 's/^echo/#echo/' &> "${MY_SSH_ENV_FILE:?}"
        elif [ -z "${SSH_AUTH_SOCK}" ]; then
            # Check for a currently running instance of the agent
            RUNNING_AGENT_COUNT="$(ps -ax | grep 'ssh-agent' | grep -v grep | wc -l | tr -d '[:space:]')"
            if [ "${RUNNING_AGENT_COUNT:-0}" = "0" ]; then
                # Launch a new instance of the agent
                ssh-agent -s | sed 's/^echo/#echo/' &> "${MY_SSH_ENV_FILE:?}"
                chmod 600 "${MY_SSH_ENV_FILE}"
            else
                # Not sure why we would get here.
                unset MY_SSH_ENV_FILE
            fi
        else
            echo "Something went wrong and the SSH agent is already running. Try killing that agent and then re-open the terminal."
            pgrep -fa -U "${MY_UUID:?}" ssh-agent
            unset MY_SSH_ENV_FILE
        fi
        # Read ssh agent env file
        if [ -f "${MY_SSH_ENV_FILE:-}" ]; then
            chmod 600 "${MY_SSH_ENV_FILE:?}"
            eval $(cat ${MY_SSH_ENV_FILE:?}) &> /dev/null
        fi
    fi
fi

# Mount directories through ssh:
function rumount {
    if [[ !  -z  ${1}  ]]; then
        # TODO: Check if mount exists
        sudo fusermount -u ~/sshmounts/${1};
    fi
}
function rmount {
    if [[ !  -z  ${1}  ]]; then
        REMOTE_PATH=/;
        if [[ !  -z  ${2}  ]]; then
            REMOTE_PATH=${2};
        fi
        mkdir -p ~/sshmounts/${1};
        echo sshfs ${1}:${REMOTE_PATH} ~/sshmounts/${1} -o allow_other ${@:3}
        sshfs ${1}:${REMOTE_PATH} ~/sshmounts/${1} -o allow_other ${@:3}
    fi
}

function ssh_tunnel_process {
    if [[ ! -z  ${1}  ]]; then
        LOG_FILE="/tmp/ssh_${1}.log";
        if [[ "${1}" == "all" ]]; then
            LOG_FILE="/tmp/ssh_*.log";
        fi
        RUNNING=$(ps awx | grep ${LOG_FILE} | grep -v grep | awk '{print $1}');
        if [[ ! -z  ${2} && "${2}" == "stop" ]]; then
            kill ${RUNNING};
        else
            if [[ ${RUNNING} ]]; then
                ssh_tunnel_process ${1} stop;
            fi
            echo ssh -C -f -N ${1} ${@:2} -E ${LOG_FILE};
            ssh -C -f -N ${1} ${@:2} -E ${LOG_FILE};
        fi
    else
        exit 1;
    fi
}

function __ssh_select_key {
    local __conf_array='';
    local __selected='';
    local i=1
    local k=0
    echo
    for ssh_key in ${HOME}/.ssh/keys/id_*.pub; do
        key=$(basename $(echo ${ssh_key} | sed 's/\.pub$//') ); 
        if [[ ${key} == ${1} ]]; then
            __selected=${i};
        fi
        echo "      ${i}) ${key}";
        __conf_array[ $k ]="${key}" 
        ((i++))
        ((k++))
    done
    echo
    read -p "Select a key from the list above: " __selected
    local numbers='^[0-9]+$'
    my_ssh_selected_key=""
    if [[ "${__selected}" -le "${#__conf_array[@]}" && ${__selected} =~ ${numbers} && "${__selected}" != "0" ]]; then
        (( __selected-- ))
        my_ssh_selected_key=${__conf_array[${__selected}]}
    else
        echo; echo "That was not a valid selection."; echo "No key selected.";
        my_ssh_selected_key=""
    fi
}

function ssh_set_key {
    __conf_array='';
    __selected='';
    i=1
    k=0
    echo
    for ssh_key in ${HOME}/.ssh/keys/id_*.pub; do
        key=$(basename $(echo ${ssh_key} | sed 's/\.pub$//') ); 
        if [[ ${key} == ${1} ]]; then
            __selected=${i};
        fi
        echo "      ${i}) ${key}";
        __conf_array[ $k ]="${key}" 
        ((i++))
        ((k++))
    done
    # If we were passed a key name as an argument to this function, then just set it here
    if [[ ! -z ${__selected} ]]; then
        echo
        echo "Using key ${__selected}) ${__conf_array[${__selected}]}";
    else
        echo
        read -p "Select a key from the list above: " __selected
    fi
    numbers='^[0-9]+$'
    if [[ "${__selected}" -le "${#__conf_array[@]}" && ${__selected} =~ ${numbers} && "${__selected}" != "0" ]]; then
        (( __selected-- ))
        selected_key=${__conf_array[${__selected}]}
        echo; echo "Setting current key to ${HOME}/.ssh/keys/${selected_key}...";
        CWD=${PWD};
        rm -f ${HOME}/.ssh/id_*
        cd ${HOME}/.ssh/
        ln -s keys/${selected_key} ${selected_key}
        ln -s keys/${selected_key}.pub ${selected_key}.pub
        ln -sf keys/${selected_key} id_ed25519
        ln -sf keys/${selected_key}.pub id_ed25519.pub
        chmod 600 ./${selected_key}
        chmod 644 ./${selected_key}.pub
        #eval "$(ssh-agent -s)";
        ssh-add ${HOME}/.ssh/${selected_key};
        cd ${CWD};
    else
        echo; echo "That was not a valid selection."; echo "No key selected.";
        return;
    fi
}
