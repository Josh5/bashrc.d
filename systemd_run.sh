#!/usr/bin/env bash
###
# File: systemd_run.sh
# Project: bashrc.d
# File Created: Tuesday, 16th July 2024 10:59:30 am
# Author: Josh5 (jsunnex@gmail.com)
# -----
# Last Modified: Wednesday, 17th July 2024 3:28:43 pm
# Modified By: Josh5 (jsunnex@gmail.com)
###

systemd_run_update_desktop_launcher() {
    local launchers=()
    local selected_launcher
    local cpu_weight
    local cpu_quota
    local memory_max
    local memory_high
    local memory_low
    local systemd_run_unit
    local systemd_run_cmd_prefix
    local exec_command

    # Find desktop files in both locations, filtering out non-regular files
    mapfile -t launchers < <(
        find /usr/share/applications ~/.local/share/applications -type f -name "*.desktop" 2>/dev/null | sort
    )

    # Check if any launchers were found
    if [ ${#launchers[@]} -eq 0 ]; then
        echo "ERROR: No desktop launchers found." >&2 # Print to stderr
        return 1
    fi

    if ! mount | grep cgroup2 >/dev/null; then
        echo "ERROR: This is only compatible with systems running cgroups v2." >&2 # Print to stderr
        return 1
    fi

    if ! command -v fzf; then
        echo "ERROR: This function requires that you have the fzf command installed." >&2 # Print to stderr
        return 1
    fi

    # Use fzf to select a launcher
    selected_launcher=$(printf '%s\n' "${launchers[@]}" | fzf --prompt="Select a launcher: ")

    if [ -z "$selected_launcher" ]; then
        echo "No launcher selected. Exiting."
        return 1
    fi

    echo
    echo "Selected launcher: '${selected_launcher}'"
    echo
    cat "${selected_launcher:?}"

    # REF: https://manpages.ubuntu.com/manpages/bionic/man5/systemd.resource-control.5.html

    # Generate systemd-run prefix
    systemd_run_cmd_prefix="systemd-run --user --scope --collect -E BAMF_DESKTOP_FILE_HINT=${selected_launcher:?}"
    echo
    echo "  [CPUWeight=weight]"
    echo "      Assign the specified CPU time weight to the processes executed, if the unified control"
    echo "      group hierarchy is used on the system. These options take an integer value and control"
    echo "      the 'cpu.weight' control group attribute. The allowed range is 1 to 10000. Defaults to"
    echo "      100. For details about this control group attribute, see cgroup-v2.txt[2] and"
    echo "      sched-design-CFS.txt[4]. The available CPU time is split up among all units within one"
    echo "      slice relative to their CPU time weight."
    echo
    read -p "Specify a CPUWeight value (e.g. 100. Leave blank to not apply): " cpu_weight
    echo
    if [ -n "${cpu_weight:-}" ]; then
        systemd_run_cmd_prefix="${systemd_run_cmd_prefix:?} -p CPUWeight=${cpu_weight:?}"
    fi
    echo
    echo "  [CPUQuota=]"
    echo "      Assign the specified CPU time quota to the processes executed. Takes a percentage"
    echo "      value, suffixed with '%'. The percentage specifies how much CPU time the unit shall"
    echo "      get at maximum, relative to the total CPU time available on one CPU. Use values > 100%"
    echo "      for allotting CPU time on more than one CPU. This controls the 'cpu.max' attribute on"
    echo "      the unified control group hierarchy and 'cpu.cfs_quota_us' on legacy. For details"
    echo "      about these control group attributes, see cgroup-v2.txt[2] and"
    echo "      sched-design-CFS.txt[4]."
    echo "      "
    echo "      Example: CPUQuota=20% ensures that the executed processes will never get more than 20%"
    echo "      CPU time on one CPU."
    echo
    read -p "Specify a CPUQuota value (e.g. 25%. Leave blank to not apply): " cpu_quota
    echo
    if [ -n "${cpu_quota:-}" ]; then
        systemd_run_cmd_prefix="${systemd_run_cmd_prefix:?} -p CPUQuota=${cpu_quota:?}"
    fi
    echo
    echo "  [MemoryMax=bytes]"
    echo "      Specify the absolute limit on memory usage of the executed processes in this unit. If"
    echo "      memory usage cannot be contained under the limit, out-of-memory killer is invoked"
    echo "      inside the unit. It is recommended to use MemoryHigh= as the main control mechanism"
    echo "      and use MemoryMax= as the last line of defense."
    echo "      "
    echo "      Takes a memory size in bytes. If the value is suffixed with K, M, G or T, the"
    echo "      specified memory size is parsed as Kilobytes, Megabytes, Gigabytes, or Terabytes (with"
    echo "      the base 1024), respectively. Alternatively, a percentage value may be specified,"
    echo "      which is taken relative to the installed physical memory on the system. If assigned"
    echo "      the special value 'infinity', no memory limit is applied. This controls the"
    echo "      'memory.max' control group attribute. For details about this control group attribute,"
    echo "      see cgroup-v2.txt[2]."
    echo
    read -p "Specify a MemoryMax value (e.g. 1G. Leave blank to not apply): " memory_max
    echo
    if [ -n "${memory_max:-}" ]; then
        systemd_run_cmd_prefix="${systemd_run_cmd_prefix:?} -p MemoryMax=${memory_max:?}"
    fi
    echo
    echo "  [MemoryHigh=bytes]"
    echo "      Specify the high limit on memory usage of the executed processes in this unit. Memory"
    echo "      usage may go above the limit if unavoidable, but the processes are heavily slowed down"
    echo "      and memory is taken away aggressively in such cases. This is the main mechanism to"
    echo "      control memory usage of a unit."
    echo "      "
    echo "      Takes a memory size in bytes. If the value is suffixed with K, M, G or T, the"
    echo "      specified memory size is parsed as Kilobytes, Megabytes, Gigabytes, or Terabytes (with"
    echo "      the base 1024), respectively. Alternatively, a percentage value may be specified,"
    echo "      which is taken relative to the installed physical memory on the system. If assigned"
    echo "      the special value 'infinity', no memory limit is applied. This controls the"
    echo "      'memory.high' control group attribute. For details about this control group attribute,"
    echo "      see cgroup-v2.txt[2]."
    echo
    read -p "Specify a MemoryHigh value (e.g. 512M. Leave blank to not apply): " memory_high
    echo
    if [ -n "${memory_high:-}" ]; then
        systemd_run_cmd_prefix="${systemd_run_cmd_prefix:?} -p MemoryHigh=${memory_high:?}"
    fi
    echo
    echo "  [MemoryLow=bytes]"
    echo "      Specify the best-effort memory usage protection of the executed processes in this"
    echo "      unit. If the memory usages of this unit and all its ancestors are below their low"
    echo "      boundaries, this unit's memory won't be reclaimed as long as memory can be reclaimed"
    echo "      from unprotected units."
    echo "      "
    echo "      Takes a memory size in bytes. If the value is suffixed with K, M, G or T, the"
    echo "      specified memory size is parsed as Kilobytes, Megabytes, Gigabytes, or Terabytes (with"
    echo "      the base 1024), respectively. Alternatively, a percentage value may be specified,"
    echo "      which is taken relative to the installed physical memory on the system. This controls"
    echo "      the 'memory.low' control group attribute. For details about this control group"
    echo "      attribute, see cgroup-v2.txt[2]."
    echo
    read -p "Specify a MemoryLow value (e.g. 256K. Leave blank to not apply): " memory_low
    echo
    if [ -n "${memory_low:-}" ]; then
        systemd_run_cmd_prefix="${systemd_run_cmd_prefix:?} -p MemoryLow=${memory_low:?}"
    fi

    # Finally add the unit name to run as
    systemd_run_unit="--unit=$(basename "$selected_launcher" .desktop).scope"
    systemd_run_cmd_prefix="${systemd_run_cmd_prefix:?} ${systemd_run_unit:?}"

    # If the launcher is outside ~/.local/share/applications, copy it there
    if [[ ! $selected_launcher =~ ^$HOME/.local/share/applications/ ]]; then
        mkdir -p ~/.local/share/applications
        cp -f "$selected_launcher" ~/.local/share/applications/
        selected_launcher="$HOME/.local/share/applications/$(basename "$selected_launcher")"
        echo
        echo "Copied launcher to: '${selected_launcher}'"
        echo
    fi

    # Update .desktop file to run the systemd-run command
    local current_command="$(awk -F= '/^\[Desktop Entry\]/{flag=1; next} flag && /^Exec=/{print substr($0, index($0,$2)); exit}' ${selected_launcher:?})"
    local original_command="$current_command"
    if echo "$current_command" | grep -q "systemd-run"; then
        original_command="$(echo "$current_command" | grep "systemd-run" | sed -e "s/.*$systemd_run_unit //")"
    fi
    sed -i "/^\[Desktop Entry\]/,/^\[/{s|^Exec=.*|Exec=${systemd_run_cmd_prefix:?} ${original_command:?}|}" "${selected_launcher:?}"

    echo
    echo "Launcher updated:"
    echo
    cat "${selected_launcher:?}"
}

systemd_run_stats() {
    # Function to convert bytes to human-readable format with 3 decimal places
    convert_to_human_readable() {
        local bytes="$1"
        if ((bytes < 1024)); then
            printf "%d B\n" "$bytes"
        elif ((bytes < 1048576)); then
            printf "%.3f KB\n" "$(echo "scale=3; $bytes / 1024" | bc)"
        elif ((bytes < 1073741824)); then
            printf "%.3f MB\n" "$(echo "scale=3; $bytes / 1048576" | bc)"
        else
            printf "%.3f GB\n" "$(echo "scale=3; $bytes / 1073741824" | bc)"
        fi
    }

    # Function to get cgroup limits
    get_cgroup_limits() {
        local cgroup_path="$1"

        echo
        echo "CPU Limit ('cpu.max'):"
        (cat "$cgroup_path/cpu.max") | while IFS= read -r line; do
            echo "    $line"
        done
        echo
        echo "Memory Limit ('memory.max'):"
        (cat "$cgroup_path/memory.max") | while IFS= read -r line; do
            echo "    $line"
        done
        echo
        echo "IO Limit ('io.max'):"
        (cat "$cgroup_path/io.max") | while IFS= read -r line; do
            echo "    $line"
        done

        echo
        echo "CPU Usage ('cpu.stat'):"
        (cat "$cgroup_path/cpu.stat") | while IFS= read -r line; do
            echo "    $line"
        done

        echo
        echo "Memory Usage ('memory.current'):"
        (convert_to_human_readable $(cat "$cgroup_path/memory.current")) | while IFS= read -r line; do
            echo "    $line"
        done

        echo
        echo "IO Usage ('io.stat'):"
        (cat "$cgroup_path/io.stat") | while IFS= read -r line; do
            echo "    $line"
        done
    }

    # Function to display cgroup details
    display_cgroup_details() {
        local scope="$1"
        local cgroup_path="/sys/fs/cgroup/$scope"
        local base_scope="${scope##*/}" 

        clear
        echo "Resource usage for /sys/fs/cgroup/$scope:"
        echo "(press ESC to go back)"
        get_cgroup_limits "$cgroup_path"

        if [ -n "${base_scope:-}" ]; then
            echo
            echo "Systemd Scope ('systemctl'):"
            systemctl --user status ${base_scope:?} --lines 1 --no-pager
        fi

        echo
        echo "Processes ('cgroup.procs'):"
        local count=0
        local term_width=$(tput cols)
        local cmd_width=$((term_width - 30))
        printf "   %s %8s %s %*s %3s %s" "PID" "PPID" "CMD" "$((cmd_width + 2))" "%MEM" "%CPU"
        echo
        (cat "$cgroup_path/cgroup.procs") | while IFS= read -r pid; do
            if ((count >= 10)); then
                echo "    ...and more"
                break
            fi
            echo "$(ps -p "$pid" -o pid,ppid,cmd:$cmd_width,%mem,%cpu --no-headers)"
            ((count++))
        done
    }

    # Function to display and select cgroups using systemd-cgls
    select_cgroup() {
        local parent_scope="$1"

        while true; do
            clear
            echo "Select a slice (use arrow keys to navigate, ENTER to select, ESC to exit):"
            local scopes=($(ls -d /sys/fs/cgroup/$parent_scope/*))
            local scope=$(printf "%s\n" "${scopes[@]}" | fzf --height 50% --border --header="Select a slice (press ESC to exit, ENTER to select):")

            if [ -z "$scope" ]; then
                return # Esc pressed, go back to parent
            fi

            scope=${scope#/sys/fs/cgroup/}
            while true; do
                clear
                display_cgroup_details "$scope"

                read -n 1 -t 1 key
                if [[ $key == $'\e' ]]; then
                    break # Esc pressed, go back to parent
                fi
            done
        done
    }

    select_cgroup "user.slice/user-$(id -u).slice/user@$(id -u).service/app.slice"
    clear
}
