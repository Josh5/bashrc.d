
Simply add the following to your ~/.bashrc file

```
if [ -d ${HOME}/bashrc.d -a -r ${HOME}/bashrc.d -a -x ${HOME}/bashrc.d ]; then
  export BASH_IT=true; # Set this to 'false' to disable bash-it
  export BASHRCD_CONFIG=linux; # Set this to 'WSL' for windows bash
  export WSL_DOCKER_AUTO_START=true; # Set this to 'true' for dockerd to autostart with wsl
  for i in ${HOME}/bashrc.d/*.sh; do
    if [ -f $i ]; then
      . $i
    fi
  done
fi
```
