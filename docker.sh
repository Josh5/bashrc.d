
# WSL docker enables buildkit by default. I dont want to configure this every installation...
if [[ "${BASHRCD_CONFIG}" == "WSL" ]]; then
    ## Disable buildkit (I would prefer to see what its doing when developing)
    export DOCKER_BUILDKIT=0
fi


docker_exec () {
    #Help:
    #>>
    # usage:                docker_exec <RUNNING_CONTAINER_NAME> [OPTIONS]
    # description:          Attach to a shell inside a running docker container.
    # options:              --workdir=<path>            -       Specify the working directory inside the container. (Default: /)
    # options:              -u, --user                  -       Attach to the shell as as the current user ID inside the container instead of root.
    # examples:             "docker_exec ubuntu -u"     -       Attach to shell inside the container as the current user
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;

    if [[ ! -z ${1} ]]; then
        __docker_env_command=""
        __docker_env_working_dir="--workdir=/";
        __docker_env_command_args="";
        # Get Args
        for arg in ${@:2}; do
            if [[ "${arg}" == "--workdir" || "${arg}" == "-w" ]]; then
                __docker_env_working_dir=""; # Will add ${arg} to ${__docker_env_command_args} below
            fi
            if [[ "${arg}" == "-u" ]]; then
                __docker_env_command_args="${__docker_env_command_args} --user=$(id -u):$(id -g)";
                continue;
            fi
            __docker_env_command_args="${__docker_env_command_args} ${arg}";
        done
        echo "Searching for docker container by name containing '${1}'...";
        # Find a container running with this name or with this string in it's name
        __docker_env_container_name=$(docker ps --format '{{.Names}}' | sort | grep ${1} | head -n 1);
        if [[ -z ${__docker_env_container_name} ]]; then
            echo "No results were found. Check that the container you are looking for is running...";
            echo 
            docker ps;
            return
        fi
        if [[ -z ${__docker_env_command} ]]; then
            # Default to running sh shell
            __docker_env_command="sh"
            # Check if container supports bash
            if [[ ! -z $(docker exec ${__docker_env_container_name} sh -c "command -v bash") ]]; then
                __docker_env_command="bash"
            fi
        fi
        # Setup console params:
        __docker_env_console_params="-e COLUMNS='$(tput cols)' -e LINES='$(tput lines)'";
        echo "Connecting to ${__docker_env_container_name} docker container";
        echo "   - docker exec -ti ${__docker_env_working_dir} ${__docker_env_command_args} ${__docker_env_container_name} ${__docker_env_command}";
        echo 
        docker exec ${__docker_env_console_params} -ti ${__docker_env_working_dir} ${__docker_env_command_args} ${__docker_env_container_name} ${__docker_env_command}
    fi
}

docker_clear_orphaned_images () {
    docker image prune
    docker rmi $(docker images | grep "<none>" | awk '{print $3}')
}

docker_cleanup () {
    docker volume prune
    docker volume rm $(docker volume ls -q -f 'dangling=true')
    docker_clear_orphaned_images
}

docker_update_all_containers () {
    if [[ -f ${HOME}/.docker/updatescript.sh ]]; then 
        chmod +x ${HOME}/.docker/updatescript.sh;
        ${HOME}/.docker/updatescript.sh;
    fi
}

docker_run_sh () {
    #Help:
    #>>
    # usage:                docker_run_sh [OPTIONS]
    # description:          Run a docker container and attach a shell. By default will run 'sh' but this can be modified with the '--exec' option.
    # options:              --exec=<command>                                              -       Specify the command to execute in the container
    # options:              --image=<docker image>                                        -       Specify the docker image to run bash inside
    # options:              --workdir=<dir>                                               -       Location to execute command in
    # options:              --passthrough                                                 -       Passthrough the current working directory to the docker container
    # options:              --user                                                        -       Run as your user rather than root
    # examples:             "docker_run_sh --image=node --workdir=pkg"                    -       Run shell in the 'pkg' subdirectory using the node docker image
    # examples:             "docker_run_sh --image=node --passthrough"                    -       Run shell with the same directory structure as PWD using the node docker image
    # examples:             "docker_run_sh --image=node --entrypoint=''"                  -       Run shell using the node docker image disabling the default entrypoint
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;
    if [[ !  -z  ${1}  ]]; then
        local docker_run_sh_working_dir="--workdir=/";
        local docker_run_sh_command="sh";
        local docker_run_sh_image=""
        local docker_run_sh_extra_params="";
        # Get Args
        for arg in ${@}; do
            if [[ "${arg}" =~ "--exec" ]]; then
                docker_run_sh_command=$(echo ${arg} | awk -F'=' '{print $2}');
                continue;
            fi
            if [[ "${arg}" =~ "--image" ]]; then
                docker_run_sh_image=$(echo ${arg} | awk -F'=' '{print $2}');
                continue;
            fi
            if [[ "${arg}" == "--workdir" || "${arg}" == "-w" ]]; then
                docker_run_sh_working_dir="";
            fi
            if [[ "${arg}" == "--passthrough" ]]; then
                docker_run_sh_extra_params="${docker_run_sh_extra_params} --volume=${PWD}:${PWD}:rw";
                if [[ ! -z ${docker_run_sh_working_dir} ]]; then
                    docker_run_sh_working_dir="--workdir=${PWD}";
                fi
                continue;
            fi
            if [[ "${arg}" == "--user" || "${arg}" == "-u" ]]; then
                docker_run_sh_extra_params="${docker_run_sh_extra_params} --user=$(id -u):$(id -g)";
                continue;
            fi
            docker_run_sh_extra_params="${docker_run_sh_extra_params} ${arg}";
        done
        if [[ -z ${docker_run_sh_image} ]]; then
            echo "Requires --image to be set" 
            __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} --help && return 1;
        fi
        local docker_repo=$(echo ${docker_run_sh_image} | awk -F: '{print $1}');
        local docker_tag=$(echo ${docker_run_sh_image} | awk -F: '{print $2}');
        __image_info=$(docker images -a --filter "reference=${docker_repo}:${docker_tag:-latest}" | grep "${docker_repo}" | grep -m1 "${docker_tag}");
        if [[ ! -z ${__image_info} ]]; then
            echo "Image information:";
            echo "   - ${__image_info}";
            __image_id=$(echo ${__image_info} | awk '{print $3}');
            echo 
            echo "Running command:";
            echo "   - docker run -ti --rm -e PGID=$(id -u) -e PUID=$(id -u) ${docker_run_sh_working_dir} ${docker_run_sh_extra_params} ${__image_id} ${docker_run_sh_command}";
            echo 
            echo "Execute...";
            echo
            docker run -ti --rm -e PGID=$(id -u) -e PUID=$(id -u) ${docker_run_sh_working_dir} ${docker_run_sh_extra_params} ${__image_id} ${docker_run_sh_command}
        else
            echo "No image found with ${docker_run_sh_image}";
        fi
    else
        echo "Need to specify an image to run...";
        __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} --help && return 1;
    fi
}

docker_in_docker_env () {
    #Help:
    #>>
    # usage:                docker_in_docker_env [OPTIONS]
    # options:              --name=<name>                                                       -       Specify a name for the docker container (default: dind_env_<suffix>)
    # options:              --image=<docker image>                                              -       Specify the docker image to run for dind (default: docker:20.10.11-dind)
    # options:              --network-name=<docker network>                                     -       Specify the docker network to connect to
    # options:              --volume-name=<docker volume>                                       -       Specify the docker volume to mount for docker images to
    # options:              --runner=<docker>                                                   -       Specify the runner to use. Either 'docker' or 'podman'.
    # examples:             "docker_in_docker_env --name=node_env_dind --image=docker:dind"     -       Run shell in the 'pkg' subdirectory using the node docker image
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;

    # Defaults
    local dind_env_image="docker:dind"
    local dind_env_container_name="dind_env_$(echo ${RANDOM} | md5sum | head -c 4)"
    local dind_env_network_name=""
    local dind_env_volume_name=""
    local dind_env_force_pull=""
    local dind_env_extra_params=""
    local dind_env_action=""
    local dind_env_runner_cmd="docker"

    # Parse args
    for arg in ${@}; do
        if [[ "${arg}" =~ "--name" ]]; then
            dind_env_container_name=$(echo ${arg} | awk -F'=' '{print $2}');
            continue;
        elif [[ "${arg}" =~ "--image" ]]; then
            dind_env_image=$(echo ${arg} | awk -F'=' '{print $2}');
            continue;
        elif [[ "${arg}" =~ "--network" ]]; then
            dind_env_network_name=$(echo ${arg} | awk -F'=' '{print $2}');
            continue;
        elif [[ "${arg}" =~ "--volume" ]]; then
            dind_env_volume_name=$(echo ${arg} | awk -F'=' '{print $2}');
            continue;
        elif [[ "${arg}" =~ "--runner" ]]; then
            dind_env_runner_cmd=$(echo ${arg} | awk -F'=' '{print $2}');
            continue;
        elif [[ "${arg}" == "--pull" ]]; then
            dind_env_force_pull='true';
            continue;
        elif [[ "${arg}" == "--stop-all" ]]; then
            dind_env_action="stop-all"
            continue;
        fi
        dind_env_extra_params="${dind_env_extra_params} ${arg}";
    done
    # Apply defaults based on provided args
    if [[ -z "${dind_env_network_name}" ]]; then
        dind_env_network_name="${dind_env_container_name}"
    fi
    if [[ -z "${dind_env_volume_name}" ]]; then
        dind_env_volume_name="${dind_env_container_name}"
    fi

    # Check that args are compatible with the selected runner
    if [[ "${dind_env_runner_cmd:?}" = "docker" ]]; then
        # Check if sudo is required to run docker commands
        if ! docker ps &>/dev/null; then
            dind_env_runner_cmd="sudo ${dind_env_runner_cmd:?}"
        fi
    fi

    if [[ "${dind_env_action:-}" == "stop-all" ]]; then
        # Remove all containers (stoped containers will remove themselves)
        ${dind_env_runner_cmd:?} stop $(${dind_env_runner_cmd:?} ps | grep 'dind_env' | awk '{print $1}') 2> /dev/null;
        # Remove the docker network (this can be re-created next time)
        ${dind_env_runner_cmd:?} network rm $(${dind_env_runner_cmd:?} network ls | grep 'dind_env' | awk '{print $1}') 2> /dev/null;
        # Remove the docker network (this can be re-created next time)
        ${dind_env_runner_cmd:?} volume rm $(${dind_env_runner_cmd:?} volume ls | grep 'dind_env' | awk '{print $2}') 2> /dev/null;
        return;
    fi

    # Ensure image exists
    if [[ "${dind_env_force_pull}" == 'true' || -z $(${dind_env_runner_cmd:?} images -a | grep "${dind_env_image}" | grep -m1 "${tag}") ]]; then
        ${dind_env_runner_cmd:?} pull "${dind_env_image}"
    fi

    # Create node_env network so we can easily join other containers
    if [[ -z $(${dind_env_runner_cmd:?} network ls 2> /dev/null | grep ${dind_env_network_name}) ]]; then
        ${dind_env_runner_cmd:?} network create -d bridge "${dind_env_network_name}"
    fi

    # Create node_env network so we can easily join other containers
    if [[ -z $(${dind_env_runner_cmd:?} volume ls 2> /dev/null | grep ${dind_env_volume_name}) ]]; then
        ${dind_env_runner_cmd:?} volume create --driver local "${dind_env_volume_name}"
    fi

    # Ensure user namespace is mapped
    if [[ "${dind_env_runner_cmd:?}" = "podman" ]]; then
        dind_env_extra_params="${dind_env_extra_params:-} --userns keep-id --security-opt label:disable";
    fi

    # Run the image
    echo 
    echo "Running dind container...";
    echo "   - " ${dind_env_runner_cmd:?} run --rm -d \
        --privileged \
        --name="${dind_env_container_name}" \
        --network="${dind_env_network_name}" \
        --network-alias "docker" \
        --env DOCKER_HOST=tcp://docker:2375 \
        --env DOCKER_DRIVER=overlay2 \
        --env DOCKER_TLS_CERTDIR="" \
        --publish 2375:2375 \
        --publish 2376:2376 \
        --volume "${dind_env_volume_name}":/var/lib/docker/ \
        --volume="${PWD}:${PWD}:rw" \
        --volume=$(readlink -e ${HOME}/.docker):/root/.docker:ro \
        ${dind_env_extra_params} \
        "${dind_env_image}"
    echo 
    ${dind_env_runner_cmd:?} run --rm -d \
        --privileged \
        --name="${dind_env_container_name}" \
        --network="${dind_env_network_name}" \
        --network-alias "docker" \
        --env DOCKER_HOST=tcp://docker:2375 \
        --env DOCKER_DRIVER=overlay2 \
        --env DOCKER_TLS_CERTDIR="" \
        --publish 2375:2375 \
        --publish 2376:2376 \
        --volume "${dind_env_volume_name}":/var/lib/docker/ \
        --volume="${PWD}:${PWD}:rw" \
        --volume=$(readlink -e ${HOME}/.docker):/root/.docker:ro \
        ${dind_env_extra_params} \
        "${dind_env_image}"
}

# Hacky ailas for running a command as similar to sudo using a minimal busybox docker image
# This is mostly just a proof of concept as to why sudoless docker is bad
docker_sudo () {
    #Help:
    #>>
    # usage:                docker_sudo <COMMAND>
    # examples:             "docker_sudo cat /etc/shadow"   -   Cat a file that would otherwise be RO by root
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;

    docker run --rm -ti \
        --privileged \
        --volume=/:/tmp/docker_sudo:rw \
        busybox:latest \
        busybox chroot /tmp/docker_sudo "${@}"
}

# docker_build_env() {
#     #Help:
#     #>>
#     # usage:                docker_build_env [OPTIONS]
#     # options:              --workdir=<dir>                                               -       Location to execute command in
#     # examples:             "docker_build_env --workdir=pkg ./make.sh 1.1.1 prod"         -       Execute command in the 'pkg' subdirectory
#     #<<
#     __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;
#     # working_dir="--workdir=/app";
#     # ARGS="";
#     # # Get Args
#     # for arg in ${@}; do
#     #     if [[ "${arg}" =~ "--workdir" || "${arg}" =~ "-w" ]]; then
#     #         path=$(echo ${arg} | awk -F'=' '{print $2}');
#     #         working_dir="--workdir=/app/${path}";
#     #         continue;
#     #     fi
#     #     ARGS="${ARGS} ${arg}";
#     # done
#     echo "Running: ${@}";
#     docker run --rm -ti \
#         --workdir=`pwd` \
#         --volume=`pwd`:`pwd`:rw \
#         --user=$(id -u):$(id -g) \
#         collector.nsquared.nz/build-agent:latest \
#         bash -c "${@}";
# }

# WSL Specific commands
if [[ "${BASHRCD_CONFIG}" == "WSL" ]]; then

    dockerd-wsl () {
        #Help:
        #>>
        # usage:                dockerd-wsl [OPTIONS]
        # options:              --start                                 -       Start the dockerd daemon process and log to /var/log/docker.log
        # options:              --stop                                  -       Stop the dockerd daemon process
        # options:              --silent                                -       Do not print to stdout when starting the dockerd
        # examples:             "dockerd-wsl --start"                   -       Start dockerd quietly
        # examples:             "dockerd-wsl --stop"                    -       Stop dockerd if it is running
        #<<
        __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;

        # Install it if it is not already installed
        if [[ ! $(command -v docker) ]]; then
            # Add docker apt repo
            source /etc/os-release
            curl -fsSL https://download.docker.com/linux/${ID}/gpg | sudo apt-key add -
            echo "deb [arch=amd64] https://download.docker.com/linux/${ID} ${VERSION_CODENAME} stable" | sudo tee /etc/apt/sources.list.d/docker.list
            sudo apt-get update
            # Install docker
            sudo apt-get install -y docker-ce docker-ce-cli containerd.io
            sudo usermod -aG docker ${USER}
        fi

        # Run docker
        start_command="sudo dockerd"
        pid=$(ps awx | grep "${start_command}" | grep -v grep | awk '{print $1}')
        if [[ ${1} == '--start' ]]; then
            if [[ -z ${pid} ]]; then
                echo "Starting Docker"
                sudo echo
                ${start_command} |& sudo tee /var/log/docker.log &> /dev/null &
            elif [[ ${2} != '--silent' ]]; then
                echo "Docker is already running"
            fi
        elif [[ ${1} == '--stop' ]]; then
            if [[ ! -z ${pid} ]]; then
                echo "Stopping Docker"
                sudo echo
                while true; do
                    pid=$(ps awx | grep "${start_command}" | grep -v grep | awk '{print $1}')
                    [[ -z ${pid} ]] && break;
                    sudo kill ${pid} &> /dev/null
                    sleep 1
                done
                echo "Docker stopped"
            elif [[ ${2} != '--silent' ]]; then
                echo "Docker is not running"
            fi
        fi
    }

    if [[ "${WSL_DOCKER_AUTO_START}" == "true" ]]; then
        dockerd-wsl --start --silent
    fi

fi

# Login to current AWS env
#   NOTE: Requires aws CLI v2+
docker_login_aws() {
    #Help:
    #>>
    # usage:                docker_login_aws [OPTIONS]
    # options:              --region                                -       Set the region (default: 'eu-west-1')
    # examples:             "docker_login_aws"                      -       Login to the docker registry with the current assumed role and the default region
    # examples:             "docker_login_aws --region=us-east-1"   -       Login to the docker registry with the current assumed role in the 'us-east-1' region
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;

    docker_login_aws_account_id=$(aws sts get-caller-identity --query "Account" --output text)
    docker_login_aws_region="${AWS_REGION:-eu-west-1}"
    # Parse args
    for arg in ${@}; do
        if [[ "${arg}" =~ "--region" ]]; then
            docker_login_aws_region=$(echo ${arg} | awk -F'=' '{print $2}');
            continue;
        fi
    done

    # Check if sudo is required to run docker commands
    local runner_cmd="docker"
    if ! docker ps &>/dev/null; then
        runner_cmd="sudo ${runner_cmd:?}"
    fi

    aws ecr get-login-password --region ${docker_login_aws_region} | ${runner_cmd:?} login --username AWS --password-stdin "${docker_login_aws_account_id}.dkr.ecr.${docker_login_aws_region}.amazonaws.com"
}

# Login to current GitLab env
docker_login_gitlab() {
    #Help:
    #>>
    # usage:                docker_login_gitlab [OPTIONS]
    # options:              --registry=<string>                         The registry to log into
    # options:              --user=<string>                             The username to use for the login
    # options:              --token=<string>                            The token to use for the login
    # examples:             "docker_login_gitlab"                       Login with the currently exported GitLab configuration
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;

    local __dlg_registry="${CI_REGISTRY:-}"
    local __dlg_user="${CI_REGISTRY_USER:-}"
    local __dlg_token="${CI_JOB_TOKEN:-}"
    
    while [[ $# -gt 0 ]]; do
        case "$1" in
            --registry=*)
                registry="${1#*=}"
                shift
                ;;
            --user=*)
                registry="${1#*=}"
                shift
                ;;
            --token=*)
                registry="${1#*=}"
                shift
                ;;
            *)
            echo "Unknown option: $1"
            return 1
            ;;
        esac
    done

    # Check if sudo is required to run docker commands
    local runner_cmd="docker"
    if ! docker ps &>/dev/null; then
        runner_cmd="sudo ${runner_cmd:?}"
    fi

    ${runner_cmd:?} login -u "${__dlg_user:?}" -p "${__dlg_token:?}" "${__dlg_registry:?}"
}

docker_remove_stopped_containers () {
    #Help:
    #>>
    # usage:                docker_remove_stopped_containers
    # description:          Removes all stopped docker containers.
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;

    __docker_remove_stopped_containers=$(docker ps -a --format '{{.Names}}' --filter "status=exited")
    if [[ ! -z "${__docker_remove_stopped_containers}" ]]; then
        docker rm ${__docker_remove_stopped_containers}
    else
        echo "No stopped containers exist to remove."
        echo
        docker ps -a
        return 1
    fi
}

# Use Nginx to server a directory of static files
function docker_serve_static {
    #Help:
    #>>
    # usage:                docker_serve_static
    # description:          Serves the current directory with Nginx
    # options:              --port=<number>                             The port to serve it on. (Default: '8080')
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;

    # Defaults
    local docker_serve_static_port="8080"
    local docker_serve_static_container_name="docker_serve_static_$(echo ${RANDOM} | md5sum | head -c 4)"

    # Parse args
    for arg in ${@}; do
        if [[ "${arg}" =~ "--port" ]]; then
            docker_serve_static_port=$(echo ${arg} | awk -F'=' '{print $2}');
            continue;
        fi
        docker_serve_static_extra_params="${docker_serve_static_extra_params} ${arg}";
    done

    docker run --rm \
        --name "${docker_serve_static_container_name}" \
        -v "$(pwd)/:/usr/share/nginx/html/:ro" \
        -p ${docker_serve_static_port}:80 \
        ${docker_serve_static_extra_params} nginx:latest 
}

# WIP: Create a Docker container sandbox environment
function sandbox_env {
    #Help:
    #>>v2
    # Usage:      sandbox_env [OPTIONS] COMMAND
    # 
    # Description:
    #   Start a container sandbox environment and attach to a shell within the started container for the specified Docker/Podman image.
    #   Will mount the current working directory (CWD) as the working directory inside the container.
    #
    #   By default the sandbox will be created with a tmpfs home directory that includes your bashrc files mounted RO. See options
    #   below on how to modify this behaviour.
    # 
    # Options:
    #   --image=<string>        The docker image to use (default: buildpack-deps:bookworm)
    #   --name=<string>         What to name the container (default: sandbox_env_<RANDOM_STRING>)
    #   --path=<string>         The path to mount as the sandbox container working directory (default: CWD)
    #   --id=<string>           Specify the container ID to execute a command against. Not used with the 'start' command (default: The container matching CWD)
    #   --network               Specify the name of the docker network to use for this sandbox (default: sandbox_env_<RANDOM_STRING>)
    #   --pull                  Force an update of the docker image before running the container (default False)
    #   --cached-home-dir       Mount a common cached version of the home directory for the user inside the CI env container (default False)
    #   --no-cwd-mount          Do not mount the CWD. Instead mount a temp path as the container's CWD (default False)
    #   --no-home-config        Do not mount any configuration from the home directory (default False)
    #   --no-passwd-mount       Do not mount the /etc/passwd path (default False)
    #   --x11                   Pass the host X11 socket to the container
    #   --runner                Force the runner to use - either 'docker' or 'podman' (default 'podman' if available, else 'docker')
    #   --dind                  Configure image with docker in docker (not compatible with podman runner)
    #   -d, --detach            Detach from the container after starting
    # 
    # Commands:
    #   attach                  Open a console inside the sandbox container for the current directory
    #   attach-root             Open a root console inside the sandbox container for the current directory
    #   configure               Configure the sandbox container for the current directory according to the ~/.config/docker-sandbox-env/env-setup.sh file
    #   show                    Display details of all 'sandbox_env_*' docker containers
    #   start                   Start a sandbox terminal inside the current directory
    #   stop                    Stop the CWD sandbox docker containers
    #   stop-all                Stop all 'sandbox_env_*' docker containers
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;

    # Defaults
    local sandbox_env_all_running_containers=""
    local sandbox_env_rand_string="$(echo ${RANDOM} | md5sum | head -c 4)"
    local sandbox_env_image="buildpack-deps:bookworm"
    local sandbox_env_path="$PWD"
    local sandbox_env_container_id=""
    local sandbox_env_force_pull="false"
    local sandbox_env_cache_home_dir="false"
    local sandbox_env_container_name="sandbox_env_${sandbox_env_rand_string}"
    local sandbox_env_network_name="sandbox_env_${sandbox_env_rand_string}"
    local sandbox_env_no_cwd_mount="false"
    local sandbox_env_no_home_confg_mount="false"
    local sandbox_env_no_passwd_mount="false"
    local sandbox_env_x11="false"
    local sandbox_env_extra_params=""
    local sandbox_env_action=""
    local sandbox_env_dind_container_name=""
    local sandbox_env_runner_cmd="docker"
    local sandbox_env_runner_cmd_prefix=""

    # Functions
    function __get_running_sandbox_container_id_for_current_working_directory {
        if [[ -z "${sandbox_env_container_id:-}" ]]; then
            sandbox_env_container_id=$(${sandbox_env_runner_cmd_prefix:-} ${sandbox_env_runner_cmd:?} ps \
                --filter "volume=${sandbox_env_path}" \
                --filter "name=sandbox_env_" \
                --format "{{.ID}} {{.Names}}" \
                | grep -v 'dind' \
                | head -n1 \
                | awk '{print $1}')
        fi
    }
    function __get_all_running_sandbox {
        if [[ -z "${sandbox_env_all_running_containers:-}" ]]; then
            sandbox_env_all_running_containers=$(${sandbox_env_runner_cmd_prefix:-} ${sandbox_env_runner_cmd:?} ps \
                --filter "name=sandbox_env_" \
                --format "{{.ID}} {{.Names}}" \
                | grep -v 'dind' \
                | awk '{print $1}')
        fi
    }

    # Override runner command
    if command -v podman &>/dev/null; then
        # Prefer podman over docker
        sandbox_env_runner_cmd="podman"
    fi

    # Parse args
    for arg in ${@}; do
        # OPTIONS:
        if [[ "${arg}" =~ "--image" ]]; then
            sandbox_env_image=$(echo ${arg} | awk -F'=' '{print $2}');
            continue;
        elif [[ "${arg}" =~ "--name" ]]; then
            sandbox_env_container_name=$(echo ${arg} | awk -F'=' '{print $2}');
            continue;
        elif [[ "${arg}" =~ "--network" ]]; then
            sandbox_env_network_name=$(echo ${arg} | awk -F'=' '{print $2}');
            continue;
        elif [[ "${arg}" == "--pull" ]]; then
            sandbox_env_force_pull='true';
            continue;
        elif [[ "${arg}" == "--cached-home-dir" ]]; then
            sandbox_env_cache_home_dir='true';
            continue;
        elif [[ "${arg}" =~ "--no-cwd-mount" ]]; then
            sandbox_env_no_cwd_mount='true';
            continue;
        elif [[ "${arg}" =~ "--no-home-config" ]]; then
            sandbox_env_no_home_confg_mount='true';
            continue;
        elif [[ "${arg}" =~ "--no-passwd-mount" ]]; then
            sandbox_env_no_passwd_mount='true';
            continue;
        elif [[ "${arg}" =~ "--x11" ]]; then
            sandbox_env_x11='true';
            continue;
        elif [[ "${arg}" =~ "--runner" ]]; then
            sandbox_env_runner_cmd=$(echo ${arg} | awk -F'=' '{print $2}');
            continue;
        elif [[ "${arg}" == "--dind" ]]; then
            # Start a sandbox_env_dind container for this env
            sandbox_env_dind_container_name="${sandbox_env_container_name}_dind"
            continue;
        elif [[ "${arg}" =~ "--path" ]]; then
            sandbox_env_path=$(echo ${arg} | awk -F'=' '{print $2}');
            continue;
        elif [[ "${arg}" =~ "--id" ]]; then
            sandbox_env_container_id=$(echo ${arg} | awk -F'=' '{print $2}');
            continue;
        # COMMANDS:
        elif [[ "${arg}" == "start" ]]; then
            sandbox_env_action="start"
            continue;
        elif [[ "${arg}" == "stop" ]]; then
            sandbox_env_action="stop"
            continue;
        elif [[ "${arg}" == "stop-all" ]]; then
            sandbox_env_action="stop-all"
            continue;
        elif [[ "${arg}" == "show" ]]; then
            sandbox_env_action="show"
            continue;
        elif [[ "${arg}" == "configure" ]]; then
            sandbox_env_action="configure"
            continue;
        elif [[ "${arg}" == "attach" ]]; then
            sandbox_env_action="attach"
            continue;
        elif [[ "${arg}" == "attach-root" ]]; then
            sandbox_env_action="attach-root"
            continue;
        fi
        # ADDITIONAL PARAMS TO PASS TO DOCKER RUN COMMAND
        sandbox_env_extra_params="${sandbox_env_extra_params:-} ${arg}";
    done

    # Check that a command was provided
    if [[ -z "${sandbox_env_action:-}" ]]; then
        echo "ERROR! No command was provided."
        echo
        __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} --help && return 1;
    fi

    # Check if sudo is required to run docker commands
    if ! docker ps &>/dev/null; then
        sandbox_env_runner_cmd_prefix="sudo"
    fi
    # Remove sudo prefix for podman containers
    if [[ "${sandbox_env_runner_cmd:?}" = "podman" ]]; then
        sandbox_env_runner_cmd_prefix=""
    fi

    # Check that args are compatible with the selected runner
    if [[ "${sandbox_env_runner_cmd:?}" = "podman" ]]; then
        # Dind is not supported with podman
        if [[ -n "${sandbox_env_dind_container_name}" ]]; then
            echo "ERROR! Docker in Docker is not compaitble with the podman runner. Try running with --runner=docker."
            echo
            __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} --help && return 1;
        fi
    fi

    if [[ "${sandbox_env_action:-}" == "attach" ]]; then
        if [[ -z "${sandbox_env_container_id}" ]]; then
            __get_running_sandbox_container_id_for_current_working_directory
        fi
        if [[ -z "${sandbox_env_container_id}" ]]; then
            echo "ERROR! No running sandbox in this directory."
            return 1;
        fi
        cmd=sh
        if ${sandbox_env_runner_cmd_prefix:-} ${sandbox_env_runner_cmd:?} exec -it ${sandbox_env_container_id:?} which bash &> /dev/null; then
            cmd=bash
        elif ${sandbox_env_runner_cmd_prefix:-} ${sandbox_env_runner_cmd:?} exec -it ${sandbox_env_container_id:?} command -v bash &> /dev/null; then
            cmd=bash
        fi
        ${sandbox_env_runner_cmd_prefix:-} ${sandbox_env_runner_cmd:?} exec -ti ${sandbox_env_container_id:?} ${cmd};
        return;
    elif [[ "${sandbox_env_action:-}" == "attach-root" ]]; then
        if [[ -z "${sandbox_env_container_id}" ]]; then
            __get_running_sandbox_container_id_for_current_working_directory
        fi
        if [[ -z "${sandbox_env_container_id}" ]]; then
            echo "ERROR! No running sandbox in this directory."
            return 1;
        fi
        cmd=sh
        if ${sandbox_env_runner_cmd_prefix:-} ${sandbox_env_runner_cmd:?} exec -it ${sandbox_env_container_id:?} which bash &> /dev/null; then
            cmd=bash
        elif ${sandbox_env_runner_cmd_prefix:-} ${sandbox_env_runner_cmd:?} exec -it ${sandbox_env_container_id:?} command -v bash &> /dev/null; then
            cmd=bash
        fi
        ${sandbox_env_runner_cmd_prefix:-} ${sandbox_env_runner_cmd:?} exec -ti -u root ${sandbox_env_container_id:?} ${cmd};
        return;
    elif [[ "${sandbox_env_action:-}" == "configure" ]]; then
        if [[ -z "${sandbox_env_container_id}" ]]; then
            __get_running_sandbox_container_id_for_current_working_directory
        fi
        ${sandbox_env_runner_cmd_prefix:-} ${sandbox_env_runner_cmd:?} exec -ti -u root ${sandbox_env_container_id} sh -c ". /tmp/.docker-sandbox-env-setup.sh";
        return;
    elif [[ "${sandbox_env_action:-}" == "stop" ]]; then
        # Remove all containers for this working directory
        if [[ -z "${sandbox_env_container_id}" ]]; then
            __get_running_sandbox_container_id_for_current_working_directory
        fi
        if [[ -n "${sandbox_env_container_id:-}" ]]; then
            local sandbox_id=$(${sandbox_env_runner_cmd_prefix:-} ${sandbox_env_runner_cmd:?} ps --format "{{.ID}} {{.Names}}" | grep ${sandbox_env_container_id} | awk '{print $2}')
            # Stop all containers with this ID. This will include the DIND container
            ${sandbox_env_runner_cmd_prefix:-} ${sandbox_env_runner_cmd:?} stop $(${sandbox_env_runner_cmd_prefix:-} ${sandbox_env_runner_cmd:?} ps | grep "${sandbox_id:?}" | awk '{print $1}') 2> /dev/null;
        fi
        return;
    elif [[ "${sandbox_env_action:-}" == "stop-all" ]]; then
        # Remove all containers (stoped containers will remove themselves)
        ${sandbox_env_runner_cmd_prefix:-} ${sandbox_env_runner_cmd:?} stop $(${sandbox_env_runner_cmd_prefix:-} ${sandbox_env_runner_cmd:?} ps | grep 'sandbox_env_' | awk '{print $1}') 2> /dev/null;
        # Remove any docker networks
        ${sandbox_env_runner_cmd_prefix:-} ${sandbox_env_runner_cmd:?} network rm $(${sandbox_env_runner_cmd_prefix:-} ${sandbox_env_runner_cmd:?} network ls | grep 'sandbox_env_' | awk '{print $1}') 2> /dev/null;
        # Remove any docker volumes
        ${sandbox_env_runner_cmd_prefix:-} ${sandbox_env_runner_cmd:?} volume rm $(${sandbox_env_runner_cmd_prefix:-} ${sandbox_env_runner_cmd:?} volume ls | grep 'sandbox_env_' | awk '{print $2}') 2> /dev/null;
        return;
    elif [[ "${sandbox_env_action:-}" == "show" ]]; then
        __get_all_running_sandbox
        if [[ -n "${sandbox_env_all_running_containers:-}" ]]; then
            echo "Ruinning container sandboxes:"
            while IFS= read -r container_id; do
                if ${sandbox_env_runner_cmd_prefix:-} ${sandbox_env_runner_cmd:?} ps --filter "volume=${sandbox_env_path}" --filter "id=${container_id}" --format "{{.ID}} {{.Names}}" | grep -q "${container_id}"; then
                    echo " *> $(${sandbox_env_runner_cmd_prefix:-} ${sandbox_env_runner_cmd:?} ps --format "ID: {{.ID}}   Name: {{.Names}}   Created: {{.CreatedAt}}   Status: {{.Status}}" | grep ${container_id})"
                else
                    echo "  > $(${sandbox_env_runner_cmd_prefix:-} ${sandbox_env_runner_cmd:?} ps --format "ID: {{.ID}}   Name: {{.Names}}   Created: {{.CreatedAt}}   Status: {{.Status}}" | grep ${container_id})"
                fi
            done <<< "${sandbox_env_all_running_containers}"
            echo
            echo "Note that the sandbox that is running for this current directory is marked with a *."
            echo
            echo "To attach to a sandbox container for this directory run one of these commands:"
            echo '  `sandbox_env attach`                     Attach to the sandbox container for this directory as your UID.'
            echo '  `sandbox_env attach --id=<str>`          Attach to a specific sandbox container as your UID using the container ID.'
            echo '  `sandbox_env attach-root`                Attach to the sandbox container for this directory as root.'
            echo '  `sandbox_env attach-root --id=<str>`     Attach to a specific sandbox container as root using the container ID.'
            echo
            echo "To configure running container with a pre-configured setup script run:"
            echo '  `sandbox_env configure`                  Configure the sandbox container for this directory.'
            echo '  `sandbox_env configure --id=<str>`       Configure a specific sandbox container using the container ID.'
            echo
            echo "To stop this running container run:"
            echo '  `sandbox_env stop`                       Stop the sandbox container for this directory.'
            echo '  `sandbox_env stop --id=<str>`            Stop a specific sandbox container using the container ID.'
            echo
        fi
        return;
    fi

    # Create docker sandbox env config directory
    local sandbox_env_config_dir="${HOME}/.config/docker-sandbox-env"
    mkdir -p "${sandbox_env_config_dir}"
    touch "${sandbox_env_config_dir}/env-setup.sh"

    # Configure CI env home directory
    local sandbox_env_home_dir
    local sandbox_env_temp_dir="$(mktemp --directory --suffix=sandbox_env)"
    if [[ "${sandbox_env_cache_home_dir}" == 'true' ]]; then
        sandbox_env_home_dir="${sandbox_env_config_dir}/home"
    else
        sandbox_env_home_dir="${sandbox_env_temp_dir:?}/home"
    fi
    mkdir -p "${sandbox_env_home_dir}"

    # Configure CWD mount
    local sandbox_env_passthrough_path
    if [[ "${sandbox_env_no_cwd_mount}" != 'true' ]]; then
        if [[ "${sandbox_env_path}" != "${HOME}" ]]; then
            sandbox_env_extra_params="${sandbox_env_extra_params:-} --volume=${sandbox_env_path}:${sandbox_env_path}:rw --workdir=${sandbox_env_path}";
        fi
        # Check if this working directory is already running in a sandbox
        __get_running_sandbox_container_id_for_current_working_directory
        if [[ -n "${sandbox_env_container_id:-}" ]]; then
            echo "ERROR! This working directory is already running in a ${sandbox_env_runner_cmd:?} container sandbox."
            echo
            echo " *> $(${sandbox_env_runner_cmd_prefix:-} ${sandbox_env_runner_cmd:?} ps --format "ID: {{.ID}}   Name: {{.Names}}   Created: {{.CreatedAt}}   Status: {{.Status}}" | grep ${sandbox_env_container_id})"
            echo
            echo "To attach to this sandbox container run one of these commands:"
            echo '  `sandbox_env attach`             Attach to this running container as your user.'
            echo '  `sandbox_env attach-root`        Attach to this running container as root.'
            echo
            echo "To stop this sandbox container run:"
            echo '  `sandbox_env stop`               Attach to this running container as your user.'
            echo
            return 1;
        fi
    else
        mkdir -p "${sandbox_env_temp_dir:?}/temp"
        sandbox_env_extra_params="${sandbox_env_extra_params:-} --volume=${sandbox_env_temp_dir:?}/temp:${sandbox_env_temp_dir:?}/temp:rw --workdir=${sandbox_env_temp_dir:?}/temp";
    fi

    # Configure home directory config file mounts
    if [[ "${sandbox_env_no_home_confg_mount}" != 'true' ]]; then
        if [[ -f "${HOME}/.bashrc" ]]; then
            sandbox_env_extra_params="${sandbox_env_extra_params:-} --volume=${HOME}/.bashrc:${HOME}/.bashrc:ro";
        fi
        if [[ -f "${HOME}/.bash_aliases" ]]; then
            sandbox_env_extra_params="${sandbox_env_extra_params:-} --volume=${HOME}/.bash_aliases:${HOME}/.bash_aliases:ro";
        fi
        if [[ -d "${HOME}/bashrc.d" ]]; then
            if [[ "${sandbox_env_path}" != "${HOME}/bashrc.d" ]]; then
                sandbox_env_extra_params="${sandbox_env_extra_params:-} --volume=${HOME}/bashrc.d:${HOME}/bashrc.d:ro";
            fi
        fi
        if [[ -f "${HOME}/.npmrc" ]]; then
            sandbox_env_extra_params="${sandbox_env_extra_params:-} --volume=${HOME}/.npmrc:${HOME}/.npmrc:ro --env NPM_CONFIG_USERCONFIG=${HOME}/.npmrc";
        fi
        if [[ -d "${HOME}/.docker" ]]; then
            if [[ "${sandbox_env_path}" != "${HOME}/.docker" ]]; then
                sandbox_env_extra_params="${sandbox_env_extra_params:-} --volume=${HOME}/.docker:${HOME}/.docker:ro";
            fi
        fi
    fi

    # Configure home directory config file mounts
    if [[ "${sandbox_env_no_passwd_mount}" != 'true' ]]; then
        sandbox_env_extra_params="${sandbox_env_extra_params:-} --volume=/etc/passwd:/etc/passwd:ro";
    fi

    # Configure X11 passthrough
    if [[ "${sandbox_env_x11}" = 'true' ]]; then
        if [[ -d /tmp/.X11-unix ]]; then
            xhost +SI:localuser:$(whoami)
            sandbox_env_extra_params="${sandbox_env_extra_params:-} --env DISPLAY=${DISPLAY:-0} --volume=/tmp/.X11-unix:/tmp/.X11-unix";
        else
            echo "WARNING! The /tmp/.X11-unix path was not found. Container will not run with X11 support."
        fi
    fi

    # Start entrypoint
    echo '#!/usr/bin/env sh' > "${sandbox_env_home_dir}/entrypoint.sh"
    echo 'export PATH=${PATH}:${HOME}/.local/bin' >> "${sandbox_env_home_dir}/entrypoint.sh"
    chmod a+rwx "${sandbox_env_home_dir}/entrypoint.sh"
    sandbox_env_extra_params="${sandbox_env_extra_params:-} --entrypoint=${HOME}/entrypoint.sh";

    # Check if we should enable AWS credentials
    # TODO: Make this a config option and pull this from the host aws cli config
    sandbox_env_include_aws_creds=""
    if [[ -n "${AWS_ACCESS_KEY_ID:-}" && -n "${AWS_SECRET_ACCESS_KEY:-}" ]]; then
        sandbox_env_include_aws_creds="--env AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID} --env AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}";
        sandbox_env_extra_params="${sandbox_env_extra_params:-} ${sandbox_env_include_aws_creds}";
    fi

    # Create 'sandbox_env' network so we can easily join other containers
    if [[ -z $(${sandbox_env_runner_cmd_prefix:-} ${sandbox_env_runner_cmd:?} network ls 2> /dev/null | grep ${sandbox_env_network_name}) ]]; then
        ${sandbox_env_runner_cmd_prefix:-} ${sandbox_env_runner_cmd:?} network create -d bridge ${sandbox_env_network_name}
    fi

    # Start Dind container if required
    if [[ -n "${sandbox_env_dind_container_name}" ]]; then
        # Start a dind container and share volumes with below CI env docker container
        if [[ -z "$(${sandbox_env_runner_cmd_prefix:-} ${sandbox_env_runner_cmd:?} ps --filter "Name=sandbox_env_" | grep dind)" ]]; then
            local dind_additinal_params=""
            if [[ "${sandbox_env_runner_cmd:?}" = "podman" ]]; then
                dind_additinal_params="${dind_additinal_params:-} --runner=podman --image=docker:dind-rootless";
            fi
            docker_in_docker_env ${sandbox_env_include_aws_creds} \
                --name="${sandbox_env_dind_container_name}" \
                --network="${sandbox_env_network_name}" \
                --volume="${sandbox_env_network_name}_dind" \
                ${dind_additinal_params:-}
        fi
        sandbox_env_extra_params="${sandbox_env_extra_params:-} --env DOCKER_HOST=tcp://docker:2375";
    fi

    # Ensure bash-it is disabled for sandbox environments
    sandbox_env_extra_params="${sandbox_env_extra_params:-} --env BASH_IT=false";

    # Ensure image exists
    local sandbox_env_image_repo=$(echo ${sandbox_env_image:?} | awk -F: '{print $1}');
    local sandbox_env_image_tag=$(echo ${sandbox_env_image:?} | awk -F: '{print $2}');
    sandbox_env_image_tag=${sandbox_env_image_tag:-latest}
    local sandbox_env_local_image_info=$(${sandbox_env_runner_cmd_prefix:-} ${sandbox_env_runner_cmd:?} images -a --filter "reference=${sandbox_env_image_repo:?}:${sandbox_env_image_tag:?}" | grep -m1 "${sandbox_env_image_tag}");
    if [[ "${sandbox_env_force_pull}" == 'true' || -z ${sandbox_env_local_image_info} ]]; then
        ${sandbox_env_runner_cmd_prefix:-} ${sandbox_env_runner_cmd:?} pull "${sandbox_env_image}"
        sandbox_env_local_image_info=$(${sandbox_env_runner_cmd_prefix:-} ${sandbox_env_runner_cmd:?} images -a --filter "reference=${sandbox_env_image_repo:?}:${sandbox_env_image_tag:?}" | grep -m1 "${sandbox_env_image_tag}");
    fi

    # Complete entrypont
    echo 'exec "$@"' >> "${sandbox_env_home_dir}/entrypoint.sh"

    # Check for available shells
    local sandbox_env_shell="sh"
    if [[ -n $(${sandbox_env_runner_cmd_prefix:-} ${sandbox_env_runner_cmd:?} run --rm ${sandbox_env_image} sh -c "command -v bash") ]]; then
        sandbox_env_shell="bash"
    fi

    # Ensure user namespace is mapped
    if [[ "${sandbox_env_runner_cmd:?}" = "podman" ]]; then
        sandbox_env_extra_params="${sandbox_env_extra_params:-} --userns keep-id --security-opt label:disable";
    fi

    # Run the image
    if [[ -n "${sandbox_env_local_image_info:-}" ]]; then
        echo "Image information:";
        echo "   - ${sandbox_env_local_image_info:?}";
        local sandbox_env_image_id=$(echo ${sandbox_env_local_image_info:?} | awk '{print $3}');
        local run_cmd="${sandbox_env_runner_cmd_prefix:-} ${sandbox_env_runner_cmd:?} run -ti --rm \
            --name=${sandbox_env_container_name} \
            --network=${sandbox_env_network_name} \
            --user=$(id -u):$(id -g) -e PGID=$(id -u) -e PUID=$(id -u) \
            --env SANDBOX_ENV=${sandbox_env_image:?} \
            --volume=${sandbox_env_home_dir}:${HOME}:rw \
            --volume=${sandbox_env_config_dir}/env-setup.sh:/tmp/.docker-sandbox-env-setup.sh:ro \
            ${sandbox_env_extra_params:-} \
            ${sandbox_env_image_id:?} ${sandbox_env_shell:?}
        "
        echo 
        echo "Running command:";
        echo "   - "${run_cmd:?};
        echo 
        echo "Execute...";
        echo
        ${run_cmd:?}
    else
        echo "No image found with ${sandbox_env_image:?}";
    fi
}
