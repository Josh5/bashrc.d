###
# File: ffmpeg.sh
# Project: bashrc.d
# File Created: Saturday, 27th March 2021 10:47:05 am
# Author: Josh.5 (jsunnex@gmail.com)
# -----
# Last Modified: Friday, 18th November 2022 12:03:54 pm
# Modified By: Josh Sunnex (jsunnex@gmail.com)
###

# Create a GIF from a Video file
function ffmpeg_make_gif {
    #Help:
    #>>
    # usage:                ffmpeg_make_gif [OPTIONS] <videofile>
    # options:              --starttime                -           Start from time stamp (eg. '00:00:00') [DEFAULT: '00:00:00']
    # options:              --stoptime                 -           Finish at time stamp (eg. '00:01:25')
    # options:              --scale                    -           Scale width of GIF (eg. '512')
    # options:              --fps                      -           Set the FPS of the GIF (eg. '10') [DEFAULT: '10']
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;
    if [[ !  -z  ${1} ]]; then
        in_file=""
        ffmpeg_gif_starttime='00:00:00';
        ffmpeg_gif_fps='10'
        for arg in $@; do
            if [[ "${arg}" =~ "--starttime" ]]; then
                ffmpeg_gif_starttime=$(echo ${arg} | awk -F'=' '{print $2}');
                continue;
            elif [[ "${arg}" =~ "--stoptime" ]]; then
                ffmpeg_gif_stoptime=$(echo ${arg} | awk -F'=' '{print $2}');
                continue;
            elif [[ "${arg}" =~ "--scale" ]]; then
                ffmpeg_gif_scale=$(echo ${arg} | awk -F'=' '{print $2}');
                continue;
            elif [[ "${arg}" =~ "--fps" ]]; then
                ffmpeg_gif_fps=$(echo ${arg} | awk -F'=' '{print $2}');
                continue;
            elif [[ -f "${arg}" ]]; then
                # Arg points to a file. Doing it this way allows us to add optins berfore or after dclairing the file
                in_file="${arg}";
                continue;
            fi
        done
    fi

    # Ensure args are valid
    [[ -z ${in_file} ]] && echo "Missing input file." && return 1;
    out_file="${in_file%.*}.gif"

    # Build args
    cmd="ffmpeg -i '${in_file}'"
    cmd="${cmd} -r 15"

    # Build filtergraph
    vf=""
    if [[ ! -z ${ffmpeg_gif_fps} ]]; then
        vf="${vf}fps=${ffmpeg_gif_fps},"
    fi
    if [[ ! -z ${ffmpeg_gif_scale} ]]; then
        vf="${vf}scale=${ffmpeg_gif_scale}:-1,"
    fi
    vf="${vf}split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse"
    cmd="${cmd} -vf '${vf}'"

    # Set start/stop times
    if [[ ! -z ${ffmpeg_gif_starttime} ]]; then
        cmd="${cmd} -ss ${ffmpeg_gif_starttime}"
    fi
    
    if [[ ! -z ${ffmpeg_gif_stoptime} ]]; then
        cmd="${cmd} -to ${ffmpeg_gif_stoptime}"
    else
        ffmpeg_gif_stoptime=$(ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 ${in_file} 2> /dev/null)
        cmd="${cmd} -to ${ffmpeg_gif_stoptime}"
    fi

    # Set the output file
    cmd="${cmd} -y ${out_file}"

    # Exec command
    echo
    echo ${cmd}
    echo
    eval ${cmd}
}


# Compress and trip a Video file
function ffmpeg_compress_clip {
    #Help:
    #>>
    # usage:                ffmpeg_compress_clip [OPTIONS] <videofile>
    # options:              --starttime                -           Start from time stamp (eg. '00:00:00') [DEFAULT: '00:00:00']
    # options:              --stoptime                 -           Finish at time stamp (eg. '00:01:25')
    # options:              --scale                    -           Scale width of video (eg. '512')
    # options:              --fps                      -           Set the FPS of the video (eg. '10') [DEFAULT: '10']
    # options:              --encoder                  -           Set the video encoder (eg. 'libx264,libx265,libvpx-vp9') [DEFAULT: 'libx264']
    # options:              --pix_fmt                  -           Set the video chroma subsampling (eg. 'yuv420p,yuv422p,yuv444p,yuv420p10le,yuv422p10le,yuv444p10le') [DEFAULT: 'yuv420p']
    # options:              --container                -           Set the output container (eg. 'mkv,mp4') [DEFAULT: same as source]
    # options:              --docker_image             -           Specify a docker image to run the command inside (rather than running natively)
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} && return;
    if [[ !  -z  ${1} ]]; then
        local ffmpeg_starttime='00:00:00';
        local ffmpeg_stoptime='';
        local ffmpeg_scale='';
        local ffmpeg_fps='';
        local video_encoder='libx264';
        local pix_fmt='yuv420p';
        local docker_image='';
        ext='';
        in_file='';
        for arg in $@; do
            if [[ "${arg}" =~ "--starttime" ]]; then
                ffmpeg_starttime=$(echo ${arg} | awk -F'=' '{print $2}');
                continue;
            elif [[ "${arg}" =~ "--stoptime" ]]; then
                ffmpeg_stoptime=$(echo ${arg} | awk -F'=' '{print $2}');
                continue;
            elif [[ "${arg}" =~ "--scale" ]]; then
                ffmpeg_scale=$(echo ${arg} | awk -F'=' '{print $2}');
                continue;
            elif [[ "${arg}" =~ "--fps" ]]; then
                ffmpeg_fps=$(echo ${arg} | awk -F'=' '{print $2}');
                continue;
            elif [[ "${arg}" =~ "--encoder" ]]; then
                video_encoder=$(echo ${arg} | awk -F'=' '{print $2}');
                continue;
            elif [[ "${arg}" =~ "--pix_fmt" ]]; then
                pix_fmt=$(echo ${arg} | awk -F'=' '{print $2}');
                continue;
            elif [[ "${arg}" =~ "--container" ]]; then
                ext=$(echo ${arg} | awk -F'=' '{print $2}');
                continue;
            elif [[ "${arg}" =~ "--docker_image" ]]; then
                docker_image=$(echo ${arg} | awk -F'=' '{print $2}');
                [[ -z "${docker_image}" ]] && docker_image="josh5/unmanic:latest"
                continue;
            elif [[ -f "${arg}" ]]; then
                # Arg points to a file. Doing it this way allows us to add optins berfore or after dclairing the file
                in_file="${arg}";
                continue;
            else
                echo "${arg}"
            fi
        done
    fi

    # Ensure args are valid
    [[ -z ${in_file} ]] && echo "Missing input file." && return 1;
    out_file="${in_file%.*}.compressed.${in_file##*.}"
    [[ ! -z ${ext} ]] && out_file="${in_file%.*}.compressed.${ext}"

    # Build args
    cmd="ffmpeg -i '${in_file}'"
    cmd="${cmd} -r 15"

    # Build filtergraph
    vf=""
    if [[ ! -z ${ffmpeg_fps} ]]; then
        new_vf="fps=${ffmpeg_fps}"
        if [[ ! -z ${vf} ]]; then
            new_vf="${vf},${new_vf}"
        fi
        vf="${new_vf}"
    fi
    if [[ ! -z ${ffmpeg_scale} ]]; then
        new_vf="scale=${ffmpeg_scale}:-1"
        if [[ ! -z ${vf} ]]; then
            new_vf="${vf},${new_vf}"
        fi
        vf="${new_vf}"
    fi
    if [[ ! -z ${pix_fmt} ]]; then
        new_vf="format=${pix_fmt}"
        if [[ ! -z ${vf} ]]; then
            new_vf="${vf},${new_vf}"
        fi
        vf="${new_vf}"
    fi
    if [[ ! -z ${vf} ]]; then
        cmd="${cmd} -vf '${vf}'"
    fi
    ## vf="fps=${ffmpeg_fps}"
    ## if [[ ! -z ${ffmpeg_scale} ]]; then
    ##     vf="${vf},scale=${ffmpeg_scale}:-1"
    ## fi

    # Set start/stop times
    if [[ ! -z ${ffmpeg_starttime} ]]; then
        cmd="${cmd} -ss ${ffmpeg_starttime}"
    fi
    
    if [[ ! -z ${ffmpeg_stoptime} ]]; then
        cmd="${cmd} -to ${ffmpeg_stoptime}"
    else
        echo ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 ${in_file}
        ffmpeg_stoptime=$(ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 ${in_file} 2> /dev/null)
        cmd="${cmd} -to ${ffmpeg_stoptime}"
    fi

    # Set video encoding
    cmd="${cmd} -c:v ${video_encoder}"

    # Set the output file
    cmd="${cmd} -y ${out_file}"

    # Run command inside a docker container
    if [[ ! -z ${docker_image} ]]; then
        # Note: Runs privileged to easily pass in required HW
        cmd="docker run --rm \
            --privileged \
            -e PUID=$(id -u) -e PGID=$(id -g) \
            --user=$(id -u):$(id -g) \
            --workdir=$(pwd) \
            --volume=$(pwd):$(pwd):rw \
            --entrypoint='' \
            '${docker_image}' \
            ${cmd}"
        echo $cmd
    fi

    # Exec command
    echo
    echo ${cmd}
    echo
    eval ${cmd}
}

function ffprobe_json {
    echo ffprobe \
        -show_format \
        -show_streams  \
        -print_format json \
        -loglevel quiet \
        "${@}"
    ffprobe \
        -show_format \
        -show_streams  \
        -print_format json \
        -loglevel quiet \
        "${@}"
}
