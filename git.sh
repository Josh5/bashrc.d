# @Author: Josh.5
# @Date:   2019-04-22 11:13:34
# @Last Modified by:   Josh.5
# @Last Modified time: 2020-04-30 10:00:17

#   ____ _ _        _    _ _                     
#  / ___(_) |_     / \  | (_) __ _ ___  ___  ___ 
# | |  _| | __|   / _ \ | | |/ _` / __|/ _ \/ __|
# | |_| | | |_   / ___ \| | | (_| \__ \  __/\__ \
#  \____|_|\__| /_/   \_\_|_|\__,_|___/\___||___/
#

# Re-import bash-colours
source "$(dirname $(readlink -e ${BASH_SOURCE[0]}))/bash-colours.sh"

# Add some git aliases
if [ $(command -v git) ] && [ -f ${HOME}/.gitconfig ]; then

    # Git commit tree view
    _ADD_ALIAS='log --all --graph --decorate --oneline --simplify-by-decoration';
    if [[ ! $( grep "${_ADD_ALIAS}" ${HOME}/.gitconfig ) ]]; then
        git config --global alias.ref-tree "${_ADD_ALIAS}";
    fi

    # Git branch tree view
    _ADD_ALIAS='!cd $(git rev-parse --git-dir)/refs/heads && tree';
    if [[ ! $( grep "${_ADD_ALIAS}" ${HOME}/.gitconfig ) ]]; then
        git config --global alias.branch-tree "${_ADD_ALIAS}";
    fi

    # Git get branch parent
    _ADD_ALIAS='!git show-branch -a 2>/dev/null | grep "*" | grep -v $(git rev-parse --abbrev-ref HEAD) | head -n1 | sed "s/.*\[\(.*\)\].*/\1/"'
    if [[ ! $( grep "${_ADD_ALIAS}" ${HOME}/.gitconfig ) ]]; then
        git config --global alias.branch-parent "${_ADD_ALIAS}";
    fi

    # Git show branch todos compared to master
    _ADD_ALIAS='!git --no-pager diff -U0 master | grep -i "^+.*TODO" | sed "s/^+//" | git --no-pager grep -nFf - 2> /dev/null'
    if [[ ! $( grep "${_ADD_ALIAS}" ${HOME}/.gitconfig ) ]]; then
        git config --global alias.branch-todos "${_ADD_ALIAS}";
    fi

    # Run a test on the changes prior to committing
    _ADD_ALIAS='!bash -c "source '${BASH_SOURCE}' && git_pre_commit_tests 2> /dev/null"'
    if [[ ! $( grep "${_ADD_ALIAS}" ${HOME}/.gitconfig ) ]]; then
        git config --global alias.pre-commit-test "${_ADD_ALIAS}";
    fi

    # Run a test on the changes prior to committing
    _ADD_ALIAS='! f() { echo $@ | xargs -I {} bash -c "source '${BASH_SOURCE}' && git_codesniffer {}" ; }; f '
    if [[ ! $( grep "${_ADD_ALIAS}" ${HOME}/.gitconfig ) ]]; then
        git config --global alias.codesniffer "${_ADD_ALIAS}";
    fi

    # Run the git branch clean up
    _ADD_ALIAS='!bash -c "source '${BASH_SOURCE}' && git_clear_multiple_branches $@"'
    if [[ ! $( grep "${_ADD_ALIAS}" ${HOME}/.gitconfig ) ]]; then
        git config --global alias.branch-cleanup "${_ADD_ALIAS}";
    fi

    # Git graph the current commit log
    # Ref: https://stackoverflow.com/questions/1838873/visualizing-branch-topology-in-git/34467298#34467298
    #   Added '--all' to force all refs
    _ADD_ALIAS='log --all --graph --abbrev-commit --decorate --format=format:"%C(bold blue)%h%C(reset) - %C(bold cyan)%aD%C(reset) %C(bold green)(%ar)%C(reset)%C(auto)%d%C(reset)%n""          %C(white)%s%C(reset) %C(dim white)- %an%C(reset)"'
    grep "${_ADD_ALIAS}" ${HOME}/.gitconfig
    if [[ ! $( grep "${_ADD_ALIAS}" ${HOME}/.gitconfig ) ]]; then
        git config --global alias.graph "${_ADD_ALIAS}";
    fi
    _ADD_ALIAS='log --all --graph --abbrev-commit --decorate --format=format:"%C(bold blue)%h%C(reset) - %C(bold cyan)%aD%C(reset) %C(bold green)(%ar)%C(reset) %C(bold cyan)(committed: %cD)%C(reset) %C(auto)%d%C(reset)%n""          %C(white)%s%C(reset)%n""          %C(dim white)- %an <%ae> %C(reset) %C(dim white)(committer: %cn <%ce>)%C(reset)"'
    if [[ ! $( grep "${_ADD_ALIAS}" ${HOME}/.gitconfig ) ]]; then
        git config --global alias.graph-detailed "${_ADD_ALIAS}";
    fi
fi

git_pre_commit_tests () {
    #Help:
    #>>
    # usage:                git_pre_commit_test
    # description:          Test git repo for the following strings:
    # description:          :    'ob_flush', 'var_dump', 'die(', 'TODO'
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;
    __check_strings=(
        "ob_flush"
        "var_dump"
        "die("
        "TODO"
        "console.log"
    )
    for check_string in "${__check_strings[@]}"; do
        echo -e "${CLMAG}Checking repo for ${CLGREEN}'${check_string}'${CLMAG}"
        echo -e "${CNORM}..."
        if git --no-pager diff | grep -iq "^+.*${check_string}"; then
            git status --short --branch $*; \
                git --no-pager diff | \
                grep "^+.*${check_string}" | \
                sed "s/^+//" | \
                git --no-pager grep -nFf - 2> /dev/null;
        else
            echo -e "${CLRED}No instances found.${CNORM}"
        fi
        echo
    done
}

git_codesniffer () {
    #Help:
    #>>
    # usage:                git_codesniffer [OPTIONS]
    # options:              --php-standard=<standard>                           Select PHP standard to test files against (default: 'PSR2')
    # options:              --files=<files to test>                             A comma separated list of files to be tested
    # options:              --blacklist=<files to ignore>                       A comma separated list of files to ignore form tests
    # options:              --branch=<git branch>                               Test files that have changed between current branch and <git branch> (default: 'master' if not branch is provided)
    # options:              --uncommitted                                       Test Uncommitted files (default behavior)
    # options:              --staged                                            Same as --uncommitted However used to view only staged files
    # options:              --exit-on-fail                                      Exit the tests when the first file fails
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;

    # Ensure all dependencies are installed
    DEP_CHECK='true';
    if [[ ! -x $(command -v phpcs) ]]; then
        echo 'Missing dependency phpcs. Run these commands:';
        echo '      1) `composer global require squizlabs/php_codesniffer`';
        echo '      2) `ln -sf $(composer config --global home)/vendor/bin/* ~/.local/bin/`';
        return;
    fi

    __print_header () {
            HEADER_WIDTH="120";
            HEADER_BORDER="$(printf '%0.1s' '#'{1..120})";
            HEADER_PADDING="$(printf '%0.1s' ' '{1..120})";
            TEXT_1="$(echo ${1} | base64 --decode)";
            TEXT_2="$(echo ${2} | base64 --decode)";
            TEXT_3="$(echo ${3} | base64 --decode)";
            echo -ne "${CHEAD}";
            printf "###%*.*s###\n" 0 "${HEADER_WIDTH}" "${HEADER_BORDER}";
            printf "###%*.*s###\n" 0 "${HEADER_WIDTH}" "${HEADER_PADDING}";

            printf "###%*.*s%s%*.*s###\n" 0 0 "${HEADER_PADDING}" "${TEXT_1}" 0 "$(((HEADER_WIDTH-${#TEXT_1})))" "${HEADER_PADDING}";
            printf "###%*.*s${CLMAG}%s${CHEAD}%*.*s###\n" 0 0 "${HEADER_PADDING}" "${TEXT_2}" 0 "$(((HEADER_WIDTH-${#TEXT_2})))" "${HEADER_PADDING}";
            printf "###%*.*s${CLBLUE}%s${CHEAD}%*.*s###\n" 0 0 "${HEADER_PADDING}" "${TEXT_3}" 0 "$(((HEADER_WIDTH-${#TEXT_3})))" "${HEADER_PADDING}";

            printf "###%*.*s###\n" 0 "${HEADER_WIDTH}" "${HEADER_PADDING}";
            printf "###%*.*s###\n" 0 "${HEADER_WIDTH}" "${HEADER_BORDER}";
            echo -ne "${CNORM}";
    }
    __print_test_ignored () {
        echo -ne "${CNORM}";
        echo -ne "Test Result:      ${CLIGNORED}|IGNORED|"
        echo -ne "${CNORM}";
        echo
        echo
    }
    __print_test_passed () {
        echo -ne "${CNORM}";
        echo -ne "Test Result:      ${CLPASSED}|PASSED|"
        echo -ne "${CNORM}";
        echo
        echo
    }
    __print_test_failed () {
        echo -ne "${CNORM}";
        echo -ne "Test Result:      ${CLERRORS}|ERRORS FOUND|"
        echo -ne "${CNORM}";
        echo
        echo
    }

    # Default ARGS:
    FILES="";
    BLACKLIST="";
    GIT_BRANCH="";
    ## Set default PHP standard PSR2
    PHP_STANDARD="PSR2";
    ## Default selection of files - test uncommitted changes
    GIT_DIFF="uncommitted"
    CHECKING_FILES_TEXT="uncommitted";
    ## Default option to break on error default is false
    BREAK_ON_ERROR="false"

    # Check ARGS for errors prior to processing
    if grep -qe '--branch.*--uncommitted\|--uncommitted.*--branch' <<< "${@}"; then
        echo "ERROR: Unable to test both between branches and uncommitted changes on this branch.";
        echo "Run the command again without using both '--branch' and '--uncommitted' arguments";
        echo 
        ${FUNCNAME[0]} --help
        return 1;
    fi
    if grep -qe '--branch.*--staged\|--staged.*--branch' <<< "${@}"; then
        echo "ERROR: Unable to test both between branches and staged changes on this branch.";
        echo "Run the command again without using both '--branch' and '--staged' arguments";
        echo 
        ${FUNCNAME[0]} --help
        return 1;
    fi
    if grep -qe '--uncommitted.*--staged\|--staged.*--uncommitted' <<< "${@}"; then
        echo "ERROR: Unable to test both uncommitted and staged changes at the same time.";
        echo "Run the command again without using both '--uncommitted' and '--staged' arguments";
        echo 
        ${FUNCNAME[0]} --help
        return 1;
    fi

    for ARG in ${@}; do

        case ${ARG} in  
            --help)
                return 0;
                ;;
            --php-standard*)
                PHP_STANDARD=$(echo ${ARG} | awk -F'=' '{print $2}');
                ;;
            --branch*)
                BRANCH_PROVIDED=$(echo ${ARG} | awk -F'=' '{print $2}');
                if [[ -z ${BRANCH_PROVIDED} ]]; then
                    GIT_BRANCH="master";
                else
                    GIT_BRANCH=${BRANCH_PROVIDED};
                fi
                GIT_DIFF="false";
                CHECKING_FILES_TEXT="changes from this branch compared to the '${GIT_BRANCH}' branch";
                ;;
            --uncommitted)
                GIT_BRANCH="";
                GIT_DIFF="uncommitted";
                CHECKING_FILES_TEXT="uncommitted files";
                ;;
            --staged)
                GIT_BRANCH="";
                GIT_DIFF="staged";
                CHECKING_FILES_TEXT="staged files";
                ;;
            --files*)
                FILES=$(echo ${ARG} | awk -F'=' '{print $2}' | tr ',' ' ');
                ;;
            --blacklist*)
                BLACKLIST=$(echo ${ARG} | awk -F'=' '{print $2}');
                ;;
            --exit-on-fail)
                BREAK_ON_ERROR="true";
                ;;
            *) 
                ;;
        esac

    done

    if [[ -z ${FILES} ]]; then
        if [[ ! -z ${GIT_BRANCH} ]]; then
            echo 
            FILES=$(git diff --name-only ${GIT_BRANCH});
        elif [[ ${GIT_DIFF} = 'staged' ]]; then
            FILES=$(git diff --name-only --cached);
        elif [[ ${GIT_DIFF} = 'uncommitted' ]]; then
            FILES=$(git diff --name-only);
        fi
    fi

    if grep -qe '.php' <<< "${FILES}"; then
        echo "Running PHP_CodeSniffer on ${CHECKING_FILES_TEXT}...";
        echo
        for FILE in ${FILES}; do
            if [[ -e ${FILE} && ! -z ${PHP_STANDARD} && ${FILE} == *.php ]]; then
                CMD="phpcs --standard=${PHP_STANDARD} ${FILE}";

                # Print header
                TEXT_1=$(echo "        Checking formatting on file:" | base64 -w0);
                TEXT_2=$(echo "           '${FILE}'" | base64 -w0);
                TEXT_3=$(echo "           "'`'"${CMD}"'`' | base64 -w0);
                __print_header "${TEXT_1}" "${TEXT_2}" "${TEXT_3}";


                # Check if this file is in the blacklist...
                if grep -qe "${FILE}" <<< "${BLACKLIST}"; then
                    # Ignore this file
                    echo
                    __print_test_ignored;
                    continue;
                fi

                bash -c "${CMD}";
                RESULT=$?;
                if [[ ${RESULT} < 1 ]]; then
                    echo
                    __print_test_passed;
                else
                    __print_test_failed;
                    if [[ ${BREAK_ON_ERROR} == 'true' ]]; then
                        break
                    fi
                fi
            fi
        done
    fi
}

# Credit: https://jonlabelle.com/snippets/view/shell/multi-select-menu-in-bash
git_clear_multiple_branches () {
    #Help:
    #>>
    # usage:                git_clear_multiple_branches [OPTIONS]
    # options:              --filter=<glob>                                     Filter the branches list
    # options:              --force                                             Allow deleting the branch irrespective of its merged status
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;
    
    __branch_filter='*'
    __force_delete=''
    for ARG in ${@}; do

        case ${ARG} in  
            *help|-h)
                return 0;
                ;;
            --filter*)
                __branch_filter=$(echo ${ARG} | awk -F'=' '{print $2}');
                ;;
            --force)
                __force_delete='--force';
                ;;
            *) 
                ;;
        esac

    done

    # Fetch list of current branches
    options=( $(git branch --list "${__branch_filter}" | grep -v '*') )
    
    menu() {
        echo "Branches:"
        for i in ${!options[@]}; do
            printf "%3d%s) %s\n" $((i+1)) "${choices[i]:- }" "${options[i]}"
        done
        [[ "$msg" ]] && echo "$msg"; :
    }
    
    prompt="Check an option (again to uncheck, ENTER when done): "
    while menu && read -rp "$prompt" num && [[ "$num" ]]; do
        [[ "$num" != *[![:digit:]]* ]] &&
        (( num > 0 && num <= ${#options[@]} )) ||
        { msg="Invalid option: $num"; continue; }
        ((num--)); msg="${options[num]} was ${choices[num]:+un}checked"
        [[ "${choices[num]}" ]] && choices[num]="" || choices[num]="+"
    done

    
    echo 
    printf "You selected"; msg=" nothing"
    __selected_branches=""
    for i in ${!options[@]}; do
        [[ "${choices[i]}" ]] && { printf " %s" "${options[i]}"; msg=""; } && __selected_branches="${__selected_branches} ${options[i]}"
    done
    echo 
    echo 
    git branch --delete ${__force_delete} ${__selected_branches}
    echo 
}

gitversion() {
    # NOTE: This function will only wrap the gitversion command. 
    #   Do not add any printout or help to this function as this allows the output to be piped
    docker run -ti --rm \
        --user=$(id -u) \
        --workdir=$(pwd)  \
        --volume=$(pwd):$(pwd):rw \
        --entrypoint='' \
        gittools/gitversion \
        /tools/dotnet-gitversion ${@}
}

git_configure_ssh_key_for_single_project() {
    #Help:
    #>>
    # usage:                git_configure_ssh_key_for_single_project [OPTIONS]
    # options:              --ssh-key=<string>                                  The path to the SSH key to assign to this repository
    # options:              --user=<string>                                     The username (Default: 'Josh.5')
    # options:              --email=<string>                                    The email (Default: 'jsunnex@gmail.com')
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;

    local __git_configure_ssh_key_ssh_key=""
    local __git_configure_ssh_key_name="Josh.5"
    local __git_configure_ssh_key_email="jsunnex@gmail.com"

    for arg in ${@}; do
        case ${arg} in  
            --ssh-key*)
                __git_configure_ssh_key_ssh_key=$(echo ${arg} | awk -F'=' '{print $2}');
                ;;
            *) 
                ;;
        esac
    done
    if [ -z ${__git_configure_ssh_key_ssh_key:-} ]; then
        __ssh_select_key
        __git_configure_ssh_key_ssh_key="${HOME}/.ssh/keys/${my_ssh_selected_key}"
    fi
    local __ssh_private_key_abspath="$(readlink -e ${__git_configure_ssh_key_ssh_key/#\~/$HOME})"
    if [[ ! -f ${__ssh_private_key_abspath} ]]; then
        echo "You must specify the path to the local key."
        __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} --help && return 1;
    fi
    git config --local user.name "${__git_configure_ssh_key_name}"
    git config --local user.email "${__git_configure_ssh_key_email}"
    git config --local core.sshCommand "ssh -o IdentitiesOnly=yes -i ${__ssh_private_key_abspath} -F /dev/null"
    git submodule foreach git config --local core.sshCommand "ssh -o IdentitiesOnly=yes -i ${__ssh_private_key_abspath} -F /dev/null"
    # Export the command for this instance
    export GIT_SSH_COMMAND="ssh -o IdentitiesOnly=yes -i ${__ssh_private_key_abspath} -F /dev/null"
}

git_clone_ssh() {
    #Help:
    #>>
    # usage:                git_clone_ssh REMOTE PATH
    # description:          Creates a clone of a git repository while providing a selection of SSH keys to use for that clone.
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;

    __ssh_select_key
    local __git_configure_ssh_key_ssh_key="${HOME}/.ssh/keys/${my_ssh_selected_key}"
    echo ${__git_configure_ssh_key_ssh_key}
    if [[ ! -f ${__git_configure_ssh_key_ssh_key} ]]; then
        echo "You must specify the path to the local key."
        __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} --help && return 1;
    fi
    GIT_SSH_COMMAND="ssh -i ${__git_configure_ssh_key_ssh_key} -o IdentitiesOnly=yes" git clone ${@}
}

git_temp_clone() {
    #Help:
    #>>
    # usage:                git_temp_clone <project>
    # description:          Creates a clone of a git repository in a temporary location.
    # options:              --ssh-key=<string>                The path to the SSH key to use for cloning
    # options:              --no-cd                           Don't switch to directory after clone
    # options:              --vscode                          Open VSCode after cloning
    #<<
    __read_function_help "${BASH_SOURCE[0]}" "${FUNCNAME[0]}" "${@}" 2> /dev/null && return;
    # Locals
    local __project_url
    local __project_path
    local __temp_directory
    local __ssh_key
    local __open_vscode
    local __cd

    __project_url=""
    __ssh_key=""
    __open_vscode="false"
    __cd="true"
    for arg in "${@}"; do
        case ${arg} in
            --ssh-key*)
                __ssh_key=$(echo ${arg} | awk -F'=' '{print $2}')
                ;;
            --vscode)
                __open_vscode="true"
                ;;
            --no-cd)
                __cd="false"
                ;;
            *)
                __project_url="${arg}";
                continue;
                ;;
        esac
    done

    # Configure SSH key to use if requested
    if [[ -n ${__ssh_key} ]]; then
      __ssh_key="$(readlink -e "${__ssh_key/#\~/$HOME}")"
      if [[ ! -f ${__ssh_key} ]]; then
          echo "You must specify the path to a local SSH key if you specify the '--ssh-key' param."
          __read_function_help "${BASH_SOURCE[0]}" "${FUNCNAME[0]}" --help && return 1;
      fi
    fi

    # Create a temp directory
    # Make this directory a subdirectory so we can avoid needing to "trust" it every time it is opened in vscode
    mkdir -p /tmp/git_temp_clone

    # Generate output path
    __temp_directory=${__project_url%".git"}
    __temp_directory=${__temp_directory##*"/"}
    __temp_directory="$(mktemp --directory --tmpdir="/tmp/git_temp_clone" --dry-run).${__temp_directory}"

    # Clone
    if [[ -n ${__ssh_key} ]]; then
        GIT_SSH_COMMAND="ssh -i ${__ssh_key} -o IdentitiesOnly=yes" git clone "${__project_url}" "${__temp_directory}"
    else
        git clone "${__project_url}" "${__temp_directory}"
    fi

    # Read created directory
    __project_path=$(ls "${__temp_directory}/")

    # Open or print out location
    if [[ "${__cd}" == "true" ]]; then
      cd "${__temp_directory}/" || return 1
    fi
    if [[ "${__open_vscode}" == "true" ]]; then
      command code "${__temp_directory}/"
    else
      ls -la "${__temp_directory}/"
    fi
}

#   ____ _ _   _           _     
#  / ___(_) |_| |__  _   _| |__  
# | |  _| | __| '_ \| | | | '_ \ 
# | |_| | | |_| | | | |_| | |_) |
#  \____|_|\__|_| |_|\__,_|_.__/ 
#

# Get latest release tag from github
github_get_latest_release() {
    #Help:
    #>>
    # usage:                github_get_latest_release <REPO>
    # examples:             "github_get_latest_release josh5/unmanic"         -      Get the latest release
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;
    REPO="${1}";
    curl --silent "https://api.github.com/repos/${REPO}/releases/latest" \
        | grep '"tag_name":' \
        | sed -E 's/.*"([^"]+)".*/\1/';
}

# Get all release tags from github
github_get_all_release() {
    #Help:
    #>>
    # usage:                github_get_latest_release <REPO>
    # examples:             "github_get_all_release josh5/unmanic"         -      Get an array of all releases
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;
    REPO="${1}";
    curl --silent "https://api.github.com/repos/${REPO}/releases" \
        | grep '"tag_name":' \
        | sed -E 's/.*"([^"]+)".*/\1/';
}

# Get all release tags from github
github_get_release_download_url() {
    #Help:
    #>>
    # usage:                github_get_latest_release <REPO> <TAG>( <TYPE>)
    # examples:             "github_get_release_download_url josh5/unmanic latest"         -      Fetch all download URLs for the tag 'latest'
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;
    REPO="${1}";
    TAG="${2}";
    TYPE="${3}";
    RES=$(curl --silent "https://api.github.com/repos/${REPO}/releases/tags/${TAG}" \
        | grep '"browser_download_url":' \
        | sed -E 's/.*"([^"]+)".*/\1/');
    if [[ -z ${TYPE} ]]; then
        echo "${RES}";
    else
        echo "${RES}"| grep -P "${TYPE}" | head -n1;
    fi
}

# 
github_search_prs_in_past_month() {
    #Help:
    #>>
    # usage:                github_search_prs_in_past_month
    # options:              --user=<string>                     The user. Eg 'josh5'
    # options:              --org=<string>                      The organisation. Eg 'Unmanic'
    # options:              --author=<string>                   The author. Eg 'Josh.5'
    # options:              --count                             Only return a total count
    # examples:             "github_search_prs_in_past_month --user=josh5 --count"         -      Get count of all merged PRs this month
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;
    REPO="${1}";

    __total_count=""

    __date=$(date --date="1 month ago" -u +"%Y-%m-%d")
    __query="type%3Apr+merged%3A%3E${__date}"

    for ARG in ${@}; do
        case ${ARG} in
            --count)
                __total_count="true";
                ;;
            --user*)
                # https://docs.github.com/en/search-github/searching-on-github/searching-issues-and-pull-requests#search-within-a-users-or-organizations-repositories
                __query="${__query:-}+user%3A$(echo ${ARG} | awk -F'=' '{print $2}')";
                ;;
            --org*)
                # https://docs.github.com/en/search-github/searching-on-github/searching-issues-and-pull-requests#search-within-a-users-or-organizations-repositories
                __query="${__query:-}+org%3A$(echo ${ARG} | awk -F'=' '{print $2}')";
                ;;
            --author*)
                # https://docs.github.com/en/search-github/searching-on-github/searching-issues-and-pull-requests#search-by-author
                __query="${__query:-}+author%3A$(url_encode $(echo ${ARG} | awk -F'=' '{print $2}'))";
                ;;
            *) 
                ;;
        esac
    done
    result=$(curl --silent -H "Accept: application/vnd.github+json" "https://api.github.com/search/issues?q=${__query}")

    if [[ ! -z ${__total_count} ]]; then
        echo "${result}" | jq -r '.total_count'
        return
    fi
    echo "${result}"
}

# 
github_search_commits_authored_in_past_month() {
    #Help:
    #>>
    # usage:                github_search_commits_authored_in_past_month
    # options:              --user=<string>                     The user. Eg 'josh5'
    # options:              --org=<string>                      The organisation. Eg 'Unmanic'
    # options:              --author=<string>                   The author. Eg 'Josh.5'
    # options:              --author-email=<string>             The author's email. Eg 'jsunnex@gmail.com'
    # options:              --count                             Only return a total count
    # examples:             "github_search_commits_authored_in_past_month --user=josh5 --count"         -      Get count of all merged PRs this month
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;
    REPO="${1}";

    __total_count=""

    __date=$(date --date="1 month ago" -u +"%Y-%m-%d")
    __query="author-date%3A%3E${__date}"
    for ARG in ${@}; do
        case ${ARG} in
            --count)
                __total_count="true";
                ;;
            --user*)
                # https://docs.github.com/en/search-github/searching-on-github/searching-commits#search-within-a-users-or-organizations-repositories
                __query="${__query:-}+user%3A$(echo ${ARG} | awk -F'=' '{print $2}')";
                ;;
            --org*)
                # https://docs.github.com/en/search-github/searching-on-github/searching-commits#search-within-a-users-or-organizations-repositories
                __query="${__query:-}+org%3A$(echo ${ARG} | awk -F'=' '{print $2}')";
                ;;
            --author*)
                # https://docs.github.com/en/search-github/searching-on-github/searching-commits#search-by-author-or-committer
                __query="${__query:-}+author%3A$(url_encode $(echo ${ARG} | awk -F'=' '{print $2}'))";
                ;;
            --author-email*)
                # https://docs.github.com/en/search-github/searching-on-github/searching-commits#search-by-author-or-committer
                __query="${__query:-}+author-email%3A$(url_encode $(echo ${ARG} | awk -F'=' '{print $2}'))";
                ;;
            *) 
                ;;
        esac
    done
    result=$(curl --silent -H "Accept: application/vnd.github+json" "https://api.github.com/search/commits?q=${__query}")

    if [[ ! -z ${__total_count} ]]; then
        echo "${result}" | jq -r '.total_count'
        return
    fi
    echo "${result}"
}

#   ____ _ _   _       _     
#  / ___(_) |_| | __ _| |__  
# | |  _| | __| |/ _` | '_ \ 
# | |_| | | |_| | (_| | |_) |
#  \____|_|\__|_|\__,_|_.__/ 
#

# Export some default CI values
export CI_API_V4_URL="https://gitlab.com/api/v4"

# Toggle GitLab CI tokens
gitlab_assume_token() {
    #Help:
    #>>
    # usage:                gitlab_assume_token [OPTIONS]
    # options:              --account=<string>                              A configure GitLab account
    # examples:             "gitlab_assume_token"                           Select an account to configure the credentials for
    # examples:             "gitlab_assume_token --account=personal"        Configure my personal GitLab account credentials
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;
    

    # Token Name:   CI_JOB_WRITE_TOKEN
    # Permissions:  api, write_repository, write_registry
    local gitlab_config_file="${HOME}/.gitlab/config.ini"
    local account="default"

    # Ensure the config file exists before continuing
    mkdir -p $(dirname ${gitlab_config_file:?})
    if [[ ! -f "${gitlab_config_file}" ]]; then
        echo "ERROR: The file '${gitlab_config_file}' does not exist.";
        return 1;
    fi
    
    while [[ $# -gt 0 ]]; do
        case "$1" in
            --account=*)
                account="${1#*=}"
                shift
                ;;
            *)
            echo "Unknown option: $1"
            return 1
            ;;
        esac
    done

    local profiles=($(grep -oP '(?<=\[profile).+(?=\]$)' ${gitlab_config_file}))
    if [[ -n ${account} && " ${profiles[@]} " =~ " ${account} " ]]; then
        selected_profile="${account}"
    else
        echo "Select a GitLab profile to assume the role of:"
        select selected_profile in "${profiles[@]}"; do
            echo
            echo "Selected profile: '${selected_profile}'"
            break
        done
    fi

    # Read config file based ont the selected account
    local gl_ci_api_v4_url=$(sed -n '/^\[profile '${selected_profile}'\]/,/^\[/p' ${gitlab_config_file} \
        | grep -oP '^CI_API_V4_URL\s*=\s*.+$' | cut -d'=' -f2 | tr -d '[:space:]')
    [ -n "${gl_ci_api_v4_url}" ] && export CI_API_V4_URL="${gl_ci_api_v4_url:?}"
    local gl_ci_job_token=$(sed -n '/^\[profile '${selected_profile}'\]/,/^\[/p' ${gitlab_config_file} \
        | grep -oP '^CI_JOB_TOKEN\s*=\s*.+$' | cut -d'=' -f2 | tr -d '[:space:]')
    [ -n "${gl_ci_job_token}" ] && export CI_JOB_TOKEN="${gl_ci_job_token:?}" \
        && export CI_JOB_MAINTAINER_TOKEN="${gl_ci_job_token:?}" \
        && export CI_JOB_WRITE_TOKEN="${gl_ci_job_token:?}" \
        && export CI_JOB_READ_API_TOKEN="${gl_ci_job_token:?}"
    local gl_ci_registry_user=$(sed -n '/^\[profile '${selected_profile}'\]/,/^\[/p' ${gitlab_config_file} \
        | grep -oP '^CI_REGISTRY_USER\s*=\s*.+$' | cut -d'=' -f2 | tr -d '[:space:]')
    [ -n "${gl_ci_registry_user}" ] && export CI_REGISTRY_USER="${gl_ci_registry_user:?}"
    local gl_ci_registry=$(sed -n '/^\[profile '${selected_profile}'\]/,/^\[/p' ${gitlab_config_file} \
        | grep -oP '^CI_REGISTRY\s*=\s*.+$' | cut -d'=' -f2 | tr -d '[:space:]')
    [ -n "${gl_ci_registry}" ] && export CI_REGISTRY="${gl_ci_registry:?}"

    echo "Success, your gitlab env is now exported to:"
    echo "  - CI_API_V4_URL:            ${CI_API_V4_URL:?}"
    echo "  - CI_JOB_TOKEN:             ${CI_JOB_TOKEN:?}"
    echo "  - CI_JOB_MAINTAINER_TOKEN:  ${CI_JOB_TOKEN:?}"
    echo "  - CI_JOB_WRITE_TOKEN:       ${CI_JOB_TOKEN:?}"
    echo "  - CI_JOB_READ_API_TOKEN:    ${CI_JOB_TOKEN:?}"
    echo "  - CI_REGISTRY_USER:         ${CI_REGISTRY_USER:?}"
    echo "  - CI_REGISTRY:              ${CI_REGISTRY:?}"
}

gitlab_api_curl() {
    #Help:
    #>>
    # usage:                gitlab_api_curl [OPTIONS] <ADDITIONAL CURL ARGS>
    # description:          Provides a simple way to curl the GitLab API using the current exported token.
    # options:              --path=<string>             The path to query
    # options:              --get=<string>              Request is a GET (default)
    # options:              --post=<string>             Request is a POST
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;

    local __path="/"
    local __request="GET"
    local __curl_args=""

    while [[ $# -gt 0 ]]; do
        case "$1" in
            --path=*)
                __path="${1#*=}"
                shift
                ;;
            --get)
                __request="GET"
                shift
                ;;
            --post)
                __request="POST"
                shift
                ;;
            *)
                __curl_args="${1}"
                shift
                ;;
        esac
    done

    curl ${__curl_args:-} --request ${__request:?} --header "PRIVATE-TOKEN: ${CI_JOB_TOKEN:?}" "${CI_API_V4_URL:?}${__path:?}"
}

gitlab_project_id() {
    #Help:
    #>>
    # usage:                gitlab_project_id [OPTIONS]
    # description:          Will prompt you for a full project ID (including namespace)
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;

    local __path_with_namespace=""
    # Read pronect with namespace and url encode
    echo
    read -p "Specify the full project ID including namespace (eg. 'Josh5/bashrc.d'): " __path_with_namespace
    __project=$(python3 -c "import urllib.parse;print(urllib.parse.quote('${__path_with_namespace}', safe=''))")
    echo "${__project:?}"
}

# Remove bulk pipelines from a project
gitlab_bulk_remove_pipelines() {
    #Help:
    #>>
    # usage:                gitlab_bulk_remove_pipelines [OPTIONS]
    # description:          Will prompt you for a full project ID (including namespace)
    # description:          Using the give project ID, this function will remove the failed pipelines 100 at a time in desc order
    # options:              --project-id=<string>       The ID of the project
    # options:              --count=<number>            The number of pipeline results to fetch (Default: 100)
    # options:              --status=<string>           Filter pipelines by status (Default: failed, Options: [failed, skipped, success])
    # options:              --source=<string>           Filter pipelines by source
    # options:              --dry                       Carry out a dry run only
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;
    # Limit to 100 at a time
    __per_page=100
    # Fetch the Gitlab token if available
    if [[ ! -z ${CI_JOB_TOKEN} ]]; then
        __token=${CI_JOB_TOKEN}
    else
        echo
        read -p "Enter your Gitlab token: " __token
    fi
    local __path_with_namespace=""
    local __dry_run="false"
    local __status="failed"
    local __source=""
    local __name=""
    while [[ $# -gt 0 ]]; do
        case "$1" in
            --dry)
                __dry_run="true"
                shift
                ;;
            --project-id=*)
                __path_with_namespace="${1#*=}"
                shift
                ;;
            --count=*)
                __per_page="${1#*=}"
                shift
                ;;
            --status=*)
                __status="${1#*=}"
                shift
                ;;
            --source=*)
                __source="${1#*=}"
                shift
                ;;
            --name=*)
                __name="${1#*=}"
                shift
                ;;
            *)
            echo "Unknown option: $1"
            exit 1
            ;;
        esac
    done
    # Read pronect with namespace and url encode
    echo
    if [ -z "${__path_with_namespace}" ]; then
        read -p "Specify the full project ID including namespace (eg. 'Josh5/bashrc.d'): " __path_with_namespace
    fi
    __project=$(python3 -c "import urllib.parse;print(urllib.parse.quote('${__path_with_namespace}', safe=''))")
    # Create temp file so we can make multiple calls to this
    __tmp_file=$(mktemp)
    [[ ${__tmp_file} == "/" ]] && exit 1;
    # Fetch all the pipelines and loop over them
    curl --header "PRIVATE-TOKEN: ${__token}" "https://gitlab.com/api/v4/projects/${__project}/pipelines?per_page=${__per_page}&sort=asc" > "${__tmp_file}"
    echo
    local __pipeline_ids_to_remove=()
    for __pipeline_id in $(cat "${__tmp_file}" | jq '.[].id') ; do
        # For every failed pipeline, make a DELETE request
        __pipeline_info=$(cat "${__tmp_file}" | jq -c ".[] | select( .id | contains(${__pipeline_id}))")
        # Filter by name
        if [ -n "${__name:-}" ]; then
            __pipeline_name="$(echo "${__pipeline_info}" | jq -cr '.name')"
            if [[ "${__pipeline_name}" != "${__name}" ]]; then
                echo "Ignoring pipeline '${__pipeline_id}' with name '${__pipeline_name}' - ${__pipeline_info}"
                echo
                continue
            fi
        fi
        # Filter by status
        if [ -n "${__status:-}" ]; then
            __pipeline_status="$(echo "${__pipeline_info}" | jq -cr '.status')"
            if [[ "${__pipeline_status}" != "${__status}" ]]; then
                echo "Ignoring pipeline '${__pipeline_id}' with status '${__pipeline_status}' - ${__pipeline_info}"
                echo
                continue
            fi
        fi
        # Filter by source
        if [ -n "${__source:-}" ]; then
            __pipeline_source="$(echo "${__pipeline_info}" | jq -cr '.source')"
            if [[ "${__pipeline_source}" != "${__source}" ]]; then
                echo "Ignoring pipeline '${__pipeline_id}' with source '${__pipeline_source}' - ${__pipeline_info}"
                echo
                continue
            fi
        fi
        # Add to removals array
        echo "Marking pipeline for removal '${__pipeline_id}' - ${__pipeline_info}"
        __pipeline_ids_to_remove[${#__pipeline_ids_to_remove[@]}]="${__pipeline_id}"
        echo
    done
    if [[ "${__dry_run:?}" != "true" ]]; then
        for __pipeline_id in ${__pipeline_ids_to_remove[@]}; do
            echo "Deleting pipeline '${__pipeline_id}'"
            curl --header "PRIVATE-TOKEN:${__token}" --request "DELETE" "https://gitlab.com/api/v4/projects/${__project}/pipelines/${__pipeline_id}"
            echo
        done
    else
        echo
        echo "(Executed dry run. Nothing was actually deleted)"
        echo
    fi
}

gitlab_manage_runners() {
    #Help:
    #>>
    # usage:                gitlab_manage_pipelines [OPTIONS]
    # description:          Manage a set of GitLab runners through the API 
    # options:              --query=<string>            Options [runners, jobs]
    # options:              --runner-id=<string>        The ID of the runner to query
    # options:              --project=<string>          Limit runners by project. (eg. 'Josh5/bashrc.d')
    # options:              --status=<string>           Limit runners or job query by status (runners: [online, offline, stale, never_contacted], jobs:[running, success, failed, canceled])
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;

    # Fetch the Gitlab token if available
    if [[ ! -z ${CI_JOB_TOKEN} ]]; then
        __token=${CI_JOB_TOKEN}
    else
        echo
        read -p "Enter your Gitlab token: " __token
    fi
    __gitlab_manage_runners_id=""
    __gitlab_manage_runners_query="jobs"
    __gitlab_manage_runners_status=""
    __gitlab_manage_runners_project=""

    for ARG in ${@}; do
        case ${ARG} in
            --query*)
                __gitlab_manage_runners_query=$(echo ${ARG} | awk -F'=' '{print $2}');
                ;;
            --runner-id*)
                __gitlab_manage_runners_id=$(echo ${ARG} | awk -F'=' '{print $2}');
                ;;
            --project*)
                __gitlab_manage_runners_project=$(python3 -c "import urllib.parse;print(urllib.parse.quote('$(echo ${ARG} | awk -F'=' '{print $2}')', safe=''))");
                ;;
            --status*)
                __gitlab_manage_runners_status=$(echo ${ARG} | awk -F'=' '{print $2}');
                ;;
            *) 
                ;;
        esac
    done
    # Create temp file so we can make multiple calls to this
    __tmp_file=$(mktemp)
    [[ ${__tmp_file} == "/" ]] && exit 1;

    # Build and execute query
    __query_args=""
    __query_args_join="?"
    if [[ ! -z "${__gitlab_manage_runners_status}" ]]; then
        __query_args="${__query_args}${__query_args_join}status=${__gitlab_manage_runners_status}"
        __query_args_join="&"
    fi
    if [[ "${__gitlab_manage_runners_query}" == "runners" ]]; then
        if [[ ! -z "${__gitlab_manage_runners_project}" ]]; then
            curl --header "PRIVATE-TOKEN: ${__token}" "https://gitlab.com/api/v4/projects/${__gitlab_manage_runners_project}/runners${__query_args}" > "${__tmp_file}"
        else
            curl --header "PRIVATE-TOKEN: ${__token}" "https://gitlab.com/api/v4/runners${__query_args}" > "${__tmp_file}"
        fi
    elif [[ "${__gitlab_manage_runners_query}" == "jobs" ]]; then
        curl --header "PRIVATE-TOKEN: ${__token}" "https://gitlab.com/api/v4/runners/${__gitlab_manage_runners_id}/jobs" > "${__tmp_file}"
    fi
    cat "${__tmp_file}" | jq
}

gitlab_gitversion() {
    #Help:
    #>>
    # usage:                gitlab_gitversion [OPTIONS]
    # options:              --ref=<string>      -   The git ref (branch/tag) to use on the remote repository.
    # options:              --commit=<string>   -   The git commit id to check. If not specified, the latest available commit on the specified branch will be used.
    # options:              --url=<string>      -   The url to the GitLab repository.
    # options:              --token=<string>    -   Your GitLab access token (Can be pulled from CI_JOB_TOKEN env variable).
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;
    # Parse args
    __gitlab_gitversion_cmd_args=""
    __gitlab_gitversion_run_help=""
    __gitlab_gitversion_url=""
    __gitlab_gitversion_token="${CI_JOB_TOKEN:-}"
    for arg in ${@}; do
        case ${arg} in
            --ref*)
                __gitlab_gitversion_cmd_args="${__gitlab_gitversion_cmd_args} /b $(echo ${arg} | awk -F'=' '{print $2}')";
                ;;
            --commit*)
                __gitlab_gitversion_cmd_args="${__gitlab_gitversion_cmd_args} /c $(echo ${arg} | awk -F'=' '{print $2}')";
                ;;
            --url*)
                __gitlab_gitversion_url=$(echo ${arg} | awk -F'=' '{print $2}')
                ;;
            --token*)
                __gitlab_gitversion_token=$(echo ${arg} | awk -F'=' '{print $2}')
                ;;
            h*)
                __gitlab_gitversion_run_help="true"
                ;;
            *) 
                ;;
        esac
    done
    # Run help if set
    if [[ "${__gitlab_gitversion_run_help}" == "true" ]]; then
        echo "here"
        gitversion /?
        return
    fi
    # Add params for parsing from remote git url
    if [[ ! -z "${__gitlab_gitversion_url}" ]]; then
        # Adds URL, username and password
        __gitlab_gitversion_cmd_args="${__gitlab_gitversion_cmd_args} /url ${__gitlab_gitversion_url} /u gitlab-ci-token /p ${__gitlab_gitversion_token}";
    fi
    # Run command
    echo "*** Running GitVersion in a docker container and piping output to 'gitversion.json' ***"
    echo gitversion ${__gitlab_gitversion_cmd_args} /dynamicRepoLocation $(pwd)
    gitversion ${__gitlab_gitversion_cmd_args} /dynamicRepoLocation $(pwd) | tee gitversion.json

    return

    echo gitversion /url ${__gitlab_gitversion_url} /u gitlab-ci-token /p ${__gitlab_gitversion_token} /b ${CI_COMMIT_REF_NAME} /c ${CI_COMMIT_SHA} /dynamicRepoLocation $(pwd)
    docker run -ti --rm \
        --user=$(id -u) \
        --workdir=$(pwd)  \
        --volume=$(pwd):$(pwd):rw \
        --entrypoint '' \
        gittools/gitversion \
        /tools/dotnet-gitversion /url ${__gitlab_gitversion_url} /u gitlab-ci-token /p ${__gitlab_gitversion_token} /b ${CI_COMMIT_REF_NAME} /c ${CI_COMMIT_SHA} /dynamicRepoLocation ${CI_PROJECT_DIR} | tee gitversion.json
}

gitlab_release_branch_version() {
    #Help:
    #>>
    # usage:                gitlab_release_branch_version [OPTIONS]
    # description:          Generate a short and long version string for a release branch hosted on GitLab.
    # description:          Requires access to the GitLab API.
    # options:              --project-id=<string>       The ID of the project.
    # options:              --branch=<string>           The release branch to use.
    # options:              --commit=<string>           The git commit id to check. Must be on the branch specified.
    # examples:             gitlab_release_branch_version --branch=$(git rev-parse --abbrev-ref HEAD) --commit=$(git rev-parse --short HEAD) --project-id=""
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;
    # Limit to 100 at a time
    __per_page=100
    # Fetch the Gitlab token if available
    if [[ ! -z ${CI_JOB_TOKEN} ]]; then
        __token=${CI_JOB_TOKEN}
    else
        echo
        read -p "Enter your Gitlab token: " __token
    fi
    local __path_with_namespace=""
    local __branch=""
    local __commit=""
    while [[ $# -gt 0 ]]; do
        case "$1" in
            --project-id=*)
                __path_with_namespace="${1#*=}"
                shift
                ;;
            --branch=*)
                __branch="${1#*=}"
                shift
                ;;
            --commit=*)
                __commit="${1#*=}"
                shift
                ;;
            *)
            echo "Unknown option: $1"
            exit 1
            ;;
        esac
    done


    # Read pronect with namespace and url encode
    echo
    if [ -z "${__path_with_namespace}" ]; then
        read -p "Specify the full project ID including namespace (eg. 'Josh5/bashrc.d'): " __path_with_namespace
    fi
    __project=$(python3 -c "import urllib.parse;print(urllib.parse.quote('${__path_with_namespace}', safe=''))")
    # Create temp file so we can make multiple calls to this
    __tmp_file=$(mktemp)
    [[ ${__tmp_file} == "/" ]] && exit 1;


    echo curl --header "PRIVATE-TOKEN: ${__token}" "${CI_API_V4_URL:?}/projects/${__project:?}/repository/compare?from=master&to=${latest_release_branch:?}"
    return
    repo_branches=$(curl --header "PRIVATE-TOKEN:  ${__token:?}" "${CI_API_V4_URL:?}/projects/${__project:?}/repository/branches")
    latest_release_branch=$(echo "${repo_branches:?}" | jq -r '.[] | select(.name | startswith("release/")) | .name' | sort -r | head -n1)
    echo ${latest_release_branch}
    echo "${repo_branches:?}" | jq 
    return
    
    # TODO: Get count of commits since last release branch
    #echo "${repo_branches:?}" | jq -r '.[] | select(.name | startswith("release/"))'

    echo curl --header "PRIVATE-TOKEN: ${__token}" "${CI_API_V4_URL:?}/projects/${__project:?}/repository/compare?from=master&to=${latest_release_branch:?}"
    branch_info=$(curl --header "PRIVATE-TOKEN: ${__token}" "${CI_API_V4_URL:?}/projects/${__project:?}/repository/compare?from=master&to=${latest_release_branch:?}")
    echo ${branch_info} | jq
    commit_count=0

    return

    # Get list of commits since master branch
    curl --header "PRIVATE-TOKEN: ${__token:?}" "${CI_API_V4_URL:?}/projects/${__project:?}/repository/compare?from=master&to=${__branch:?}" > "${__tmp_file}"
    # Get branch commit sha
    initial_branching_commit_sha=$(cat "${__tmp_file}" | jq -r '.commits[0].parent_ids[0]')
    echo $initial_branching_commit_sha
    # Get a count of commits on this branch
    branch_commit_count=$(cat "${__tmp_file}" | jq -r '.commits | length')
    echo $branch_commit_count

    # Check if the current branch starts with "release" (case-insensitive)
    __branch=release/1.1.x
    if [[ "${__branch:-}" =~ ^release ]]; then
        # Extract the major and minor version numbers (as shown in the previous answer)
        version_numbers=$(echo "${__branch:?}" | grep -oE '[0-9]+\.[0-9]+')
        MAJOR=$(echo "${version_numbers:?}" | cut -d'.' -f1)
        MINOR=$(echo "${version_numbers:?}" | cut -d'.' -f2)
        PATCH=${branch_commit_count:?}

        echo "Branch starts with 'release'. MAJOR=$MAJOR, MINOR=$MINOR, PATCH=$PATCH"
        echo "Commit count compared to master branch"
    else
        echo "Branch does not start with 'release'."
        # Extract the major and minor version numbers (as shown in the previous answer)
        version_numbers=$(echo "${__branch:?}" | grep -oE '[0-9]+\.[0-9]+')
        MAJOR=$(echo "${version_numbers:?}" | cut -d'.' -f1)
        MINOR=$(echo "${version_numbers:?}" | cut -d'.' -f2)

        echo "Branch starts with 'release'. MAJOR=$MAJOR, MINOR=$MINOR"
        echo "Commit count compared to master branch"
    fi
}

# Create a GitLab CI container development environment
function gitlab_ci_env {
    #Help:
    #>>
    # usage:                gitlab_ci_env [OPTIONS]
    # description:          Start a GitLab CI environment and attach to a shell within the started container for the specified Docker image.
    # options:              --image=<string>                            The docker image to use
    # options:              --name=<name>                               What to name the container (Default: 'gl_ci_env_' plus a randomly generated string)
    # options:              --pull                                      Force an update of the docker image before running the container
    # options:              --cached-home-dir                           Mount a common cached version of the home directory for the user inside the CI env container
    # options:              --clear-cached-home-dir                     Clear common cached home directory and exit
    # options:              --dind                                      Configure image with docker in docker
    # options:              --stop-all                                  Stop all 'gl_ci_env_*' docker containers
    # options:              --exec-root                                 Open a root console inside the last started CI env container
    # options:              --configure                                 Configure the last started CI env container according to the ~/.gitlab/gitlab-ci-env-setup.sh file
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;

    echo "This function is Deprecated. Use docker_sandbox_env instead."
    __read_function_help $(cd $(dirname ${BASH_SOURCE[0]}) && pwd)/docker.sh docker_sandbox_env --help && return 1;

    # Defaults
    local gl_ci_env_image="node:16"
    local gl_ci_env_force_pull="false"
    local gl_ci_env_cache_home_dir="false"
    local gl_ci_env_clear_cache_home_dir="false"
    local gl_ci_env_container_name="gitlab_ci_env_$(echo ${RANDOM} | md5sum | head -c 4)"
    local gl_ci_env_network_name=""
    local gl_ci_env_extra_params=""
    local gl_ci_env_action=""
    local gl_ci_env_dind_container_name=""

    # Parse args
    for arg in ${@}; do
        if [[ "${arg}" =~ "--image" ]]; then
            gl_ci_env_image=$(echo ${arg} | awk -F'=' '{print $2}');
            continue;
        elif [[ "${arg}" =~ "--name" ]]; then
            gl_ci_env_container_name=$(echo ${arg} | awk -F'=' '{print $2}');
            continue;
        elif [[ "${arg}" =~ "--network" ]]; then
            gl_ci_env_network_name=$(echo ${arg} | awk -F'=' '{print $2}');
            continue;
        elif [[ "${arg}" = "--pull" ]]; then
            gl_ci_env_force_pull='true';
            continue;
        elif [[ "${arg}" = "--cached-home-dir" ]]; then
            gl_ci_env_cache_home_dir='true';
            continue;
        elif [[ "${arg}" = "--clear-cached-home-dir" ]]; then
            gl_ci_env_clear_cache_home_dir='true';
            continue;
        elif [[ "${arg}" = "--dind" ]]; then
            # Start a gl_ci_env_dind container for this env
            gl_ci_env_dind_container_name="${gl_ci_env_container_name}_dind"
            continue;
        elif [[ "${arg}" = "--stop-all" ]]; then
            gl_ci_env_action="stop-all"
            continue;
        elif [[ "${arg}" = "--configure" ]]; then
            # Source the gitlab-ci-env-setup.sh file to configure the container
            docker exec -ti -u root $(docker ps | grep 'gitlab_ci_env_' | awk '{print $1}' | head -n1) sh -c ". /tmp/.gitlab-ci-env-setup.sh";
            return;
        elif [[ "${arg}" = "--exec-root" ]]; then
            # Get the last started container and open a root console in it
            docker exec -ti -u root $(docker ps | grep 'gitlab_ci_env_' | awk '{print $1}' | head -n1) sh;
            return;
        fi
        gl_ci_env_extra_params="${gl_ci_env_extra_params} ${arg}";
    done

    # Create gitlab CI env config directory
    local gl_ci_env_config_dir="${HOME}/.gitlab"
    mkdir -p "${gl_ci_env_config_dir}"
    if [[ "${gl_ci_env_clear_cache_home_dir:-false}" = 'true' ]]; then
        rm -rf "${gl_ci_env_config_dir}/home"
        return
    fi
    touch "${gl_ci_env_config_dir}/gitlab-ci-env-setup.sh"

    # Configure CI env home directory
    local gl_ci_env_home_dir
    if [[ "${gl_ci_env_cache_home_dir}" = 'true' ]]; then
        gl_ci_env_home_dir="${gl_ci_env_config_dir}/home"
    else
        gl_ci_env_home_dir="$(mktemp --directory --suffix=gitlab_ci_env)/home"
        if [ -f "${HOME}/.npmrc" ]; then
            gl_ci_env_extra_params="${gl_ci_env_extra_params} --volume=${HOME}/.npmrc:${HOME}/.npmrc:ro";
            gl_ci_env_extra_params="${gl_ci_env_extra_params} --env NPM_CONFIG_USERCONFIG=${HOME}/.npmrc";
        fi
        if [ -d "${HOME}/.docker" ]; then
            gl_ci_env_extra_params="${gl_ci_env_extra_params} --volume=${HOME}/.docker:${HOME}/.docker:ro";
        fi
    fi
    mkdir -p "${gl_ci_env_home_dir}"

    # Start entrypoint
    echo '#!/usr/bin/env sh' > "${gl_ci_env_home_dir}/entrypoint.sh"
    echo 'export PATH=${PATH}:${HOME}/.local/bin' >> "${gl_ci_env_home_dir}/entrypoint.sh"
    chmod +x "${gl_ci_env_home_dir}/entrypoint.sh"
    gl_ci_env_extra_params="${gl_ci_env_extra_params} --entrypoint=${HOME}/entrypoint.sh";

    # Apply defaults based on provided args
    if [[ -z "${gl_ci_env_network_name}" ]]; then
        gl_ci_env_network_name="gitlab_ci_env"
    fi

    if [[ "${gl_ci_env_action:-}" = "stop-all" ]]; then
        # Remove all containers (stoped containers will remove themselves)
        docker stop $(docker ps | grep 'gitlab_ci_env_' | awk '{print $1}') 2> /dev/null;
        # Remove any docker networks
        docker network rm $(docker network ls | grep "${gl_ci_env_network_name}" | awk '{print $1}') 2> /dev/null;
        # Remove any docker volumes
        docker volume rm $(docker volume ls | grep "${gl_ci_env_network_name}" | awk '{print $2}') 2> /dev/null;
        return;
    fi

    # Start Dind container if required
    if [[ ! -z "${gl_ci_env_dind_container_name}" ]]; then
        # Start a dind container and share volumes with below CI env docker container
        if [[ -z "$(docker ps --filter "Name=gitlab_ci_env_" | grep dind)" ]]; then
            docker_in_docker_env \
                --name="${gl_ci_env_dind_container_name}" \
                --network="${gl_ci_env_network_name}" \
                --volume="${gl_ci_env_network_name}_dind" \
                --env AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID} \
                --env AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}
        fi
        gl_ci_env_extra_params="${gl_ci_env_extra_params} --env DOCKER_HOST=tcp://docker:2375";
        echo
    fi

    # Ensure image exists
    local gl_ci_env_docker_repo=$(echo ${gl_ci_env_image} | awk -F: '{print $1}');
    local gl_ci_env_docker_tag=$(echo ${gl_ci_env_image} | awk -F: '{print $2}');
    gl_ci_env_docker_tag=${gl_ci_env_docker_tag:-latest}
    local gl_ci_env_local_image_info=$(docker images -a --filter "reference=${gl_ci_env_docker_repo}:${gl_ci_env_docker_tag}" | grep -m1 "${gl_ci_env_docker_tag}");
    if [[ "${gl_ci_env_force_pull}" = 'true' || -z ${gl_ci_env_local_image_info} ]]; then
        docker pull "${gl_ci_env_image}"
    fi

    # Create gitlab_ci_env network so we can easily join other containers
    if [[ -z $(docker network ls 2> /dev/null | grep ${gl_ci_env_network_name}) ]]; then
        docker network create -d bridge ${gl_ci_env_network_name}
    fi

    # Complete entrypont
    echo 'exec "$@"' >> "${gl_ci_env_home_dir}/entrypoint.sh"

    # Run the image
    if [[ "${PWD}" != "${HOME}" ]]; then
        passthrough="--passthrough"
        # Create the path so it is not created by root on container creation
        mkdir -p "${PWD/$HOME/$gl_ci_env_config_dir\/home}"
    fi
    docker_run_sh \
        --image="${gl_ci_env_image}:${gl_ci_env_tag}" \
        ${passthrough} --user --exec=sh \
        --name="${gl_ci_env_container_name}" \
        --network="${gl_ci_env_network_name}" \
        --env AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID} \
        --env AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY} \
        --volume=/etc/passwd:/etc/passwd:ro \
        --volume="${gl_ci_env_home_dir}":"${HOME}":rw \
        --volume="${gl_ci_env_config_dir}/gitlab-ci-env-setup.sh":/tmp/.gitlab-ci-env-setup.sh:ro \
        --volume="${HOME}/.bashrc":"${HOME}/.bashrc":ro \
        --volume="${HOME}/.bash_aliases":"${HOME}/.bash_aliases":ro \
        ${gl_ci_env_extra_params}
}
