######## SETUP: #########
#
### Install taskwarrior
# Run:
# $ sudo apt install taskwarrior
# 
### Setup inthe.AM
# Create account at https://inthe.am/ and install the certificates
# Run:
# $ task sync init
# Add the following to crontab:
# "*/5 * * * * task sync >> /tmp/task_sync.log 2>&1"
#


#### Tasks Functions when using Tasks2IssueTracker:
#
function harvest {
    #>>
    # usage:                harvest [PROJECT] [OPTIONS]
    # examples:             "harvest gen This is a timers..."         -       Start a generic timer
    # examples:             "harvest project 12345"                   -       Start a timer associated to a current mantis issue
    # examples:             "harvest project Working on this.."       -       Start a timer against a project with no association to a mantis issue
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;
    script=$(ls ${HOME}/.task/hooks/on-add-Tasks2IssueTracker* 2> /dev/null);
    if [[ -z ${script} ]]; then
        echo "You need to first install Tasks2IssueTracker from https://gist.github.com/Josh5/62ef0947b084eba65e7800b02b294e87";
        return
    fi
    if [[ ! -z ${1} && ! -z ${2} ]]; then
        if [[ ${1} == 'gen' ]]; then
            PROJECT='harvest';
        else
            PROJECT=${1};
        fi
        if [[ ${2} =~ ^[0-9]+$ ]]; then
            ISSUE=${2};
            # Runs - "task add project:${PROJECT} tags:${ISSUE} blah" but without adding the task
            query='{"description":"","project":"'${PROJECT}'","tags":["'${ISSUE}'"]}';
        else
            DESCRIPTION=${@:2};
            # Runs - "task add project:${PROJECT} tags:${ISSUE} ${DESCRIPTION}" but without adding the task or raising t
            query='{"description":"'${DESCRIPTION}'","project":"'${PROJECT}'","tags":["1"]}';
        fi
        echo ${query} | ${script};
        echo
        if [[ ! -z ${ISSUE} ]]; then
            echo "Timer started for ISSUE ${ISSUE}";
        else
            echo "Timer started with description '${DESCRIPTION}'";
        fi
    else
        echo "you need to specify something to start on harvest.";
        echo "At least type 'harvest gen I started a new timer...'";
    fi
}
