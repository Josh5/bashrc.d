#!/usr/bin/env bash
###
# File: komiser.sh
# Project: bashrc.d
# File Created: Thursday, 3rd August 2023 9:31:28 am
# Author: Josh.5 (jsunnex@gmail.com)
# -----
# Last Modified: Thursday, 3rd August 2023 5:23:56 pm
# Modified By: Josh.5 (jsunnex@gmail.com)
###

function komiser_runner {
    #Help:
    #>>
    # usage:                komiser_runner [OPTIONS]
    # description:          Run Komiser Docker container.
    # options:              --configure-profiles                Configure the profiles for Komiser to scan.
    # options:              --all-regions                       Scan all AWS regions with Komiser.
    # options:              --collection=<string>               The collection to run Komiser. (Default: 'default')
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2>/dev/null && return

    # Defaults
    local komiser_arg_collection="default"
    local komiser_arg_scan_all_regions="false"
    local komiser_arg_run_config="false"
    local komiser_start_args="--config /etc/komiser/config/config.toml"

    local __komiser_version="3.1.0"

    # Parse args
    for arg in ${@}; do
        if [[ "${arg}" =~ "--collection" ]]; then
            komiser_arg_collection=$(echo ${arg} | awk -F'=' '{print $2}')
            continue
        elif [[ "${arg}" == "--all-regions" ]]; then
            komiser_arg_scan_all_regions='true'
            continue
        elif [[ "${arg}" == "--configure-profiles" ]]; then
            komiser_arg_run_config='true'
            continue
        fi
    done

    local __aws_config_file="${HOME}/.aws/config"
    local __komiser_config_path="${HOME}/.config/komiser/${komiser_arg_collection}"
    local __komiser_cache_path="${HOME}/.cache/komiser/${komiser_arg_collection}"
    mkdir -p \
        "${__komiser_config_path:?}"/{config,data} \
        "${__komiser_cache_path:?}/config"

    if [[ "${komiser_arg_run_config}" == "true" ]]; then
        # Fetch list of configured profiles
        local profiles=($(grep -oP '(?<=\[profile).+(?=\]$)' ${__aws_config_file}))

        # Check if any of these profiles are already configured
        for i in ${!profiles[@]}; do
            if grep "name=\"${profiles[i]}\"" "${__komiser_config_path:?}/config/config.toml" &>/dev/null; then
                choices[i]="+"
            fi
        done

        # Prompt to add / remove any profiles
        menu() {
            [[ "$msg" ]] && echo && echo "$msg"
            echo
            echo "Configured AWS Profiles:"
            for i in ${!profiles[@]}; do
                printf "%3d%s) %s\n" $((i + 1)) "${choices[i]:- }" "${profiles[i]}"
            done
            :
        }

        prompt="Select a profile (again to unselect, ENTER when done): "
        while menu && read -rp "$prompt" num && [[ "$num" ]]; do
            [[ "$num" != *[![:digit:]]* ]] &&
                ((num > 0 && num <= ${#profiles[@]})) ||
                {
                    msg="Invalid option: $num"
                    continue
                }
            ((num--))
            msg="   - ${profiles[num]} was ${choices[num]:+un}selected"
            [[ "${choices[num]}" ]] && choices[num]="" || choices[num]="+"
        done

        echo
        printf "Selected AWS profiles:"
        msg=" nothing"
        local __selected_branches=""
        for i in ${!profiles[@]}; do
            [[ "${choices[i]}" ]] && {
                printf " %s" "${profiles[i]}"
                msg=""
            } && __selected_branches="${__selected_branches} ${profiles[i]}"
        done
        echo
        echo

        echo "" >"${__komiser_config_path:?}/config/config.toml"
        for profile in ${__selected_branches}; do
            cat <<EOF >>"${__komiser_config_path:?}/config/config.toml"
[[aws]]
name="${profile}"
source="CREDENTIALS_FILE"
path="/etc/komiser/config/credentials.yaml"
profile="${profile}"
EOF
            echo "" >>"${__komiser_config_path:?}/config/config.toml"
        done
        cat <<EOF >>"${__komiser_config_path:?}/config/config.toml"
[sqlite]
file="/etc/data/komiser.db"
EOF

        return 0
    fi

    # Fetch credentials for each profile
    echo "" >"${__komiser_cache_path:?}/config/credentials.yaml"
    local __komiser_regions=""
    for profile in $(grep "name=" "${__komiser_config_path:?}/config/config.toml" | cut -d= -f2 | tr -d '"'); do
        aws_assume_role --region --profile=${profile:?} --silent
        if [[ $? -gt 0 ]]; then
            echo "Failed to assume role. Exit!"
            exit 1
        fi
        if [[ -z "${AWS_CONFIG_PROFILE_NAME}" ]]; then
            echo "Required parameter 'AWS_CONFIG_PROFILE_NAME' is not specified after assume role. Exit!"
            exit 1
        fi
        cat <<EOF >>"${__komiser_cache_path:?}/config/credentials.yaml"
[${AWS_CONFIG_PROFILE_NAME:?}]
aws_access_key_id=${AWS_ACCESS_KEY_ID:?}
aws_secret_access_key=${AWS_SECRET_ACCESS_KEY:?}
aws_session_token=${AWS_SESSION_TOKEN:?}
region=${AWS_REGION:?}
EOF
        echo "" >>"${__komiser_cache_path:?}/config/credentials.yaml"

        if [[ "${komiser_arg_scan_all_regions:-true}" != "true" ]]; then
            if [[ "${komiser_start_args}" != *"regions=${AWS_REGION:?}"* ]]; then
                komiser_start_args="${komiser_start_args} --regions=${AWS_REGION:?}"
            fi
        fi
    done
    echo "Komiser Config:"
    cat "${__komiser_config_path:?}/config/config.toml"
    echo
    echo "Assumed credentials:"
    cat "${__komiser_cache_path:?}/config/credentials.yaml"
    echo

    # Pull latest komiser image
    docker pull tailwarden/komiser:${komiser_version:-latest}

    # Create 'komiser_net' network so we can easily join other containers
    local __komiser_network_name="komiser_service_net"
    if [[ -z $(docker network ls 2>/dev/null | grep ${__komiser_network_name}) ]]; then
        docker network create -d bridge ${__komiser_network_name}
    fi

    # Run komiser
    docker run --rm \
        --name komiser_service \
        --network="${__komiser_network_name}" \
        -v "${__komiser_config_path:?}/config/config.toml":"/etc/komiser/config/config.toml":ro \
        -v "${__komiser_cache_path:?}/config/credentials.yaml":"/etc/komiser/config/credentials.yaml":ro \
        -v "${__komiser_config_path:?}/data/":"/etc/data/":rw \
        -p 3000:3000 \
        --entrypoint='' \
        "tailwarden/komiser:${komiser_version:-latest}" \
        komiser start ${komiser_start_args:-}

}
