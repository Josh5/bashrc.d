###
# File: nodejs.sh
# Project: bashrc.d
# File Created: Thursday, 03rd August 2019 10:14:05 am
# Author: Josh.5 (jsunnex@gmail.com)
# -----
# Last Modified: Wednesday, 4th September 2024 5:02:39 pm
# Modified By: Josh5 (jsunnex@gmail.com)
###

######## SETUP: #########
#
### Install nvm
#
# Follow the instructions here:
#   https://github.com/nvm-sh/nvm#installing-and-updating
#
###

# Enable nvm if installed
if [ -d ${HOME}/.nvm ]; then
    export NVM_DIR="${HOME}/.nvm"
    [ -s "${NVM_DIR}/nvm.sh" ] && \. "${NVM_DIR}/nvm.sh"                   # This loads nvm
    [ -s "${NVM_DIR}/bash_completion" ] && \. "${NVM_DIR}/bash_completion" # This loads nvm bash_completion
    export NVM_SYMLINK_CURRENT="true"
    export PATH="${HOME}/.nvm/current/bin:${PATH}"
    # Also add to .profile for intelij
    # TODO: Test launching webstorm from toolbox after a fresh login to see if this even works
    if [[ ! $( grep 'Added by nodejs.sh import of bashrc.d' ${HOME}/.profile ) ]]; then
       echo '' >> ${HOME}/.profile
       echo '# Added by nodejs.sh import of bashrc.d' >> ${HOME}/.profile
       echo 'export NVM_DIR="$HOME/.nvm"' >> ${HOME}/.profile
       echo '[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm' >> ${HOME}/.profile
       echo '[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion" # This loads nvm bash_completion' >> ${HOME}/.profile
    fi
fi

function nodejs_install {
    #Help:
    #>>
    # usage:                nvm_cli_install
    # description:          Installs a nvm
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2>/dev/null && return

    NODEJS_VERSION="v20.11.0"
    [ "X${@}" != "X" ] && NODEJS_VERSION="${@}"

    # Create any missing directories
    mkdir -p \
        "${HOME}"/.local/{bin,share}
    export PATH="${PATH}:${HOME}/.local/bin"

    # Install NodeJS
    command -v wget >/dev/null 2>&1 || { echo "ERROR! Missing required command 'wget'"; return 1; }
    command -v tar >/dev/null 2>&1 || { echo "ERROR! Missing required command 'tar'"; return 1; }
    mkdir -p "${HOME}/.local/share/nodejs"
    wget -q "https://nodejs.org/dist/${NODEJS_VERSION}/node-${NODEJS_VERSION}-linux-x64.tar.gz" \
        -O "${HOME}/.local/share/nodejs/node-${NODEJS_VERSION}-linux-x64.tar.gz"
    tar --extract \
        --file "${HOME}/.local/share/nodejs/node-${NODEJS_VERSION}-linux-x64.tar.gz" \
        --strip-components 1 \
        --directory "${HOME}/.local/share/nodejs/" \
        --no-same-owner
    for bin in $(ls "${HOME}"/.local/share/nodejs/bin/); do 
        ln -sf $(realpath -s "${HOME}"/.local/share/nodejs/bin/${bin}) "${HOME}"/.local/bin/${bin}; 
    done
    node --version
    npm --version
}

function nvm_cli_install {
    #Help:
    #>>
    # usage:                nvm_cli_install
    # description:          Installs a nvm
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2>/dev/null && return

    if command -v curl &>/dev/null; then
        curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.4/install.sh | bash
    elif command -v wget &>/dev/null; then
        wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.4/install.sh | bash
    fi

    if [[ -f "${HOME}/bashrc.d/nodejs.sh" ]]; then
        source "${HOME}/bashrc.d/nodejs.sh"
    fi
}
