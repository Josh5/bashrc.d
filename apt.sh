#!/usr/bin/env bash
###
# File: apt.sh
# Project: bashrc.d
# File Created: Tuesday, 18th October 2022 12:04:09 pm
# Author: Josh Sunnex (jsunnex@gmail.com)
# -----
# Last Modified: Tuesday, 18th October 2022 12:13:09 pm
# Modified By: Josh Sunnex (jsunnex@gmail.com)
###


# Add some helper functions for apt (if this distro has apt)
if command -v apt &> /dev/null; then\
    apt_recently_installed() {
        grep " installed " /var/log/dpkg.log
        echo
        echo "Check '/var/log/dpkg.log' For more details."
    }
fi
