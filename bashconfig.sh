

#       /$$$$$$  /$$ /$$
#      /$$__  $$| $$|__/
#     | $$  \ $$| $$ /$$  /$$$$$$   /$$$$$$$  /$$$$$$   /$$$$$$$
#     | $$$$$$$$| $$| $$ |____  $$ /$$_____/ /$$__  $$ /$$_____/
#     | $$__  $$| $$| $$  /$$$$$$$|  $$$$$$ | $$$$$$$$|  $$$$$$
#     | $$  | $$| $$| $$ /$$__  $$ \____  $$| $$_____/ \____  $$
#     | $$  | $$| $$| $$|  $$$$$$$ /$$$$$$$/|  $$$$$$$ /$$$$$$$/
#     |__/  |__/|__/|__/ \_______/|_______/  \_______/|_______/
#
#
#

# Add path to env with sudo command
alias sudoenv='sudo env PATH=$PATH'

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='grep -F --color=auto'
    alias egrep='grep -E --color=auto'
fi

# ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Add date string aliases
alias now='date +%Y%m%d-%H%M%S'
alias now_stamp='date +%s'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# SP  ' '  0x20 = · U+00B7 Middle Dot
# TAB '\t' 0x09 = ￫ U+FFEB Halfwidth Rightwards Arrow
# CR  '\r' 0x0D = § U+00A7 Section Sign (⏎ U+23CE also works fine)
# LF  '\n' 0x0A = ¶ U+00B6 Pilcrow Sign (was "Paragraph Sign")
alias show-whitespace="sed 's/ /·/g;s/\t/￫/g;s/\r/§/g;s/$/¶/g'"

# Generate a random 16 character password
alias genpasswd='echo $(echo "$(date +%s.%N) $(shuf -n1 -i0-999999)" | sha256sum | base64 | head -c 16)'

# Create a butane alias for running butane commands with docker
alias butane='docker run --rm --interactive       \
    --security-opt label=disable        \
    --volume ${PWD}:/pwd --workdir /pwd \
    quay.io/coreos/butane:release'

# Any local only scripts (if they exist)
if [ -d ${HOME}/bashrc.d/local_only -a -r ${HOME}/bashrc.d/local_only -a -x ${HOME}/bashrc.d/local_only ]; then
  for i in ${HOME}/bashrc.d/local_only/*.sh; do
    if [ -f $i ]; then
      . $i
    fi
  done
fi

#      /$$$$$$$$                                           /$$           /$$    /$$                    /$$           /$$       /$$
#     | $$_____/                                          | $$          | $$   | $$                   |__/          | $$      | $$
#     | $$       /$$   /$$  /$$$$$$   /$$$$$$   /$$$$$$  /$$$$$$        | $$   | $$ /$$$$$$   /$$$$$$  /$$  /$$$$$$ | $$$$$$$ | $$  /$$$$$$   /$$$$$$$
#     | $$$$$   |  $$ /$$/ /$$__  $$ /$$__  $$ /$$__  $$|_  $$_/        |  $$ / $$/|____  $$ /$$__  $$| $$ |____  $$| $$__  $$| $$ /$$__  $$ /$$_____/
#     | $$__/    \  $$$$/ | $$  \ $$| $$  \ $$| $$  \__/  | $$           \  $$ $$/  /$$$$$$$| $$  \__/| $$  /$$$$$$$| $$  \ $$| $$| $$$$$$$$|  $$$$$$
#     | $$        >$$  $$ | $$  | $$| $$  | $$| $$        | $$ /$$        \  $$$/  /$$__  $$| $$      | $$ /$$__  $$| $$  | $$| $$| $$_____/ \____  $$
#     | $$$$$$$$ /$$/\  $$| $$$$$$$/|  $$$$$$/| $$        |  $$$$/         \  $/  |  $$$$$$$| $$      | $$|  $$$$$$$| $$$$$$$/| $$|  $$$$$$$ /$$$$$$$/
#     |________/|__/  \__/| $$____/  \______/ |__/         \___/            \_/    \_______/|__/      |__/ \_______/|_______/ |__/ \_______/|_______/
#                         | $$
#                         | $$
#                         |__/

export PATH="${PATH}:${HOME}/.local/bin"
if [ -d ${HOME}/.local/share/umake/bin -a -r ${HOME}/.local/share/umake/bin -a -x ${HOME}/.local/share/umake/bin ]; then
    export PATH="${PATH}:${HOME}/.local/share/umake/bin"
fi



#      /$$$$$$$$                              /$$     /$$
#     | $$_____/                             | $$    |__/
#     | $$    /$$   /$$ /$$$$$$$   /$$$$$$$ /$$$$$$   /$$  /$$$$$$  /$$$$$$$   /$$$$$$$
#     | $$$$$| $$  | $$| $$__  $$ /$$_____/|_  $$_/  | $$ /$$__  $$| $$__  $$ /$$_____/
#     | $$__/| $$  | $$| $$  \ $$| $$        | $$    | $$| $$  \ $$| $$  \ $$|  $$$$$$
#     | $$   | $$  | $$| $$  | $$| $$        | $$ /$$| $$| $$  | $$| $$  | $$ \____  $$
#     | $$   |  $$$$$$/| $$  | $$|  $$$$$$$  |  $$$$/| $$|  $$$$$$/| $$  | $$ /$$$$$$$/
#     |__/    \______/ |__/  |__/ \_______/   \___/  |__/ \______/ |__/  |__/|_______/
#
#
#

function batstat {
    if [[ c$(ommand -v upower) ]]; then
        upower -i `upower -e | grep 'BAT'`;
    else
        echo "You do not have upower installed..."
    fi
}

function extract {
    #Help:
    #>>
    # usage:                extract <FILE>
    # description:          Extract a file.
    # description:          Supports extensions:
    # description:          :    .tar.bz2, .tar.gz, .bz2, .rar, .gz, .tar, .tbz2, .tgz, .zip, .Z, .7z
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;
    unset ${extract_command};
    if [ -f ${1} ] ; then
        case ${1} in
          *.tar.bz2)   extract_command="tar xjf ${1}"     ;;
          *.tar.gz)    extract_command="tar xzf ${1}"     ;;
          *.bz2)       extract_command="bunzip2 ${1}"     ;;
          *.rar)       extract_command="unrar x ${1}"     ;;
          *.gz)        extract_command="gunzip ${1}"      ;;
          *.tar)       extract_command="tar xf ${1}"      ;;
          *.tbz2)      extract_command="tar xjf ${1}"     ;;
          *.tgz)       extract_command="tar xzf ${1}"     ;;
          *.zip)       extract_command="unzip ${1}"       ;;
          *.Z)         extract_command="uncompress ${1}"  ;;
          *.7z)        extract_command="7z x ${1}"        ;;
          *)           echo "'${1}' cannot be extracted via ex()" ;;
        esac
        echo ${extract_command};
        if [ ! -z "${extract_command}" ]; then
            echo ${extract_command};
            ${extract_command};
        else
            echo
            __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} --help && return;
        fi
    else
        echo "'${1}' is not a valid file";
        echo
        __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} --help && return;
    fi
}

# Set a window always on top
function always_on_top_selector {
    #Help:
    #>>
    # usage:                wmctrl
    # description:          Select a window to set as "Always on Top"
    #<<
    __read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;
    wmctrl -r :SELECT: -b add,above
}


# Disable keys
function key_disable_capslock {
    xmodmap -e 'keycode 66=';
}


# Enable keys
function key_enable_capslock {
    xmodmap -e 'keycode 66=Caps_Lock';
}

# Modify keys
function key_shift_to_capslock {
    xmodmap -e 'keycode 50=Caps_Lock';
}

## DEFAULTS:
#key_disable_capslock;


function __read_function_help {
    # This function prints out help text for a bash function:
    # For this to work, you need to have a header inside the function with usage, options etc.
    # This header block must start with '#>>' finish with '#<<'
    # With the exception of usage, all values set here can be repeated 

    # Instructions:
    # 1) Add a header block like in the example below
    # 2) Immediately after the header block, add this line:
    # "__read_function_help ${BASH_SOURCE[0]} ${FUNCNAME[0]} ${@} 2> /dev/null && return;"

    # EXAMPLE:
    #>>
    # usage:                the_bash_function [OPTIONS] [ARG]
    # options:              --delete                        -           Delete this thing
    # options:              --save                          -           Save this thing
    # arguments:            path                            -           Path to a thing
    # examples:             the_bash_function --delete      -           Deletes something
    # description:          This is a description paragraph of what this function can do
    #<<

    # RESULT:
    # Usage:      the_bash_function [OPTIONS]
    # 
    #     This is a description paragraph of what this function can do
    # 
    # Options:
    #              --delete                        -           Delete this thing
    #              --save                          -           Save this thing
    # 
    # Arguments:
    #              path                            -           Path to a thing
    # 
    # Examples:
    #              the_bash_function --delete      -           Deletes something

    if [[ ! -z  ${1} ||  -z  ${2} ]]; then
        for arg in ${@:3}; do
            # Only show output if we are passed a 'help' arg
            if [[ "${arg}" == "help" ||  "${arg}" == "-h" ||  "${arg}" == "--help" ]]; then
                OUTPUT_TEXT="";
                SOURCE_FILE=${1};
                SOURCE_FUNCTION=${2};

                # Read function from source file
                FUNCTION_HELP_TEXT_V2=$(sed -n "/^\s*\(function\)*\s*${SOURCE_FUNCTION}\s*.*{/,/}/p" ${SOURCE_FILE} | awk "/>>v2/,/<</");
                FUNCTION_HELP_TEXT=$(sed -n "/^\s*\(function\)*\s*${SOURCE_FUNCTION}\s*.*{/,/}/p" ${SOURCE_FILE} | awk "/>>/,/<</");

                if [[ -n ${FUNCTION_HELP_TEXT_V2} ]]; then
                    while IFS= read -r FUNCTION_HELP_TEXT_LINE; do
                        # Use awk to skip the markers `#>>v2` and `#<<`
                        if ! echo "$FUNCTION_HELP_TEXT_LINE" | awk '/^[[:space:]]*#>>v2/ || /^[[:space:]]*#<</ {exit 1}'; then
                            continue
                        fi

                        # Remove the leading whitespace and `# ` prefix
                        FUNCTION_HELP_TEXT_LINE="${FUNCTION_HELP_TEXT_LINE##*([[:space:]])}"
                        FUNCTION_HELP_TEXT_LINE="${FUNCTION_HELP_TEXT_LINE##\#}"
                        
                        # Print the cleaned line
                        OUTPUT_TEXT="${OUTPUT_TEXT}${FUNCTION_HELP_TEXT_LINE}\n"
                    done <<< "${FUNCTION_HELP_TEXT_V2}"
                    OUTPUT_TEXT="${OUTPUT_TEXT}\n"
                else
                    # Add usage string to output text (usage is displayed in-line, so spacing is 12 - text)
                    KEY_STRING='usage:'
                    USAGE_TEXT=$(echo "${FUNCTION_HELP_TEXT}" | grep "${KEY_STRING}" | sed "s|^[^:]*${KEY_STRING}||" | sed "s/^[ \t]*/    /");
                    if [[ ! -z ${USAGE_TEXT} ]]; then
                        OUTPUT_TEXT="${OUTPUT_TEXT} Usage:  ${USAGE_TEXT}\n\n";
                    fi

                    # Add usage string to output text (usage is displayed in-line, so spacing is 12 - text)
                    KEY_STRING='description:'
                    DESCRIPTION_TEXT=$(echo "${FUNCTION_HELP_TEXT}" | grep "${KEY_STRING}" | sed "s|^[^:]*${KEY_STRING}||" | sed "s/^[ \t]*/    /");
                    if [[ ! -z ${DESCRIPTION_TEXT} ]]; then
                        OUTPUT_TEXT="${OUTPUT_TEXT} Description:\n ${DESCRIPTION_TEXT}\n\n";
                    fi

                    # Add options string to output text
                    KEY_STRING='options:'
                    OPTIONS_TEXT=$(echo "${FUNCTION_HELP_TEXT}" | grep "${KEY_STRING}" | sed "s|^[^:]*${KEY_STRING}||" | sed "s/^[ \t]*/    /");
                    if [[ ! -z ${OPTIONS_TEXT} ]]; then
                        OUTPUT_TEXT="${OUTPUT_TEXT} Options:\n${OPTIONS_TEXT}\n\n";
                    fi

                    # Add arguments string to output text
                    KEY_STRING='arguments:'
                    ARGUMENTS_TEXT=$(echo "${FUNCTION_HELP_TEXT}" | grep "${KEY_STRING}" | sed "s|^[^:]*${KEY_STRING}||" | sed "s/^[ \t]*/    /");
                    if [[ ! -z ${ARGUMENTS_TEXT} ]]; then
                        OUTPUT_TEXT="${OUTPUT_TEXT} Arguments:\n${ARGUMENTS_TEXT}\n\n";
                    fi

                    # Add examples string to output text
                    KEY_STRING='examples:'
                    EXAMPLES_TEXT=$(echo "${FUNCTION_HELP_TEXT}" | grep "${KEY_STRING}" | sed "s|^[^:]*${KEY_STRING}||" | sed "s/^[ \t]*/    /");
                    if [[ ! -z ${EXAMPLES_TEXT} ]]; then
                        OUTPUT_TEXT="${OUTPUT_TEXT} Examples:\n${EXAMPLES_TEXT}\n\n";
                    fi
                fi

                # Print output text:
                echo -ne "${OUTPUT_TEXT}";
                return 0;
            fi
        done
    fi
    # No help was displayed
    return 1;
}


function json_dict_to_env {
    for keyval in $( echo "${@}" | grep -E '": [^\{]' | sed -e 's/: /=/' -e "s/\(\,\)$//"); do
      echo "Exporting $keyval"
      eval export $keyval
    done
}
