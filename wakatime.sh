# @Author: Josh S
# @Date:   2018-08-07 10:46:10
# @Last Modified by:   Josh S
# @Last Modified time: 2018-09-14 08:17:41
#
# Ref: https://wakatime.com/terminal
#
# Install the python module:
#$ sudo -H python2.7 -m pip install wakatime;
#
# Clone bash script:
#
#$ mkdir -p ${HOME}/Tools
#$ cd ${HOME}/Tools
#$ git clone https://github.com/gjsheep/bash-wakatime.git 

if [ "X${BASH_VERSION:-}" != "X" ]; then
    if [[ ! -z $(command -v wakatime) && -e ${HOME}/.wakatime.cfg ]]; then
        source ${HOME}/Tools/bash-wakatime/bash-wakatime.sh
    fi
fi
